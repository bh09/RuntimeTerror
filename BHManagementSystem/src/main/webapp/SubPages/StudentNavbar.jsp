<%-- 
    Document   : StudentNavbar
    Created on : 2019.06.28., 6:02:26
    Author     : rajnaig
--%>

<%@page import="hu.braininghub.BHManagementSystem.service.EventInterface"%>
<%@page import="hu.braininghub.BHManagementSystem.dto.TeamDTO"%>
<%@page import="hu.braininghub.BHManagementSystem.service.TeamInterface"%>
<%@page import="hu.braininghub.BHManagementSystem.dto.StudentDTO"%>
<%@page import="hu.braininghub.BHManagementSystem.service.StudentInterface"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="hu.braininghub.BHManagementSystem.service.StudentBean"%>
<%@page import="javax.ejb.EJB"%>
<%@page contentType="text/html" pageEncoding="Latin2"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=Latin2">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="shortcut icon" href="./favicon-144.png" />
        <link 
        href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" 
        rel="stylesheet"  type='text/css'>
        <!--mozg� sz�zal�khoz-->
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        
        <!--chartokhoz-->
        <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    
        <link rel="stylesheet" type="text/css" href="..\CSS\carets.css">
        <style>
        <jsp:include page=".\CSS\carets.css"/>
        <jsp:include page=".\CSS\menu_background.css"/>
        <jsp:include page=".\CSS\avatar.css"/>
        <jsp:include page=".\CSS\scrolling_text.css"/>
        </style>  
        <style>
         .navbar .navbar-brand img{
          height: 30px; 
    margin-top: -5px;
}            
          .dropdown-menu{right: 0;}

          

  

          </style>
          <%!

StudentInterface students;
EventInterface events;
%>   
          <%
              
               Long studentID=(Long)session.getAttribute("studentID");
              
              /*instanceName = (StudentInterface)new InitialContext().lookup("java:global/[application_name]/[module_name]/[enterprise_bean_name]/[inteface_name]");*/
              students = (StudentInterface)new InitialContext().lookup("java:global/BHManagementSystem-1.0-SNAPSHOT/StudentBean!hu.braininghub.BHManagementSystem.service.StudentInterface");
              StudentDTO student=  students.getStudentByStudentID(studentID);
              request.setAttribute("username",student.getName());
              events=(EventInterface)new InitialContext().lookup("java:global/BHManagementSystem-1.0-SNAPSHOT/EventBean!hu.braininghub.BHManagementSystem.service.EventInterface");
              TeamDTO team=student.getTeam();
              session.setAttribute("teamID", team.getId());
              request.setAttribute("teamname",team.getTeamName());
              String eventsInString=events.getEventsForTeamIDAndConvertToString(team.getId());
              if(eventsInString.isEmpty()){
              request.setAttribute("scrollingtext","Nincs �j esem�ny");
              }else{
              request.setAttribute("scrollingtext",eventsInString);    
              }
          %>
          <link rel="icon" href="./Pictures/favicon-144.png">
    </head>
    <body>
    <body>
<nav class="navbar navbar-expand-lg navbar-light custom-carets" style="background-color: rgb(13, 54, 106);">

 
      <a class="nav-text"><span name="username" style="color: #11e88d">�dv�z�llek&nbsp;${username}&nbsp;a&nbsp;${teamname}.&nbsp;csoportban</span></a>
      <form class="navbar-form" name="homeForm" id="homeForm" action="${servletName}" method="POST">
      <a class="nav-link" style="color: #11e88d"onClick="document.forms.homeForm.submit();">Kezd�lap<span class="sr-only">(current)</span></a>
       <input type="hidden" name="inputLink" value="home"/>
      </form>    
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-text">
              <div class="container-fluid">  
              <div class="box" style="background-color: white">
                <div class="eventscroller">
                    <marquee>
                      <div id="scrollmessage">
                        <!--Ide j�nnek az eventek-->
                          <h3><span id="message"style="color: #11e88d">${scrollingtext}</span></h3>
                          </div>
                        <!--Ide j�nnek az eventek v�ge-->  
                    </marquee>
                  </div>
                    </div>
                    </div>     
              </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>Feladatok</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <!--form class="navbar-form" name="homeworkForm" id="homeworkForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.homeworkForm.submit();">H�zi feladat</a>
                  <input type="hidden" name="inputLink" value="homework"/>
              </form>  
                  <div class="dropdown-divider"></div-->
                  <form class="navbar-form" name="quizForm" id="quizForm" action="${servletName}" method="POST">
                <a class="dropdown-item" onClick="document.forms.quizForm.submit();">Dolgozat</a>
             <input type="hidden" name="inputLink" value="quiz"/>
                  </form>
                  </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>�rt�kel�sek</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <form class="navbar-form" name="homeworkpointsForm" id="homeworkpointsForm" action="${servletName}" method="POST">
                <a class="dropdown-item" onClick="document.forms.homeworkpointsForm.submit();">H�zi �rt�kel�sek</a>
                <input type="hidden" name="inputLink" value="homeworkpoints"/>
          </form>      
                <div class="dropdown-divider"></div>
                <form class="navbar-form" name="quizpointsForm" id="quizpointsForm" action="${servletName}" method="POST">
                <a class="dropdown-item" onClick="document.forms.quizpointsForm.submit();">Dolgozat</a>
                <input type="hidden" name="inputLink" value="quizpoints"/>
                </form>
                <div class="dropdown-divider"></div>
                <form class="navbar-form" name="zhForm" id="zhForm" action="${servletName}" method="POST">
                 <a class="dropdown-item" onClick="document.forms.zhForm.submit();">ZH</a>
                <input type="hidden" name="inputLink" value="zhpoints"/>
                </form>
                 <div class="dropdown-divider"></div>
                <form class="navbar-form" name="interviewForm" id="interviewForm" action="${servletName}" method="POST"> 
                 <a class="dropdown-item" onClick="document.forms.interviewForm.submit();">Interview</a>
            <input type="hidden" name="inputLink" value="interview"/>
                </form>
                </li>        
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>Men�</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <form class="navbar-form" name="profilForm" id="profilForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.profilForm.submit();">Profil szerkeszt�se</a>
                <input type="hidden" name="inputLink" value="profil"/>
                </form>
                  <div class="dropdown-divider"></div>
               <form class="navbar-form" name="quitForm" id="quitForm" action="${servletName}" method="POST">    
                <a class="dropdown-item" onClick="document.forms.quitForm.submit();">Kil�p�s</a>
                <input type="hidden" name="inputLink" value="quit"/>
            </li>
        </ul>
       
      </div>
    </nav>
    </body>
</html>
