<%-- 
    Document   : TeacherNavbar
    Created on : 2019.07.01., 22:56:17
    Author     : rajnaig
--%>


<%@page import="hu.braininghub.BHManagementSystem.dto.TeamDTO"%>
<%@page import="java.util.List"%>
<%@page import="hu.braininghub.BHManagementSystem.service.TeacherNavbarInterface"%>
<%@page import="hu.braininghub.BHManagementSystem.service.EventInterface"%>
<%@page import="hu.braininghub.BHManagementSystem.dto.TeacherDTO"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="hu.braininghub.BHManagementSystem.service.TeacherInterface"%>
<%@page contentType="text/html" pageEncoding="Latin2"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=Latin2">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--link rel="shortcut icon" href="./favicon-144.png" /-->
        <link rel="icon" href="favicon-144.png">
        <link 
        href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" 
        rel="stylesheet"  type='text/css'>
        <!--mozg� sz�zal�khoz-->
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        
        <!--chartokhoz-->
        <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    
        <link rel="stylesheet" type="text/css" href="..\CSS\carets.css">
        <style>
        <jsp:include page=".\CSS\carets.css"/>
        <jsp:include page=".\CSS\menu_background.css"/>
        <jsp:include page=".\CSS\avatar.css"/>
        <jsp:include page=".\CSS\scrolling_text.css"/>
        </style>  
        <style>
         .navbar .navbar-brand img{
          height: 30px; 
    margin-top: -5px;
}            
          .dropdown-menu{right: 0;}

          

  

          </style>
                    <%!

TeacherInterface teachers;
TeacherNavbarInterface teacherNavbar;
EventInterface events;
%>  
<%
    
    
    Long teacherID=(Long)session.getAttribute("teacherID");
    if(null==teacherID){
        String teacherIDstring=request.getParameter("teacherID");
        teacherID=Long.parseLong(teacherIDstring);
        session.setAttribute("teacherID", teacherID);
    }
    
    teachers = (TeacherInterface)new InitialContext().lookup("java:global/BHManagementSystem-1.0-SNAPSHOT/TeacherBean!hu.braininghub.BHManagementSystem.service.TeacherInterface");
    TeacherDTO teacher=  teachers.getTeacherByTeacherID(teacherID);
              request.setAttribute("username",teacher.getName());          
              teacherNavbar = (TeacherNavbarInterface)new InitialContext().lookup("java:global/BHManagementSystem-1.0-SNAPSHOT/TeacherNavbarBean!hu.braininghub.BHManagementSystem.service.TeacherNavbarInterface");          
              List<TeamDTO>teams=teacherNavbar.addChooseTeam();
              String servletName=(String)session.getAttribute("servletName");
              request.setAttribute("servletName",servletName);
              Long teamID=(Long)session.getAttribute("teamID");
              if(null==teamID){
              teams=teacherNavbar.getSelectedTeam(-1L, teams);
              teamID=-1L;
                      session.setAttribute("teamID", -1L);
              }else{
              teams=teacherNavbar.getSelectedTeam(teamID, teams);
              }
              
              String eventsInString;
    request.setAttribute("teamList", teams);
    events=(EventInterface)new InitialContext().lookup("java:global/BHManagementSystem-1.0-SNAPSHOT/EventBean!hu.braininghub.BHManagementSystem.service.EventInterface");          
                if (teamID==-1L) {
                eventsInString="V�lassz csoportot";      
                  }else{
                   eventsInString=events.getEventsForTeamIDAndConvertToString(teamID);
                   if(eventsInString.isEmpty()){
                   eventsInString="Nincs �j esem�ny";    
                   }
                }
                request.setAttribute("scrollingtext", eventsInString);

    
    /*String home=request.getParameter("home");
    String attendance=request.getParameter("attendance");
    String homework=request.getParameter("homework");
    String quiz=request.getParameter("quiz");
    String homeworkedit=request.getParameter("homeworkedit");
    String quizedit=request.getParameter("quizedit");
    String homeworkpoints=request.getParameter("homeworkpoints");
    String quizpoints=request.getParameter("quizpoints");
    String interview=request.getParameter("interview");
    String profil=request.getParameter("profil");
    String quit=request.getParameter("quit");
    System.out.println(home);
    System.out.println(attendance);
    System.out.println(homework);
    System.out.println(quiz);
    System.out.println(homeworkedit);
    System.out.println(quizedit);
    System.out.println(homeworkpoints);
    System.out.println(quizpoints);
    System.out.println(interview);
    System.out.println(profil);
    System.out.println(quit);
    session.setAttribute("redirected", false);
    if(null!=home&&servletName!="TeacherServlet"){
      request.setAttribute("home",null);
      session.setAttribute("redirected", true);
      home=null;
      request.getRequestDispatcher("/TeacherServlet").forward(request, response); 
    }else if(null!=attendance){
    }else if(null!=homework){
    }else if(null!=quiz){
        quiz=null;
        request.setAttribute("quiz",null);
        Cookie cookieTeacher=new Cookie("teacherID",teacherID.toString());
        response.addCookie(cookieTeacher);
        Cookie cookieTeam=new Cookie("teamID",teamID.toString());
        response.addCookie(cookieTeam);
        response.sendRedirect("http://localhost:8080/BHManagementSystem/QuizQuestionServlet");
        //request.getRequestDispatcher("/QuizQuestionServlet").forward(request, response); 
    }else if(null!=homeworkedit){
        
    }else if(null!=quizedit){
  
    }else if(null!=homeworkpoints){
    
    }else if(null!=quizpoints){
        
    }else if(null!=interview){
        
    }else if(null!=profil){
        
    }else if(null!=quit){
        
    }*/
%>
    </head>
    <body>
        
<nav class="navbar navbar-expand-lg navbar-light custom-carets" style="background-color: rgb(13, 54, 106);">

 
      <a class="nav-text"><span name="username" style="color: #11e88d">�dv�z�llek&nbsp;${username}</span></a>
      <form class="navbar-form" name="homeForm" id="homeForm" action="${servletName}" method="POST">
      <a class="nav-link" style="color: #11e88d"onClick="document.forms.homeForm.submit();">Kezd�lap<span class="sr-only">(current)</span></a>
       <input type="hidden" name="inputLink" value="home"/>
      </form>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <style>
            .bootstrap-select.btn-group:not(.input-group-btn), .bootstrap-select.btn-group[class*="span"] {
    margin: 0 !important;
}          
            </style>
            <script>
                $('select').selectpicker();
            </script>    
                
            <li class="nav-text">
              <div class="container-fluid">  
              <div class="box" style="background-color: white">
                <div class="eventscroller">
                    <marquee>
                      <div id="scrollmessage">
                        <!--Ide j�nnek az eventek-->
                          <h3><span id="message">${scrollingtext}</span></h3>
                          </div>
                        <!--Ide j�nnek az eventek v�ge-->  
                    </marquee>
                  </div>
                    </div>
                    </div>     
              </li>
          <li class="nav-item dropdown">
              
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>Jelenl�ti �v</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <form class="navbar-form" name="attendanceForm" id="attendanceForm" action="${servletName}" method="POST"> 
                  <a class="dropdown-item" onClick="document.forms.attendanceForm.submit();">Jelenl�ti kit�lt�se</a>
                <input type="hidden" name="inputLink" value="attendance"/>
                </form>
                <div class="dropdown-divider"></div>
                <form class="navbar-form" name="attendanceSearchForm" id="attendanceSearchForm" action="${servletName}" method="POST"> 
                <a class="dropdown-item" onClick="document.forms.attendanceSearchForm.submit();">Jelenl�ti �v keres�s</a>
                <input type="hidden" name="inputLink"  value="attendancesearch"/>
                </form>
            </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>Feladatok ki�r�sa</span>
              </a>
              
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
             <form class="navbar-form" name="homeworkForm" id="homeworkForm" action="${servletName}" method="POST">     
                  <a class="dropdown-item" onClick="document.forms.homeworkForm.submit();">H�zi feladat</a>
                  <input type="hidden" name="inputLink" value="homework"/>
                </form>
                  <div class="dropdown-divider"></div>
                  <form class="navbar-form" name="quizForm" id="quizForm" action="${servletName}" method="POST">
                <a class="dropdown-item"  onClick="document.forms.quizForm.submit();">Dolgozat</a>
                <input type="hidden" name="inputLink" value="quiz"/>
                </form>
                <div class="dropdown-divider"></div>
                  <form class="navbar-form" name="examForm" id="examForm" action="${servletName}" method="POST">
                <a class="dropdown-item"  onClick="document.forms.examForm.submit();">Vizsga</a>
                <input type="hidden" name="inputLink" value="exam"/>
                </form>
            </li>
          <li class="nav-item dropdown">
              
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>Feladatok szerkeszt�se</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <form class="navbar-form" name="homeworkeditForm" id="homeworkeditForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.homeworkeditForm.submit();">H�zi feladat</a>
                <input type="hidden" name="inputLink"  value="homeworkedit"/>
                </form>
                <div class="dropdown-divider"></div>
                <form class="navbar-form" name="quizeditForm" id="quizeditForm" action="${servletName}" method="POST">
                <a class="dropdown-item" onClick="document.forms.quizeditForm.submit();">Dolgozat</a>
                <input type="hidden" name="inputLink"  value="quizedit"/>
                </form>
                <div class="dropdown-divider"></div>
                <form class="navbar-form" name="exameditForm" id="exameditForm" action="${servletName}" method="POST">
                <a class="dropdown-item" onClick="document.forms.exameditForm.submit();">Vizsga</a>
                <input type="hidden" name="inputLink"  value="examedit"/>
                </form>
                
            </li>  
            <li class="nav-item dropdown">
                
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                    <span class=caret>
                      Pontoz�s
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <form class="navbar-form" name="homeworkpointForm" id="homeworkpointForm" action="${servletName}" method="POST">
                    <a class="dropdown-item"onClick="document.forms.homeworkpointForm.submit();">H�zik pontoz�sa</a>
                  <input type="hidden" name="inputLink" id="teacherIDPost" value="homeworkpoints"/>
                </form>
                  <div class="dropdown-divider"></div>
                  <form class="navbar-form" name="quizpointForm" id="quizpointForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.quizpointForm.submit();">Dolgozatok</a>
                  <input type="hidden" name="inputLink" id="teacherIDPost" value="quizpoints"/>
                  </form>
                  <div class="dropdown-divider"></div>
                  <form class="navbar-form" name="interviewForm" id="interviewForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.interviewForm.submit();">Interj�k pontoz�sa</a>
                  <input type="hidden" name="inputLink" id="teacherIDPost" value="interview"/>
                </form>
                </li>  
          <li class="nav-item dropdown">
              
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="color: #11e88d">
                  <span class=caret>Men�</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <form class="navbar-form" name="profilForm" id="profilForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.profilForm.submit();">Profil szerkeszt�se</a>
                <input type="hidden" name="inputLink" value="profil"/>
                </form>
              <div class="dropdown-divider"></div>
              <form class="navbar-form" name="creategroupForm" id="creategroupForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.creategroupForm.submit();">�j Csoport</a>
                <input type="hidden" name="inputLink" value="creategroup"/>
                </form>
              <div class="dropdown-divider"></div>
                <form class="navbar-form" name="registrationForm" id="registrationForm" action="${servletName}" method="POST">
                  <a class="dropdown-item" onClick="document.forms.registrationForm.submit();">Regisztr�ci�</a>
                <input type="hidden" name="inputLink" value="registration"/>
                </form>
                <div class="dropdown-divider"></div>
                <form class="navbar-form" name="quitForm" id="quitForm" action="${servletName}" method="POST">
                <a class="dropdown-item" onClick="document.forms.quitForm.submit();">Kil�p�s</a>
            <input type="hidden" name="inputLink" value="quit"/>
            </form>
              </li>
              <li>
                <form class="navbar-form" name="TeamForm" id="TeamForm" action="${servletName}" method="POST">
              <div class="form-group">
                    <select class="form-control" name="teamDropdown"onchange="document.forms.TeamForm.submit();">
               <c:forEach items="${teamList}"  var="team">
                   <option type="button" value="${team.id}">${team.teamName}</option>
     </c:forEach>
 </select>
                    </div>
     </form>  
            </li>
        </ul>
       
      </div>
    </nav>
                          
    </body>
</html>