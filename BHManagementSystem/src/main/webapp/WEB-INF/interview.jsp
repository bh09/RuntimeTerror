<%-- 
    Document   : interview
    Created on : 2019.06.20., 14:27:28
    Author     : Bence58<%@page contentType="text/html" pageEncoding="UTF-8"%>
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"-->
        <title>interju ertekeles</title>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>   
    </head>
    <body>
        <h1>Interju jsp</h1>

        <div class="row d-flex justify-content-center">
            <div class="col-md-10">

                <!--   <form method="post"> </form> -->
                <div class="col-5">
                    <form name="TeamForm" id="TeamForm" action="Interview" method="POST">
                        <!--label for="comment">Csoport:</label>
                        <select name="teamDropdown" onchange="this.form.submit();" class="form-control" required>
                            <!--c:forEach items="${teamList}"  var="team">
                                <option type="button" value="${team.id}">${team.teamName}</option>
                            <!--/c:forEach>
                        </select>
                        <br/>

                </div-->



                <c:choose>
                    <c:when test="${teamNotChosen}">
                        <fieldset disabled><div class="col-5">
                                <label for="comment">Tanuló:</label>
                                <select type="text" class="form-control" name="student" id="comment">
                                    <option>Tanuló 1</option>
                                </select>
                                <div class="text-muted">
                                   Kérlek válassz egy tanulót!
                                </div>
                            </div>
                            <br/>

                            <div class="col-12">
                                <label for="comment">Hr Komment:</label>
                                <textarea type="text" class="form-control " rows="5" id="comment" name="hrcomment" required></textarea>
                                <div class="text-muted">
                                    Kérlek fűzz hozzá valamit!
                                </div>
                            </div>
                            <br />
                            <div class="col-4">
                                <label for="hrpontok">HR Pontok:</label>
                                <input type="number" name="hrpoints" class="form-control" id="hrpontok" min="1" max="5" required>
                                <div class="text-muted">
                                    Kérlek értékeld az adott halgató HR teljesitményét egy pontszámmal!
                                </div>
                            </div>
                            <br/>
                            <div class="col-12">
                                <label for="comment">Szakmai Komment:</label>
                                <textarea class="form-control" rows="5" id="comment" name="javacomment" required></textarea>
                                <div class="text-muted">
                                    Kérlek fűzz hozzá valamit!
                                </div>
                            </div>
                            <br />
                            <div class="col-4">
                                <label for="comment">Szakmai Pontok:</label>
                                <input type="number" name="javapoints" class="form-control" id="comment" min="1" max="5" required>
                                <div class="text-muted">
                                    Kérlek értékeld az adott halgató szakmai teljesitményét egy pontszommal!
                                </div>
                            </div>
                            <br/>
                            <div class="d-flex justify-content-end">
                                <input type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                            </div>
                    </div> 

                    <br/>
                    <br/>
                    <br/>
                </fieldset>


            </c:when>

            <c:otherwise>

                <div class="col-5">
                    <label for="comment">Tanuló:</label>
                    <select name="studentDropdown"  class="form-control" required>
                        <c:forEach items="${studentList}"  var="student">
                            <option type="button" value="${student.id}">${student.name}</option>
                        </c:forEach>
                    </select>
                    <div class="text-muted">
                        Kérlek válassz egy tanulót!
                    </div>
                </div>
                <br/>

                <div class="col-5">
                    <label for="comment">Interjú:</label>
                    <select name="interviewSelect"  class="form-control" required>
                        <option type="button" value="1">Első</option>
                        <option type="button" value="2">Második</option>
                    </select>
                    <div class="text-muted">
                        Kérlek válassz hányadik interjúja lessz a tanulónak!
                    </div>
                </div>
                <br/>

                <div class="col-12">
                    <label for="comment">Hr Komment:</label>
                    <textarea type="text" class="form-control " rows="5" id="comment" name="hrcomment" required></textarea>
                    <div class="text-muted">
                        Kérlek fűzz hozzá valamit!
                    </div>
                </div>
                <br />
                <div class="col-4">
                    <label for="hrpontok">HR Pontok:</label>
                    <input type="number" name="hrpoints" class="form-control" id="hrpontok" min="0" max="5" required>
                    <div class="text-muted">
                        Kérlek értékeld az adott halgató HR teljesitményét egy pontszámmal!
                    </div>
                </div>
                <br/>
                <div class="col-12">
                    <label for="comment">Szakmai Komment:</label>
                    <textarea class="form-control" rows="5" id="comment" name="javacomment" minlength="3" maxlength="100" required></textarea>
                    <div class="text-muted">
                        Kérlek fűzz hozzá valamit!
                    </div>
                </div>
                <br />
                <div class="col-4">
                    <label for="comment">Szakmai Pontok:</label>
                    <input type="number" name="javapoints" class="form-control" id="comment" min="0" max="5" required>
                    <div class="text-muted">
                        Kérlek értékeld az adott halgató szakmai teljesitményét egy pontszommal!
                    </div>
                </div>
                <br/>
                <div class="d-flex justify-content-end">
                    <input type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                </div>
                <div>
                    <p>
                    <table class="table">
                        <thead>
                        <th>Név</th>
                        <th>Első interjú</th>
                        <th>Második interjú</th>
                        </thead>
                        <c:forEach items="${studentList}"  var="student">
                            <tr >

                                <c:choose>
                                    <c:when test="${student.firstInterview==false}"> 
                                        <td class="table-danger"><c:out value="${student.name}"></c:out></td>
                                        <td class="table-danger"><c:out value="Nincs"></c:out></td>
                                        <td class="table-danger"><c:out value="Nincs"></c:out></td>
                                    </c:when>
                                    <c:when test="${student.secondInterview==true}">
                                        <td class="table-success"><c:out value="${student.name}"></c:out></td>
                                        <td class="table-success"><c:out value="Van"></c:out></td>
                                        <td class="table-success"><c:out value="Van"></c:out></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="table-warning"><c:out value="${student.name}"></c:out></td>
                                        <td class="table-warning"><c:out value="Van"></c:out></td>
                                        <td class="table-warning"><c:out value="Nincs"></c:out></td>

                                    </c:otherwise>

                                </c:choose>


                            </tr>
                        </c:forEach>
                    </table>
                    </p>
                </div>

            </div>
            <br/>
            <br/>
            <br/>

        </c:otherwise>
    </c:choose>  
    <!-- 
    <div class="form-group">

        <div class="col-5">
            <label for="comment">Csoport:</label>
            <select type="text"  class="form-control" name="group" id="comment">
                <option>Csoport 1</option>
                <option>Csoport 2</option>
                <option>Csoport 3</option>
                <option>Csoport 4</option>
                <option>Csoport 5</option>
            </select>
            <div class="text-muted">
                K�rlek v�lassz egy csoportot!
            </div>
        </div>
        <br/>
    -->


</form>
</div>
</div>
</body>
</html>
