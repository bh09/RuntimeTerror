<%-- 
    Document   : TaskList
    Created on : 2019.06.25., 20:06:49
    Author     : rajnaig
--%>

<%@page contentType="text/html" pageEncoding="Latin1"%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Questions</title>
    <!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"-->
    <!--script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script-->
    <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"-->
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script-->
    <!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script-->
    
    
    </head>
    <body>
        
<table class="table table-striped" name="quizTable">
    <form name="questionForm" id="questionForm" action="TaskListServlet" method="POST">    
    <p>Az els� <span><input type="number" name="limitTable" id="hrpontok" min="1" value="${limit}" required style="width:100%;max-width:80px "></span>tal�lat Keres�s<span><input type="text" name="search"></span><span><input type="submit" value="Mehet!" class="btn btn-outline-success btn-sm"></span></p>    
        <tr>
        <div name="quizTable" onchange="document.questionForm.submit();">          
            <td align="center"><button name="quiztable" class="quiztable"  value="${id}" style="padding: 0;border: none;background: none;">${idText}</button></td>
            <td align="center"><button name="quiztable" class="quiztable"value="${firstQuestion}" style="padding: 0;border: none;background: none;">${firstQuestionText}</button></td>
            <td align="center"><button name="quiztable" class="quiztable"value="${secondQuestion}" style="padding: 0;border: none;background: none;">${secondQuestionText}</button></td>
            <td align="center"><button name="quiztable" class="quiztable"value="${thirdQuestion}" style="padding: 0;border: none;background: none;">${thirdQuestionText}</button></td>
        </div>
        </tr>    
        <c:forEach items="${questionList}"  var="question">
        <tr>
        <td align="center" >${question.id}</td>
        <!--form name="saveForm" id="saveForm" action="TaskListServlet" method="POST"-->
        <td><input type="text" name="firstQuestion${question.id}" class="form-control" id="firstQuestion${question.id}" value="${question.firstQuestion}" required></td>
        <td><input type="text" name="secondQuestion${question.id}" class="form-control" id="secondQuestion${question.id}" value="${question.secondQuestion}" required></td>
        <td><input type="text" name="thirdQuestion${question.id}" class="form-control" id="thirdQuestion${question.id}" value="${question.thirdQuestion}" required></td>
        <td><button name="overwriteQuestion" class="btn btn-primary btn-sm"value="${question.id}"onclick="location.reload(forceGet);"><i class="far fa-save"></i></button>            
        <br>
        <c:if test="${question.fireQuestion==null}">
        <button name="fireQuestion" class="btn btn-success btn-sm"value="${question.id}"onclick="fire();"><i class="far fa-play-circle"></i></button>
        <br>
        <input type="hidden" name="areYouSureFireQuestion" id="areYouSureFireQuestion"/>
            <script>
            function fire(){    
            var txt;
            var r = confirm("Biztos, hogy �les�ted a k�rd�st?");
            if (r == true) {
            txt = "I";
            } else {
            txt = "N";
            }
            document.getElementById("areYouSureFireQuestion").value = txt;
        }
        </script>
        </c:if>
        <button name="deleteQuestion" class="btn btn-danger btn-sm"value="${question.id}"onclick="location.reload(forceGet);"><i class="far fa-trash-alt"></i></button>
        <br>
        <button name="copyToClipBoard"id="copyToClipBoard" class="btn btn-secondary btn-sm"value="${question.id}"onclick="copyClipBoard(${question.id})"><i class="fas fa-paste"></i></button></td>
        <script>
            function copyClipBoard(docID) {
  console.log(docID);
  var copyTextfirst = document.getElementById("firstQuestion"+docID);
  var copyTextsecond = document.getElementById("secondQuestion"+docID);
  var copyTextthird= document.getElementById("thirdQuestion"+docID);
   var el = document.createElement('textarea');  
   el.value = copyTextfirst.value+"\n"+copyTextsecond.value+"\n"+copyTextthird.value;
   document.body.appendChild(el);
        el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
}
    </script>
        <!--/form-->

        </tr>
        </c:forEach>
        
    </table>
       
       </form>
       </body>
</html>
