<%-- 
    Document   : addHomework
    Created on : 2019.06.27., 7:46:12
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            $(function () {
                $("#datepicker").datepicker();
                $("#datepicker").attr('required', 'required');
//                $("#datepicker").attr('readonly', 'readonly');
            });
        </script>
        <title>Add Homework</title>

    </head>




    <body>
        
        <c:choose>
                <c:when test="${fieldsVisible}">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="col-5">
                    <br/>
                    <h1>Házi feladat létrehozása: </h1>
                </div>
            </div>
        </div>

        <div class="row d-flex justify-content-center">
            
                    <div class="col-md-10">
                        <form action="HomeworkServlet" method="post">
                            <br/>
                            <div class="col-5">
                                <label for="comment">Házi feladat határidő:</b></label><br/>
                                <input type="text" id="datepicker" name="date" autocomplete="off">  <!--readonly-->
                            </div>



                            <br/>


                            <div class="col-12">
                                <label for="comment">Kapható maximális pontszám:</b></label>
                                </br>
                                <select name="maxPoints">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>


                            </br>
                            <div class="col-12">
                                <label for="comment">Házi feladat címe:</b></label>
                                <br><input type="text" name="hwtitle" required=""/><br/><br/>
                            </div>


                            <div class="col-5">
                                <label for="comment">Feladat leírása:</b></label>
                                <textarea type="text" class="form-control " rows="5" id="comment" name="hwdesc" pattern=".{10,}" required></textarea>
                            </div>
                            </br>
                            </br>
                            <div class="col-5">
                                <input type="submit" value="Létrehoz" class="btn btn-outline-success col-4">
                            </div>
                        </form>
                    </div>
                </c:when>
                <c:otherwise>
                    <br>
                    <h3><center>Válassz csoportot</center></h3>
                        </c:otherwise>
                    </c:choose>
        </div>
    </body>
</html>
