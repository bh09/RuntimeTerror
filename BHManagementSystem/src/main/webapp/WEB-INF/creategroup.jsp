<%-- 
    Document   : creategroup
    Created on : 2019.07.10., 18:35:27
    Author     : Bence58
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Új csoport hozzáadása</title>
    </head>
    <body>
        <h1>Új csoport hozzáadása</h1>

        <br/>
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">

                <form action="CreateGroup" method="POST">
                    <div class="col-5">
         
                        <input type="text" class="form-control" name="googleCallendar" placeholder="Add meg a google calendart!" required><br/>
                       <a href="https://calendar.google.com">Google naptár bejelenkezés</a>
                        <input type="text" class="form-control" name="groupname" placeholder="Add meg a csoport nevét!" required><br/>
                        <input name="GroupSubmit" type="submit" class="btn btn-outline-success col-4" value="Mehet">
                    </div>

                </form>

            </div>
        </div>


    </body>
</html>
