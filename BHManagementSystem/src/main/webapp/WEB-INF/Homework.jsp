<%-- 
    Document   : Homework
    Created on : 2019.06.23., 7:03:51
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="Latin1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>jQuery UI Datepicker - Default functionality</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });
        </script>



        <title>H�zi feladat l�trehoz�sa</title>
    </head>
    <body>
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="col-5">
                    <h1>H�zi feladat l�trehoz�sa: </h1>
                </div>
            </div>
        </div>



        <div class="row d-flex justify-content-center">




            <div class="col-md-10">


                <br/>
                <div class="col-5">
                    <label for="comment">Date:</label>
                    <p><input type="text" id="datepicker"></p>
                </div>


                <!--   <form method="post"> </form> -->
                <div class="col-5">
                    <form name="TeamForm" id="TeamForm" action="Homework" method="POST">
                        <label for="comment">Csoport:</label>
                        <select name="teamDropdown" onchange="this.form.submit();" class="form-control" required>
                            <c:forEach items="${teamList}"  var="team">
                                <option type="button" value="${team.id}">${team.teamName}</option>
                            </c:forEach>
                        </select>
                        <br/>

                </div>

                <div class="col-12">
                    <label for="comment">C�m:</label>
                    <br><input type="text" name="title"/><br/><br/>
                </div>


                <c:choose>
                    <c:when test="${teamNotChosen}">
                        <fieldset disabled><div class="col-5">
                                <!--                                <label for="comment">Tanul�:</label>
                                                                <select type="text" class="form-control" name="student" id="comment">
                                                                    <option>Tanul� 1</option>
                                                                    <option>Tanul� 2</option>
                                                                    <option>Tanul� 3</option>
                                                                    <option>Tanul� 4</option>
                                                                    <option>Tanul� 5</option>
                                                                </select>
                                                                <div class="text-muted">
                                                                    K�rlek v�lassz egy tanul�t!
                                                                </div>-->
                            </div>
                            <br/>

                            <div class="col-12">
                                <label for="comment">Feladat le�r�sa:</label>
                                <textarea type="text" class="form-control " rows="5" id="comment" name="hrcomment" required></textarea>
                                <div class="text-muted">
                                    <!--Feladat le�r�sa...-->
                                </div>
                            </div>
                            <br />
                            <div class="col-4">
                                <label for="hrpontok">HR Pontok:</label>
                                <input type="number" name="hrpoints" class="form-control" id="hrpontok" min="1" max="5" required>
                                <div class="text-muted">
                                    K�rlek �rt�keld az adott halgat� hr teljesitm�ny�t egy pontsz�mmal!
                                </div>
                            </div>
                            <br/>


                            <!--                            <div class="col-12">
                                                            <label for="comment">Szakmai Komment:</label>
                                                            <textarea class="form-control" rows="5" id="comment" name="javacomment" required></textarea>
                                                            <div class="text-muted">
                                                                K�rlek f�zz hozz� valamit!
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="col-4">
                                                            <label for="comment">Szakmai Pontok:</label>
                                                            <input type="number" name="javapoints" class="form-control" id="comment" min="1" max="5" required>
                                                            <div class="text-muted">
                                                                K�rlek �rt�keld az adott halgat� szakmai teljesitm�ny�t egy pontsz�mmal!
                                                            </div>
                                                        </div>
                                                        <br/>
                                                        <div class="d-flex justify-content-end">
                                                            <input type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                                                        </div>-->


                    </div> 

                    <br/>
                    <br/>
                    <br/>
                </fieldset>


            </c:when>

            <c:otherwise>

                <!--                <div class="col-5">
                                    <label for="comment">Tanul�:</label>
                                    <select type="text" class="form-control" name="student" id="comment">
                                        <option>Tanul� 1</option>
                                        <option>Tanul� 2</option>
                                        <option>Tanul� 3</option>
                                        <option>Tanul� 4</option>
                                        <option>Tanul� 5</option>
                                    </select>
                                    <div class="text-muted">
                                        K�rlek v�lassz egy tanul�t!
                                    </div>
                                </div>-->
                <br/>

                <div class="col-12">
                    <label for="comment">Feladat le�r�sa:</label>
                    <textarea type="text" class="form-control " rows="5" id="comment" name="hrcomment" required></textarea>
                    <!--                    <div class="text-muted">
                                            K�rlek f�zz hozz� valamit!
                                        </div>-->
                </div>
                <br />
                <!--                <div class="col-4">
                                    <label for="hrpontok">HR Pontok:</label>
                                    <input type="number" name="hrpoints" class="form-control" id="hrpontok" min="0" max="5" required>
                                    <div class="text-muted">
                                        K�rlek �rt�keld az adott halgat� hr teljesitm�ny�t egy pontsz�mmal!
                                    </div>
                                </div>
                                
                                
                                <br/>
                                <div class="col-12">
                                    <label for="comment">Szakmai Komment:</label>
                                    <textarea class="form-control" rows="5" id="comment" name="javacomment" required></textarea>
                                    <div class="text-muted">
                                        K�rlek f�zz hozz� valamit!
                                    </div>
                                </div>
                                <br />
                                <div class="col-4">
                                    <label for="comment">Szakmai Pontok:</label>
                                    <input type="number" name="javapoints" class="form-control" id="comment" min="0" max="5" required>
                                    <div class="text-muted">
                                        K�rlek �rt�keld az adott halgat� szakmai teljesitm�ny�t egy pontsz�mmal!
                                    </div>
                                </div>
                                <br/>
                -->                                <div class="d-flex justify-content-end">
                    <input type="submit" value="L�trehoz" class="btn btn-outline-success col-4">
                </div>


            </div> 
            <br/>
            <br/>
            <br/>

        </c:otherwise>
    </c:choose>  
    <!-- 
    <div class="form-group">

        <div class="col-5">
            <label for="comment">Csoport:</label>
            <select type="text"  class="form-control" name="group" id="comment">
                <option>Csoport 1</option>
                <option>Csoport 2</option>
                <option>Csoport 3</option>
                <option>Csoport 4</option>
                <option>Csoport 5</option>
            </select>
            <div class="text-muted">
                K�rlek v�lassz egy csoportot!
            </div>
        </div>
        <br/>
    -->


</form>
</div>
</div>
</body>
</html>

