<%-- 
    Document   : attendancesearch
    Created on : 2019.07.09., 23:14:14
    Author     : Bence58
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>AttendanceSearch</h1>


        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <form method="POST">
                    <div class="col-4">

                        <input type="date" class="form-control" name="attendanceDay" placeholder="Válassz dátumot" required><br/>
                        <input type="submit" class="btn btn-outline-success col-4" value="Keres">  
                    </div>

                </form>


                <div>
                    <p>
                    <table class="table">
                        <thead>
                        <th>Név</th>
                        <th>Dátum</th>
                        <th>Hiányzás az adott napon (Perc)</th>
                        </thead>
                        <c:forEach items="${listOfStudents}"  var="student">

                            <tr>
                                <td class="table-success"><c:out value="${student.name}"></c:out></td>
                                <td class="table-success"><c:out value="${student.date}"></c:out></td>
                                <td class="table-success"><c:out value="${student.missingTime}"></c:out></td>
                                </tr>

                        </c:forEach>
                    </table>
                    </p>
                </div>


            </div>
        </div>
    </body>
</html>
