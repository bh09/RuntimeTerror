<%-- 
    Document   : DisplayHomeworkResult
    Created on : 2019.07.06., 21:40:00
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Homework Display</title>
    </head>

    <body>
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="col-5">
                    <br/>
                    <h1>Házi feladat eredmények listázása </h1>
                    <br>
                </div>
            </div>
        </div>
        <form method="post">
            <div class="row d-flex justify-content-center">
                <div class="col-md-10">
                    <div class="col-1">
                        <label for="comment">Csoport:</label>
                        <select name="teamDropdown"   onchange="this.form.submit()"  class="form-control" required>
                            <c:forEach items="${teamList}"  var="team">
                                <option type="button" name="teamselect" value="${team.id}">${team.teamName}</option>
                            </c:forEach>
                        </select>
                        <br/>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="col-1">
                        <label for="comment">Diák:</label>
                        <select name="studentDropdown"   onchange="this.form.submit()"  class="form-control" required>
                            <!--<option type="button" name="studentselect" value="> 1" >Összes</option>-->
                            <c:forEach items="${studentList}"  var="student">
                                <option type="button" name="studentselect" value="${student.getId()}">${student.getName()}</option>
                            </c:forEach>
                        </select>
                        <br/>
                    </div>
                </div>                
                <div class="col-md-10">
                    <div class="col-1">
                        <label for="comment">Házi feladat:</label>
                        <select name="homeworkDropdown"   onchange="this.form.submit()"  class="form-control" required>
                            <!--<option type="button" name="homeworkselect" value="> 1" >Összes</option>-->
                            <c:forEach items="${homeworkList}"  var="homework">
                                <option type="button" name="homeworkselect" value="${homework.getId()}">${homework.getTitle()}</option>
                            </c:forEach>
                        </select>
                        <br/>
                    </div>
                </div>
                <table id="example" class="table table-striped table-bordered nowrap"  style="width:80%">
                    <thead>
                        <tr>
                            <th>Diák</th>
                            <th>Házi feladat</th>
                            <th>Elérhető pontszám</th>
                            <th>Kapott pontszám</th>
                            <th>Százalék</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:set var = "sumOfMaxPoints" value = "${0}"/>
                        <c:set var = "sumOfResultPoints" value = "${0}"/>
                        <c:forEach items="${homeworkResultList}" var="list">    
                            <tr>

                                <td>
                                    <c:out   value="${list.getStudentDTO().getName()}"/>
                                </td><td>
                                    <c:out   value="${list.getHomework().getTitle()}"/>
                                </td><td>
                                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${list.getHomework().getMaxPoints()}"/>
                                    <input type="hidden" name="calculation1" value="${sumOfMaxPoints = sumOfMaxPoints+list.getHomework().getMaxPoints()}">
                                </td><td>
                                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${list.getResultPoints()}"/>
                                    <input type="hidden" name="calculation2" value="${sumOfResultPoints = sumOfResultPoints + list.getResultPoints()}">
                                </td><td>
                                    <fmt:formatNumber type="number" maxFractionDigits="0" value="${list.getResultPoints()/list.getHomework().getMaxPoints()*100}"/> %
                                </td></td>
                            </tr>
                        </c:forEach>   
                        <tr>
                            <td>
                            </td>
                            <td><b>Összesen</b></td>
                            <td><b><c:out   value="${sumOfMaxPoints}"/></b></td>
                            <td><b>   <fmt:formatNumber type="number" maxFractionDigits="0" value="${sumOfResultPoints}"/>   </b></td>
                            <td><b>
                                    <fmt:formatNumber type="number" maxFractionDigits="0" value="${sumOfResultPoints/sumOfMaxPoints*100}"/> %
                                </b></td>
                        </tr>
                    </tbody>
                </table>                   
            </div>
        </form>
    </body>
</html>