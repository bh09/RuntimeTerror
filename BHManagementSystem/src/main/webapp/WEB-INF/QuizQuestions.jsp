<%-- 
    Document   : QuizQuestions
    Created on : 2019.06.25., 10:59:01
    Author     : rajnaig
--%>

<%@page contentType="text/html" pageEncoding="Latin2"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>   
        <title>Quiz Questions</title>
    </head>
    <body>
<c:if test="${dbFailure}">
    <p style="color:red">Hiba az adatok t�rol�sa sor�n, pr�b�ld meg �jra</p>
        </c:if>        

<c:if test="${writeQuestion1}">
    <p style="color:red">Az 1. k�rd�smez�t nem t�lt�tted ki</p>
        </c:if>
<c:if test="${writeQuestion2}">
    <p style="color:red">A 2. k�rd�smez�t nem t�lt�tted ki</p>
        </c:if>
<c:if test="${writeQuestion3}">
    <p style="color:red">Az 3. k�rd�smez�t nem t�lt�tted ki</p>
        </c:if>
<c:if test="${dbSuccess}">
    <p style="color:green">Az adatb�zis m�velet sikeresen befejez�d�tt</p>
        </c:if>    
    <c:choose>
        <c:when test="${fieldsVisible}">  
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <form method="post">
                    <div class="form-group">
                        <div class="col-12">
                            <label for="comment">Els� k�rd�s:</label>
                            <textarea type="text" class="form-control " rows="5" id="comment" name="question1"></textarea>

                        </div>
                        <br />

                        <div class="col-12">
                            <label for="comment">M�sodik k�rd�s:</label>
                            <textarea class="form-control" rows="5" id="comment" name="question2"></textarea>
                        </div>
                        <br />
                        <div class="col-12">
                            <label for="comment">Harmadik k�rd�s:</label>
                            <textarea class="form-control" rows="5" id="comment" name="question3"></textarea>
                        </div>
                        <br/>
                        <div class="d-flex justify-content-end">
                            <input type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                        </div>
                    </div> 
                </form>
            </div>
        </div>
        </c:when>
        <c:otherwise>
            <h3><center>V�lassz csoportot</center></h3>
        </c:otherwise>
        </c:choose> 
    </body>
</html>

