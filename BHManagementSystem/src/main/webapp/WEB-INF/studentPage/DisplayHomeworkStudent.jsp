<%-- 
    Document   : DisplayHomeworkStudent
    Created on : 2019.07.14., 16:21:02
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../../SubPages/StudentNavbar.jsp"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/desert.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Homework Display</title>
    </head>

    <body>

        <div class="myimage">
            <img src="Pictures/asztal11GIF.gif" width="100%" style="opacity: 0.9">

            <br>
            <div class="row d-flex justify-content-center">

                <br/>
                <h1 class="text-center">Házi feladatok listázása </h1>
                <br>

            </div>

            <br>
            <br>

            <div class="row d-flex justify-content-center">

                <table id="example" class="table table-striped table-bordered nowrap"  style="width:60%">
                    <thead>
                        <tr>
                            <th>Házi feladat címe</th>
                            <th>Feladás dátuma</th>
                            <th>Leadási határidő</th>
                            <th>Feladat leírása</th>
                            <th>Kapható pontszám</th>
                            <th>Elért pontszám</th>
                            <th>Százalék</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${homeworkResults}" var="list">    
                            <tr>
                                <td>
                                    <c:out   value="${list.getHomework().getTitle()}"/>
                                </td><td>
                                    <c:out   value="${list.getHomework().getDateReleased()}"/>
                                </td><td>
                                    <c:out   value="${list.getHomework().getDeadline()}"/>
                                </td><td>
                                    <c:out   value="${list.getHomework().getDescription()}"/>
                                </td><td>
                                    <c:out   value="${list.getHomework().getMaxPoints()}"/>
                                </td><td>
                                    <fmt:parseNumber var = "i" integerOnly = "true" 
                                                     type = "number" value = "${list.getResultPoints()}" />
                                    <c:out   value="${i}"/>
                                </td><td>
                                    ${Math.round(list.getResultPoints()
                                      /list.getHomework().getMaxPoints()
                                      *100)} %
                                </td>
                            </tr>
                        </c:forEach>    
                    <tfoot>
                        <tr style="font-weight: bold">
                            <td>Összesen: </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><fmt:parseNumber var = "i" integerOnly = "true" 
                                             type = "number" value = "${Math.round(totalMaxPoints)}" />
                                ${i}</td>
                            <td><fmt:parseNumber var = "i" integerOnly = "true" 
                                             type = "number" value = "${Math.round(totalResultPoints)}" />
                                ${i}</td>
                            <td><fmt:parseNumber var = "i" integerOnly = "true" 
                                             type = "number" value = "${Math.round(totalPercentage)}" />
                                ${i} %</td>
                        </tr>
                    </tfoot>



                    </tbody>
                </table>

            </div>    



        </div>

    </body>
</html>
