<%-- 
    Document   : DisplayExamsStudent
    Created on : 2019.07.14., 16:42:50
    Author     : Takács Gergely
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="../../SubPages/CSS/backgroundimage.css"
              <jsp:include page="$/../../../SubPages/StudentNavbar.jsp"/>
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Exams Display</title>
    </head>

    <body>

        <br>


        <div class="row d-flex justify-content-center">
            <br/>
            <br>
            <h1 class="text-center">Vizsgaeredmények listázása </h1>
        </div>

        <br>
        <br>

        <div class="row d-flex justify-content-center">

            <c:choose>
                <c:when test="${!examResultListNormal.isEmpty()}">
                    <br>
                    <br>
                    <table id="example" class="table table-striped table-bordered nowrap"  style="width:50%">
                        <thead>
                            <tr>
                                <th>Dátum</th>
                                <th>Típus</th>
                                <th>Elmélet (Max 30 pont)</th>
                                <th>Százalék</th>
                                <th>Gyakorlat (Max 30 pont)</th>
                                <th>Százalék</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${examResultListNormal}" var="list">    
                                <tr>
                                    <td>
                                        <c:out   value="${list.getExamDTO().getExamDate()}"/>
                                    </td><td>
                                        <c:out   value="${list.getExamDTO().getExamType().getDesc()}"/>
                                    </td>

                                    <c:choose>
                                        <c:when test="${list.getResultPointsTheory() < 18 && list.getResultPointsTheory()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>

                                        <c:out   value="${list.getResultPointsTheory()}"/>
                                    </td>

                                    <c:choose>
                                        <c:when test="${list.getResultPointsTheory() < 18 && list.getResultPointsTheory()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        ${Math.round(list.getResultPointsTheory()/30*100)} %
                                    </td>


                                    <c:choose>
                                        <c:when test="${list.getResultPointsJava() < 18 && list.getResultPointsJava()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        <c:out   value="${list.getResultPointsJava()}"/>
                                    </td>


                                    <c:choose>
                                        <c:when test="${list.getResultPointsJava() < 18 && list.getResultPointsJava()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        ${Math.round(list.getResultPointsJava()/30*100)} %
                                    </td>
                                </tr>
                            </c:forEach>    
                        </tbody>
                    </table>
                </c:when>
            </c:choose>    
        </div>
        <br>
        <br>


        <div class="row d-flex justify-content-center">
            <c:choose>
                <c:when test="${!examResultListFinal.isEmpty()}">
                    <table id="example" class="table table-striped table-bordered nowrap"  style="width:60%">
                        <thead>
                            <tr>
                                <th>Dátum</th>
                                <th>Típus</th>
                                <th>Elmélet (Max 30 pont)</th>
                                <th>Százalék</th>
                                <th>Elmélet (Max 30 pont)</th>
                                <th>Százalék</th>
                                <th>Csapatmunka (Max 30 pont)</th>
                                <th>Százalék</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${examResultListFinal}" var="list">    
                                <tr>
                                    <td>
                                        <c:out   value="${list.getExamDTO().getExamDate()}"/>
                                    </td><td>
                                        <c:out   value="${list.getExamDTO().getExamType().getDesc()}"/>
                                    </td>


                                    <c:choose>
                                        <c:when test="${list.getResultPointsTheory() < 18 && list.getResultPointsTheory()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        <c:out   value="${list.getResultPointsTheory()}"/>
                                    </td>

                                    <c:choose>
                                        <c:when test="${list.getResultPointsTheory() < 18 && list.getResultPointsTheory()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        ${Math.round(list.getResultPointsTheory()/30*100)} %
                                    </td>


                                    <c:choose>
                                        <c:when test="${list.getResultPointsJava() < 18 && list.getResultPointsJava()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        <c:out   value="${list.getResultPointsJava()}"/>
                                    </td>

                                    <c:choose>
                                        <c:when test="${list.getResultPointsJava() < 18 && list.getResultPointsJava()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        ${Math.round(list.getResultPointsJava()/30*100)} %
                                    </td>

                                    <c:choose>
                                        <c:when test="${list.getResultPointsTeamwork() < 18 && list.getResultPointsTeamwork()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        <c:out   value="${list.getResultPointsTeamwork()}"/>
                                    </td>

                                    <c:choose>
                                        <c:when test="${list.getResultPointsTeamwork() < 18 && list.getResultPointsTeamwork()>=1}">
                                            <td class="table-danger">
                                            </c:when>
                                            <c:otherwise>
                                            <td> 
                                            </c:otherwise>
                                        </c:choose>
                                        ${Math.round(list.getResultPointsTeamwork()/30*100)} %
                                    </td>
                                </tr>
                            </c:forEach>    
                        </tbody>
                    </table>
                </c:when>
            </c:choose>
        </div>

        <br>
        <br>

        <c:choose>
            <c:when test="${examResultListFinal.isEmpty() && examResultListNormal.isEmpty()}">

                <div class="row d-flex justify-content-center">

                    <br/>
                    <h3 class="text-center">Nincs eredmény </h3>
                    <br>

                </div>
            </c:when>


        </c:choose>



    </body>
</html>
