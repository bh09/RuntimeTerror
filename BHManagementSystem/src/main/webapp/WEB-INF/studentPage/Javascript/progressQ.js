/*előállítja a csúszka gradiensét a progressq.css alapján*/
function createQGradient(){
$(document).ready(function () {
    /*beolvassa a div data-amount attribútumát(hány % a csúszka állása)*/ 
    var dataval = parseInt($('.progressQ').attr("data-amount"));
    var currentData=parseInt($('.progressQ').attr("data"));
    
    if (dataval < 101) {
        /*A valóságban nem a gradienscsík mozog előre, hanem a háttércsík csúszik hátra, amit az amount.css-ben szabályozunk*/
        $('.progressQ .amount').css("width", 100 - dataval + "%");
    }
    /*1000 ns végállásra megváltoztatja a progressQ contentjét a mi százalékunkra*/
    $('.progressQ').css("content", dataval + "%");
  },1000);
}
/*A számlálót pörgeti*/
function createCountTextQ(){
    $('.progressQ').each(function () {
    var $this = $(this);
    jQuery({ Counter: 0 }).animate({ Counter: $this.attr('data') }, {
      duration: 2000,
      easing: 'swing',
      step: function (now) {
        $('.progressQ').css('content',$this.attr('data',Math.ceil(now)));
      }
    });
  });
}    