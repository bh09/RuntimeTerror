function createParticipationGradient(){
    $(document).ready(function () {
    var dataval = parseInt($('.progressparticipation').attr("data-amount"));
    var currentData=parseInt($('.progressparticipation').attr("data"));
    
    if (dataval < 101) {
        $('.progressparticipation .amount').css("width", 100 - dataval + "%");
    }
    $('.progressparticipation').css("content", dataval + "%");
  },1000);  
  }
  function createCountTextParticipation(){
    $('.progressparticipation').each(function () {
      var $this = $(this);
      jQuery({ Counter: 0 }).animate({ Counter: $this.attr('data') }, {
        duration: 2000,
        easing: 'swing',
        step: function (now) {
          $('.progressparticipation').css('content',$this.attr('data',Math.ceil(now)));
        }
      });
    });    
  }