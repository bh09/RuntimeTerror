<%-- 
    Document   : StudentsQuizResult
    Created on : 2019.07.14., 2:24:52
    Author     : rajnaig
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../../SubPages/StudentNavbar.jsp"/>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Röpzh eredmények</title>
    </head>
    <body>
        <table class="table table-striped">
                 <th>Dátum</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
            <c:forEach items="${checkedList}" var="answer">
                <tr>
                <td>    
                <c:out value="${answer.questions.fireQuestion.toLocalDateTime().toLocalDate()}"/>
                 </td><td>   
                <c:out value="${answer.questions.firstQuestion}"/>
                 </td><td>   
                <c:out value="${answer.comment1}"/>
                 </td><td>   
                <c:out value="${answer.score1}"/>
                 </td><td>   
                <c:out value="${answer.questions.secondQuestion}"/>
                 </td><td>   
                <c:out value="${answer.comment2}"/>
                 </td><td>   
                <c:out value="${answer.score2}"/>
                 </td><td>   
                <c:out value="${answer.questions.thirdQuestion}"/>
                 </td><td>   
                <c:out value="${answer.comment3}"/>
                 </td><td>   
                <c:out value="${answer.score3}"/>
                </td>
                </tr>    
            </c:forEach>
        </table>
    </body>
</html>
