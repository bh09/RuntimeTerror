<%-- 
    Document   : Student
    Created on : 2019.06.28., 3:22:24
    Author     : rajnaig
--%>

<%@page contentType="text/html" pageEncoding="Latin2"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=Latin2">
        <%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
        <link rel="shortcut icon" href="./GeneralPictures/favicon-144.png" />
        <!--link rel="icon" href="favicon-144.png"-->
      <!--customiz�lni a message-et-->
      <title>BrainingHub for tesztfelhaszn�l�</title>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">    
    <!--script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script-->
        <!--script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script-->
        <!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <link 
        href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" 
        rel="stylesheet"  type='text/css'>
        <!--mozg� sz�zal�khoz-->
        <!--script src="https://code.jquery.com/jquery-1.10.2.js"></script-->
        
        <!--chartokhoz-->
        <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->


        <link rel="stylesheet" type="text/css" href="..\CSS\carets.css">
        <style>
        <jsp:include page=".\CSS\carets.css"/>
        <jsp:include page=".\CSS\menu_background.css"/>
        <jsp:include page=".\CSS\avatar.css"/>
        <jsp:include page=".\CSS\scrolling_text.css"/>
        <jsp:include page=".\CSS\doughnut.css"/>
        <jsp:include page=".\CSS\progressQ.css"/>
        <jsp:include page=".\CSS\progressparticipation.css"/>
        <jsp:include page=".\CSS\statistics.css"/>
        </style>
        <script>
            <jsp:include page=".\Javascript\donutcounter.js"/> 
            <jsp:include page=".\Javascript\progressQ.js"/>
            <jsp:include page=".\Javascript\progressParticipation.js"/>    
        </script>    
        <style>
         .navbar .navbar-brand img{
          height: 30px; 
    margin-top: -5px;
}            
          .dropdown-menu{right: 0;}

          

  

          </style>
    </head>
<body>
<jsp:include page="$/../../../SubPages/StudentNavbar.jsp"/>  
    

<div class="donutContainer css"style="text-align:center;">
    <!--svg width="160" height="160" xmlns="http://www.w3.org/2000/svg">
     <g>
      <title>Layer 1</title>
      <circle id="circle" style="stroke-dashoffset: 20;/* 160 of 440 */" class="donut" r="69.85699" cy="81" cx="81" stroke-width="8" stroke="#11e88d" fill="none"/>
      <text x="50%" y="50%" transform="rotate(90 95,95)" text-anchor="middle" stroke="#11e88d" stroke-width="0px" dy=".3em"fill="#11e88d"style="font-size:20px;color:11e88d">A kurzus</text>
      <text class="count" data-stop="100" x="35%" y="50%" transform="rotate(90 80,80)" text-anchor="middle" stroke="#51c5cf" stroke-width="0px" dy=".3em"fill="#11e88d"style="font-size:25px">centered!</text>
      <text  x="65%" y="50%" transform="rotate(90 80,80)" text-anchor="middle" stroke="#51c5cf" stroke-width="0px" dy=".3em"fill="#11e88d"style="font-size:25px"> %-�t</text>
      <text x="50%" y="50%" transform="rotate(90 65,65)" text-anchor="middle" stroke="#51c5cf" stroke-width="0px" dy=".3em"fill="#11e88d"style="font-size:20px">teljes�tetted</text>
    </g>
    </svg-->
</div>
<!--Ez a script �ll�tja be a k�r sz�zal�kos �rt�k�t-->
<!--script>
document.getElementById('circle').style.strokeDashoffset=440-($('.count').attr("data-stop"))/100*440;
</script>
<script>
    donutCounter();
  </script-->
<div class="statistics">
  <span style="background-color: rgb(13, 54, 106);display:block">&nbsp;</span>
   <div class="progressQ" data-amount="${displayQ}" data="${displayQ}">

    <script>
        createQGradient();
        createCountTextQ();
        </script>

    <div class="amount">
      
    </div>
    
    </div>
    <span style="background-color: rgb(13, 54, 106);display:block">&nbsp;</span>
<div class="progressparticipation" data-amount="${missedClass}" data="${missedClass}">
    
  <script>
  
  </script>
    <script>
        createParticipationGradient();
        createCountTextParticipation();
        </script>
  
  <div class="amount"></div>
  </div>
  <span style="background-color: rgb(13, 54, 106);display:block">&nbsp;</span>
</div>
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="${calendar}" style="border: 0" width="800" height="600" frameborder="0" scrolling="no" onerror="$(this).attr('src','https:\\\\accounts.google.com');"></iframe>
  </div>

    </body>
</html>
