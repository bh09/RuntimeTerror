<%-- 
    Document   : interview
    Created on : 2019.06.20., 14:27:28
    Author     : Bence58<%@page contentType="text/html" pageEncoding="UTF-8"%>
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"-->
        <title>interju ertekeles</title>
        <jsp:include page="$/../../../SubPages/StudentNavbar.jsp"/>  
    </head>
    <body>
        <h1>Interju jsp</h1>

        <div class="row d-flex justify-content-center">
            <div class="col-md-10">

                <!--   <form method="post"> </form> -->
                <div class="col-5">
                    <form name="answerForm" id="answerForm" action="StudentAnswerServlet" method="POST">

                <c:choose>
                    <c:when test="${fieldsVisible}">
                   <div class="col-12">
                    <label for="comment">Első kérdés: <span style="font-weight: bold;">${firstQuestion}</span></label>
                    <textarea type="text" class="form-control " rows="5" id="comment" name="firstQuestion" minlength="3" required></textarea>
                    
                </div>
                <br />

                <div class="col-12">
                    <label for="comment">Második kérdés:<span style="font-weight: bold;">${secondQuestion}</span></label>
                    <textarea class="form-control" rows="5" id="comment" name="secondQuestion" minlength="3" required></textarea>
                    
                </div>
                <br />
                <div class="col-12">
                    <label for="comment">Harmadik kérdés:<span style="font-weight: bold;">${thirdQuestion}</span></label>
                    <textarea class="form-control" rows="5" id="comment" name="thirdQuestion" minlength="3" required></textarea>
                    
                </div>
                <br />
                <br/>
                <div class="d-flex justify-content-end">
                    <input name="create" type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                </div>
                

            </div>
            <br/>
            <br/>
            <br/>

            </c:when>

            <c:otherwise>
        <h2>Nincs új röpdolgozat élesítve</h2>
        </c:otherwise>
    </c:choose>  


</form>
</div>
</div>
</body>
</html>

