<%-- 
    Document   : studentinterview
    Created on : 2019.07.03., 19:16:43
    Author     : Bence58
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head><head>
        <jsp:include page="$/../../../SubPages/StudentNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>interju test jsp</title>
    </head>
</head>
<body>
    <h1>Userpage Interview</h1>

    <div class="row d-flex justify-content-center">
        <div class="col-md-10"> 
            <c:choose>

                <c:when test="${interviews.isEmpty()}">


                    <div class="card border-danger col-5">

                        <div class="card-body text-danger">
                            <h5 class="card-title">Nem volt még interjúd</h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-transparent border-danger"></div>
                    </div>






                </c:when>
                <c:otherwise>



                    <div class="card-deck">
                        <c:forEach items="${interviews}"  var="interview">
                            <div class="card border-success col-5">

                                <div class="card-body text-success">
                                    <h5 class="card-title">Interjú</h5>
                                    <p class="card-text"></p>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item" >${interview.hrComment}</li>
                                    <li class="list-group-item">${interview.hrPoints}</li>
                                    <li class="list-group-item">${interview.javaComment}</li>
                                    <li class="list-group-item">${interview.javaPoints}</li>
                                </ul>

                            </div>
                        </c:forEach>
                    </div> 

          

                </c:otherwise>

            </c:choose>
        </div>



    </div>
</body>
</html>
