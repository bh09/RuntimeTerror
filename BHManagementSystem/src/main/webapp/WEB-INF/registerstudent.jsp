<%-- 
    Document   : registerstudent
    Created on : 2019.07.11., 15:11:31
    Author     : Bence58
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student Register</h1>

        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <form method="POST" class="col-4">
                    <label for="comment">Interjú:</label>
                    <select name="select"  class="form-control" required>
                        <option type="button" value="student">Diák</option>
                        <option type="button" value="teacher">Tanár</option>
                    </select>
                    <label for="pass">Ideiglenes Password:</label>
                    <input type="text" class="form-control" value="${randomPass}" id="pass" readonly><br/>
                    <input type="text" class="form-control" name="name" placeholder="Tanuló neve" required><br/>
                    <input type="email" class="form-control" name="email" placeholder="Email" required><br/>
                    <input type="number" class="form-control" name="phoneNumber" placeholder="Telefonszám" required><br/>
                    <input type="date" class="form-control" name="birthDate" placeholder="Válassz Születési évet" required><br/>
                    <input type="password" class="form-control" name="password" placeholder="Jelszó" required><br/>
                    <div class="d-flex justify-content-end">
                        <input type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                    </div>
                </form>



            </div>
        </div>
    </body>
</html>
