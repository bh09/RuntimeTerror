<%-- 
    Document   : DisplayExams
    Created on : 2019.07.11., 23:55:01
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Exams Display</title>
    </head>

    <body>
        <br>
        <div class="row d-flex justify-content-center">
           
                    <br/>
                    <h1>Vizsgák:</h1>
                    <br>
           
        </div>
        <br>
        <br>
        <div class="row d-flex justify-content-center">

            <table id="example" class="table table-striped table-bordered nowrap"  style="width:50%">
                <thead>
                    <tr>
                        <th>Dátum</th>
                        <th>Vizsgatípus</th>
                        <th>Értékelés</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${examList}" var="list">    
                        <tr>
                            <td>
                                <c:out   value="${list.getExamDate()}"/>
                            </td><td>
                                <c:out   value="${list.getExamType().getDesc()}"/>
                            </td>
                            
<!--                            <td align="center"> 
                                <form action="/BHManagementSystem/UpdateHomeworkServlet" method="post">
                                    <button name="edit"  class="btn btn-success" type="submit" value="${list.getId()}" >Szerkeszt</button>
                                </form>
                            </td>-->
                            
                            
                            <td align = "center">
                                <div class="text-left">
                                    <form action="DisplayExamsServlet" method="post" >
                                        <c:set var="hasResult" value="${false}"/>
                                        <c:forEach items="${allHomeworkResults}" var="result">
                                            <c:if test="${list.isOneExamResultPresent()}">
                                                <c:set var="hasResult" value="${true}"/>
                                            </c:if>
                                        </c:forEach>
                                     
                                        <c:choose>
                                            <c:when test="${hasResult}">
                                                <button name="evaluate" class="btn btn-warning" disabled  value="${list.getId()}"> Értékelve </button>
                                            </c:when>    
                                            <c:otherwise>
                                                <button name="evaluate" class="btn btn-warning" value="${list.getId()}"> Értékelés </button>
                                            </c:otherwise>
                                        </c:choose>

                                    </form>    
                                </div>
                            </td>
<!--                            <td align="center">
                                <div class="col-12">
                                    <form action="DisplayExamServlet" method="post" >
                                        <button name="edit"  class="btn btn-success" type="submit" value="${list.getId()}" >Szerkeszt</button>
                                    </form>
                                </div>
                            </td>-->
                        </tr>
                    </c:forEach>    
                </tbody>
            </table>

        </div>    






    </body>
</html>
