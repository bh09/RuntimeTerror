<%-- 
    Document   : addHomework
    Created on : 2019.06.27., 7:46:12
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {

                $("#datepicker").datepicker({format: 'yyyy-mm-dd'});
            });
        </script>
        <title>Update Homework</title>
    </head>




    <body background="">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="col-5">
                    <br/>
                    <h1>Házi feladat szerkesztése: </h1>
                </div>
            </div>
        </div>

        <div class="row d-flex justify-content-center">

            <div class="col-md-10">
                <form action="UpdateHomeworkServlet" method="post">
                    <br/>
                    <div class="col-5">
                        <label for="comment"><b>Házi feladat határidő:  </b></label>
                        <p><input type="text" value="${datePickerDate}" id="datepicker" name="date" autocomplete="off" required=""></p>
                    </div>
                    <input type="hidden" name="DateReleased" value="${homework.getDateReleased()}">
                    <input type="hidden" name="homeworkId" value="${homework.getId()}">

                    <div class="col-2">
                        <!--<form name="TeamForm" id="TeamForm" action="addHomework" method="POST">-->

                        <label for="comment"><b>Csoport:</b></label>

                        <!--onchange="this.form.submit();"-->
                        <select name="teamDropdown"     class="form-control" required>
                            <c:forEach items="${teamList}"  var="team">
                                <option type="button" value="${team.id}">${team.teamName}</option>
                            </c:forEach>
                        </select>
                        <br/>

                    </div>


                    <div class="col-3">
                        <label for="comment"><b>Kapható maximális pontszám:</b></label>
                        </br>
                        <select name="maxPoints">
                            <c:forEach items="${pointsDropDown}"  var="point">
                                <option type="button" value="${point.intValue()}">${point.intValue()}</option>
                            </c:forEach>
                        </select>
                    </div>


                    </br>
                    <div class="col-12">
                        <label for="comment"><b>Házi feladat címe:</b></label>
                        <br><input type="text" value="${homework.getTitle()}" name="hwtitle" required=""/><br/><br/>
                    </div>


                    <div class="col-5">
                        <label for="comment"><b>Feladat leírása:</b></label>
                        <textarea type="text" class="form-control " rows="5" id="comment" name="hwdesc" pattern=".{10,}" required>${homework.getDescription()}</textarea>

                    </div>


                    </br>
                    </br>
                    <div class="d-flex justify-content-center">
                        <input type="submit" name="modify" value="Módosít" class="btn btn-outline-success col-3">
                    </div>


                </form>
                        <br>
                        <br>
                <form action="UpdateHomeworkServlet" method="post">
                    <div class="d-flex justify-content-center">     
                        <input type="submit" name="back" value="Vissza" class="btn btn-outline-success col-3">  
                    </div>
                </form>        
            </div>



        </div>







    </body>
</html>
