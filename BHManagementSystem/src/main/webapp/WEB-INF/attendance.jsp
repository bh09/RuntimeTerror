<%-- 
    Document   : attendance
    Created on : 2019.07.08., 11:57:59
    Author     : Bence58
--%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>attendance</h1>

        <div class="row d-flex justify-content-center">
            <div class="col-md-10">

                <div>
                    <p>
                    <table class="table">
                        <thead>
                        <th>Név</th>
                        <th>Mennyit hiányzott(Perc)</th>
                        <th>Részt vett e az órán?</th>
                        </thead>
                        <c:forEach items="${listOfStudents}"  var="student">
                            <form name="${student.name}"
                                  action="AttendanceServlet"method="POST">
                                <tr >
                                    <td class="table-success"><c:out value="${student.name}"></c:out></td>
                                <input type="hidden" name="studentName" value="${student.name}"/>    
                                <input type="hidden" name="studentId" value="${student.id}"/>  
                                <td class="table-success">
                                    <input type="number" class="form-control" placeholder="Hianyzas" value="0" name="missing" required>
                                </td>
                            </form>
                            <td class="table-success">
                                <div class="d-flex justify-content-end">
                                    <input type="submit" class="btn btn-outline-success col-4" value="SUBMIT" name="studentsAttendance" onClick="document.forms.${student.name}.submit();">    
                                </div> 
                                    
                            </td>
                            </tr>

                        </c:forEach>
                    </table>
                    </p>
                </div>
         
            </div>
        </div>

    </body>
</html>
