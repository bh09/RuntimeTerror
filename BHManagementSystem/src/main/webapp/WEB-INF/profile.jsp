<%-- 
    Document   : profile
    Created on : 2019.07.13., 14:42:12
    Author     : Bence58
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Profil</h1>

        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <form method="POST" class="col-4">

                    <input type="hidden" class="form-control" name="id" value="${student.getId()}"><br/>
                    <input type="text" class="form-control" name="name" placeholder="Név" value="${student.getName()}" required><br/>
                    <input type="email" class="form-control" name="email" placeholder="Email" required><br/>
                    <input type="number" class="form-control" name="phoneNumber" placeholder="Telefonszám" value="${student.getPhoneNumber()}" required><br/>
                    <input type="date" class="form-control" name="birthDate" placeholder="Válassz Születési évet" value="${student.getBirtDate()}" required><br/>
                    <input type="password" class="form-control" name="password" placeholder="Jelszó" required><br/>
                    <input type="password" class="form-control" name="password" placeholder="Jelszó" required><br/>
                    <div class="d-flex justify-content-end">
                        <input type="submit" value="Mehet!" class="btn btn-outline-success col-4">
                    </div>
                </form>

            </div>
        </div>

    </body>
</html>
