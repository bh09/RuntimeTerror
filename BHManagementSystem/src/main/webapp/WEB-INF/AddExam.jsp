<%-- 
    Document   : AddExam
    Created on : 2019.07.09., 15:40:33
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
                $("#datepicker").attr('required', 'required');
//                $("#datepicker").attr('readonly', 'readonly');
            });
        </script>
        <title>Add Exam</title>
    </head>




    <body>
        
        <c:choose>
                <c:when test="${fieldsVisible}">
        
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="col-5">
                    <br/>
                    <h1>Vizsga időpont létrehozása: </h1>
                </div>
            </div>
        </div>

        <div class="row d-flex justify-content-center">

            <div class="col-md-10">
                <form action="AddExamServlet" method="post">
                    <br/>
                    <div class="col-2">
                        <label for="comment">Vizsga időpontja:</b></label><br/>
                        <input type="text" id="datepicker" name="date" autocomplete="off" class="form-control" required>  <!--readonly-->
                    </div>
                    <br>
                    <!--                    <div class="col-2">
                                            <label for="comment">Csoport:</b></label>
                                            <select name="teamDropdown"     class="form-control" required>
                    <c:forEach items="${teamList}"  var="team">
                        <option type="button" value="${team.id}">${team.teamName}</option>
                    </c:forEach>
                </select>
                <br/>
            </div>-->

                    <div class="col-2">
                        <label for="comment">Vizsga típusa:</b></label>
                        <br>
                        <select name="examType" class="form-control" required>
                            <option type="button" value="ZH1">Első ZH</option>
                            <option type="button" value="ZH2">Második ZH</option>
                            <option type="button" value="FINAL_EXAM">Záróvizsga</option>
                        </select>
                    </div>
                    <div>
                        <br/>
                        <div>
                            <div class="col-md-10">
                                <br>    
                                <br>
                                <input type="submit" onclick="return confirm('Kérlek ellenőrizd a beírt adatokat!')" name="create" value="Létrehoz" class="btn btn-outline-success col-4">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            </c:when>
                <c:otherwise>
                    <br>
                    <h3><center>Válassz csoportot</center></h3>
                        </c:otherwise>
                    </c:choose>
        </div>
    </body>
</html>
