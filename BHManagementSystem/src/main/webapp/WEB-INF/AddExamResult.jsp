<%-- 
    Document   : AddExamResult
    Created on : 2019.07.12., 13:30:59
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%--<jsp:useBean id="${ExamResultMap}" class="java.util.HashMap" scope="page"/>--%>

<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="..\CSS\fadeout.css" rel="stylesheet" type="text/css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script language="JavaScript">
            function toggle(source) {
                checkboxes = document.getElementsByName('foo');
                for (var i = 0, n = checkboxes.length; i < n; i++) {
                    checkboxes[i].checked = source.checked;
                }
            }
        </script>
        <script>
            window.onload = function () {
                window.setTimeout(fadeout, 1500); //8 seconds
            }

            function fadeout() {
                document.getElementById('fadeout').style.opacity = '0';
            }

        </script>
        <script>

            function myRoundFunction(x) {
                return Math.round(x) * 100 / 100;
            }

        </script>


        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Add Exam Results</title>
    </head>

    <body>
        <br>
        <div class="row d-flex justify-content-center">
                    <br/>
                    <h1>Értékelés</h1>
                    <br>
        </div>
        <br>
        <br>


        <div class="row d-flex justify-content-center">


            <c:choose>
                <c:when test="${exam.getExamType().getDesc().equals('Záróvizsga')}">
                    <table id="example" class="table table-striped table-bordered nowrap"  style="width:70%">
                    </c:when>
                    <c:otherwise>
                        <table id="example" class="table table-striped table-bordered nowrap"  style="width:60%">

                        </c:otherwise>
                    </c:choose>




                    <thead>
                        <tr>
                            <th>Tanuló</th>
                            <th>Elmélet pontszám (Max 30)</th>
                            <th>Százalék</th>
                            <th>Gyakorlat pontszám (Max 30)</th>
                            <th>Százalék</th>
                                <c:if test="${exam.getExamType().getDesc().equals('Záróvizsga')}">
                                <th>Csapatmunka pontszám (Max 30)</th>
                                <th>Százalék</th>
                                </c:if>
                            <th></th>
                        </tr>
                    </thead>


                    <tbody>


                        <c:forEach items="${studentList}" var="student">

                            <tr>
                                <td>
                                    <form id="${student.getId()}" action="AddExamResultServlet" method="post">
                                        <c:out value="${student.getName()}"/>
                                    </form>    
                                </td>
                                <c:choose>
                                    <c:when test="${(ExamResultMap.get(student.getId()).getResultPointsTheory()< 18) && (ExamResultMap.get(student.getId()).getResultPointsTheory() >= 1)}">
                                        <td class="table-danger">
                                        </c:when>  
                                        <c:otherwise>
                                        <td>
                                        </c:otherwise>
                                    </c:choose>


                                    <div class="form-group row">
                                        <label for="example-number-input" class="col-2 col-form-label"></label>
                                        <div class="col-5">
                                            <input required form="${student.getId()}" step=0.25 min="0" max="30" class="form-control" type="number" value="${ExamResultMap.get(student.getId()).getResultPointsTheory()}" name="theoryPoints" id="theoryPoints">
                                        </div>
                                    </div>
                                </td>

                                <c:choose>
                                    <c:when test="${(ExamResultMap.get(student.getId()).getResultPointsTheory()< 18) && (ExamResultMap.get(student.getId()).getResultPointsTheory() >= 1)}">
                                        <td class="table-danger">
                                        </c:when>  
                                        <c:otherwise>
                                        <td>
                                        </c:otherwise>
                                    </c:choose>

                                    ${Math.round(ExamResultMap.get(student.getId()).getResultPointsTheory()/30*100)} %




                                </td>

                                <c:choose>
                                    <c:when test="${(ExamResultMap.get(student.getId()).getResultPointsJava()< 18) && (ExamResultMap.get(student.getId()).getResultPointsJava() >= 1)}">
                                        <td class="table-danger">
                                        </c:when>  
                                        <c:otherwise>
                                        <td>
                                        </c:otherwise>
                                    </c:choose>
                                    <div class="form-group row">
                                        <label for="example-number-input" class="col-2 col-form-label"></label>
                                        <div class="col-5">
                                            <input required form="${student.getId()}" step=0.25 min="0" max="30" class="form-control" type="number" value="${ExamResultMap.get(student.getId()).getResultPointsJava()}" name="javaPoints" id="javaPoints">
                                        </div>
                                    </div>
                                </td>

                                <c:choose>
                                    <c:when test="${(ExamResultMap.get(student.getId()).getResultPointsJava()< 18) && (ExamResultMap.get(student.getId()).getResultPointsJava() >= 1)}">
                                        <td class="table-danger">
                                        </c:when>  
                                        <c:otherwise>
                                        <td>
                                        </c:otherwise>
                                    </c:choose>
                                    
                                            
                                            ${Math.round(ExamResultMap.get(student.getId()).getResultPointsJava()/30*100)} %





                                </td>






                                <c:if test="${exam.getExamType().getDesc().equals('Záróvizsga')}">
                                    <c:choose>
                                        <c:when test="${(ExamResultMap.get(student.getId()).getResultPointsTeamwork()< 18) && (ExamResultMap.get(student.getId()).getResultPointsTeamwork() >= 1)}">
                                            <td class="table-danger">
                                            </c:when>  
                                            <c:otherwise>
                                            <td>
                                            </c:otherwise>
                                        </c:choose>
                                        <!--<label for="example-number-input" class="col-2 col-form-label"></label>-->
                                        <div class="col-5">
                                            <input required form="${student.getId()}" step=0.25 min="0" max="30" class="form-control" type="number" value="${ExamResultMap.get(student.getId()).getResultPointsTeamwork()}" name="teamworkPoints" id="teamworkPoints">
                                        </div>
                                    </td>
                                    <c:choose>
                                        <c:when test="${(ExamResultMap.get(student.getId()).getResultPointsTeamwork()< 18) && (ExamResultMap.get(student.getId()).getResultPointsTeamwork() >= 1)}">
                                            <td class="table-danger">
                                            </c:when>  
                                            <c:otherwise>
                                            <td>
                                            </c:otherwise>
                                        </c:choose>
                                        ${Math.round(ExamResultMap.get(student.getId()).getResultPointsTeamwork()/30*100)} %

                                    </td>
                                </c:if>


                                <td>
                                    <div class="col-5">
                                        <button type="submit" form="${student.getId()}" value="${student.getId()}" name="evaluate" class="btn btn-success">Értékel</button>
                                        <!--<div id="fadeout">Elmentve !</div>-->
                                    </div>
                                </td>


                            </tr>

                        </c:forEach>

                    </tbody>
                </table>
                <div class="col-md-10">
                    <br>
                    <br>
                    <form action="DisplayExamsServlet" method="post">
                        <div class="d-flex justify-content-end">     
                            <input type="submit" name="back" value="Vissza" class="btn btn-outline-success col-3">  
                        </div>
                    </form>        
                </div>


        </div>    






    </body>
</html>

