<%-- 
    Document   : AnswersTeacher
    Created on : 2019.07.13., 18:29:31
    Author     : rajnaig
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Röpdolgozatok Értékelése</title>
        <%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>

        <table class="table table-striped" name="uncheckedTable">
            <th>Hallgató</th>
                 <th>Dátum</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
            <c:forEach items="${uncheckedList}" var="answer">
                <form name="${answer.key.id}" action="TeacherAnswerServlet" method="post">
                <tr class="table-danger">
                    <td>
                    <c:out value="${answer.key.student.name}"  />
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.fireQuestion.toLocalDateTime().toLocalDate()}"/>
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.firstQuestion}"/>
                    </td>
                     <td>
                    <c:out value="${answer.key.answer1}"/>
                    </td>
                    <td>
                        <select class="form-control"name="resultPoints1_${answer.key.id}">
                        <c:forEach items="${answer.value.get(0)}" var="points">
                        <option values="${points}">${points}</option>    
                        </c:forEach>    
                    </td>
                    <td>
                    <input class="form-control" type="text" name="firstComment" value="${answer.key.comment1}">
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.secondQuestion}"/>
                    </td>
                     <td>
                    <c:out value="${answer.key.answer2}"/>
                    </td>
                    <td>
                        <select class="form-control"name="resultPoints2_${answer.key.id}">
                        <c:forEach items="${answer.value.get(1)}" var="points">
                        <option values="${points}">${points}</option>    
                        </c:forEach>    
                    </td>
                    <td>
                    <input class="form-control" type="text" name="secondComment" value="${answer.key.comment2}">
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.thirdQuestion}"/>
                    </td>
                     <td>
                    <c:out value="${answer.key.answer3}"/>
                    </td>
                    <td>
                        <select class="form-control" name="resultPoints3_${answer.key.id}">
                        <c:forEach items="${answer.value.get(2)}" var="points">
                        <option values="${points}">${points}</option>    
                        </c:forEach>    
                    </td>
                    <td>
                    <input class="form-control" type="text" name="thirdComment" value="${answer.key.comment3}">
                    </td>
                    <td>
                     <button class="btn btn-success btn-sm" type="text" name="create" value="${answer.key.id}" onClick="document.forms.${answer.key.id}.submit()"><i class="far fa-check-circle"></i></button>   
                    </td>    
            </tr>
            </form>
                    
        </c:forEach>
        </table>
   
        <!--form name="limitForm" action="TeacherAnswerServlet" method="post">
            <p>Az elsö <span><input type="number" name="limitTable" id="limitTable" min="1" value="${limit}" required style="width:100%;max-width:80px "></span>találat<span><button type="text" value="Mehet!" onchange="document.forms.limitForm.submit()" class="btn btn-outline-success btn-sm"></button></span></p-->

            <table class="table table-striped" name="checkedTable">
                <th>Hallgató</th>
                 <th>Dátum</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                 <th>Kérdés</th>
                 <th>Válasz</th>
                 <th>Pont</th>
                 <th>Megjegyzés</th>
                <c:forEach items="${checkedList}" var="answer">
                <form name="${answer.key.id}" action="TeacherAnswerServlet" method="post">
                <tr class="table-info">
                    <td>
                    <c:out value="${answer.key.student.name}"  />
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.fireQuestion.toLocalDateTime().toLocalDate()}"/>
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.firstQuestion}"/>
                    </td>
                     <td>
                    <c:out value="${answer.key.answer1}"/>
                    </td>
                    <td>
                        <select class="form-control"name="resultPoints1_${answer.key.id}">
                        <c:forEach items="${answer.value.get(0)}" var="points">
                        <option values="${points}">${points}</option>    
                        </c:forEach>    
                    </td>
                    <td>
                    <input type="text" name="firstComment" value="${answer.key.comment1}"class="form-control">
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.secondQuestion}"/>
                    </td>
                     <td>
                    <c:out value="${answer.key.answer2}"/>
                    </td>
                    <td>
                        <select class="form-control"name="resultPoints2_${answer.key.id}">
                        <c:forEach items="${answer.value.get(1)}" var="points">
                        <option values="${points}">${points}</option>    
                        </c:forEach>    
                    </td>
                    <td>
                    <input type="text"class="form-control" name="secondComment" value="${answer.key.comment2}">
                    </td>
                    <td>
                    <c:out value="${answer.key.questions.thirdQuestion}"/>
                    </td>
                     <td>
                    <c:out value="${answer.key.answer3}"/>
                    </td>
                    <td>
                        <select class="form-control" name="resultPoints3_${answer.key.id}">
                        <c:forEach items="${answer.value.get(2)}" var="points">
                        <option values="${points}">${points}</option>    
                        </c:forEach>    
                    </td>
                    <td>
                    <input type="text" class="form-control"name="thirdComment" value="${answer.key.comment3}">
                    </td>
                    <td>
                     <button class="btn btn-success btn-sm" type="text" name="create" value="${answer.key.id}" onClick="document.forms.${answer.key.id}.submit()"><i class="far fa-check-circle"></i></button>   
                    </td>    
            </tr>
            </form>
                    
        </c:forEach>
        </table>

    </body>
</html>
