<%-- 
    Document   : DisplayHomeworks
    Created on : 2019.06.27., 16:09:54
    Author     : Dell7720
--%>

<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<title>jQuery UI Datepicker - Default functionality</title>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>
        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Homework Display</title>
    </head>

    <body>
        <c:choose>
                <c:when test="${fieldsVisible}">
        
        <br>
        <div class="row d-flex justify-content-center">
            
                    <br/>
                    <h1 class="text-center">Házi feladatok listázása </h1>
                    <br>
            
        </div>
        <br>
        <br>
        <div class="row d-flex justify-content-center">

            <table id="example" class="table table-striped table-bordered nowrap"  style="width:60%">
                <thead>
                    <tr>
                        <th>Házi feladat címe</th>
                        <th>Feladás dátuma</th>
                        <th>Leadási határidő</th>
                        <th>Csoport</th>
                        <th>Kapható pontszám</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${allHomweworks}" var="list">    
                        <tr>
                            <td>
                                <c:out   value="${list.getTitle()}"/>
                            </td><td>
                                <c:out   value="${list.getDateReleased()}"/>
                            </td><td>
                                <c:out   value="${list.getDeadline()}"/>
                            </td><td>
                                <c:out   value="${list.getTeam().getTeamName()}"/>
                            </td><td>
                                <c:out   value="${list.getMaxPoints()}"/>
                            </td><td align="center"> 
                                <form action="DisplayHomeworkServlet" method="post">
                                    <button name="edit"  class="btn btn-success" type="submit" value="${list.getId()}" >Szerkeszt</button>
                                </form>
                            </td><td align = "center">
                                <div class="col-12">
                                    <form action="DisplayHomeworkServlet" method="post" >
                                        <c:set var="hasResult" value="${false}"/>
                                        <c:forEach items="${allHomeworkResults}" var="result">
                                            <c:if test="${result.getHomework().getId() eq list.getId()}">
                                                <c:set var="hasResult" value="${true}"/>
                                            </c:if>
                                        </c:forEach>
                                     <!--Ezt itt meg kell változtatni RG-->
                                        <c:choose>
                                            
                                            <c:when test="${hasResult}">
<!--                                                <button name="evaluate" class="btn btn-warning" value="${list.getId()}"> Értékel </button>-->
                                                <button name="evaluate" class="btn btn-secondary" value="${list.getId()}"> Értékelve </button
                                            </c:when>    
                                            <c:otherwise>
                                                <button name="evaluate" class="btn btn-warning" value="${list.getId()}"> Értékel </button>
                                            </c:otherwise>
                                        </c:choose>

                                    </form>    
                                </div>
                            </td><td align="center">
                                <div class="col-12">
                                    <form action="DisplayHomeworkServlet" method="post" >
                                        <button name="delete" class="btn btn-danger" onclick="return confirm('Biztos benne hogy törli?')" value="${list.getId()}"> Töröl </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>    
                </tbody>
            </table>

            </c:when>
                <c:otherwise>
                    <br>
                    <h3><center>Válassz csoportot</center></h3>
                        </c:otherwise>
                    </c:choose>
        </div>    






    </body>
</html>