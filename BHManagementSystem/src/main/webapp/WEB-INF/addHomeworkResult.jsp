<%-- 
    Document   : addHomeworkResult
    Created on : 2019.07.03., 10:24:53
    Author     : Dell7720
--%>

<%@page import="javax.naming.InitialContext"%>
<%@page import="hu.braininghub.BHManagementSystem.service.HomeworkResultInterface"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="$/../../SubPages/TeacherNavbar.jsp"/> 
        <%!
            HomeworkResultInterface homeworkResults;
        %>
        <%

            homeworkResults = (HomeworkResultInterface) new InitialContext().lookup("java:global/BHManagementSystem-1.0-SNAPSHOT/HomeworkResultBean!hu.braininghub.BHManagementSystem.service.HomeworkResultInterface");
        %>  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            function confirmComplete() {
                alert("confirmComplete");
                var answer = confirm("Are you sure you want to continue");
                if (answer === true)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        </script>

        <script>

            $("input .myclass").click(function () {
                var value = $(this).val();
            });

        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>

        <!--        <script>
                    $(function () {
                        $("#datepicker").datepicker();
                    });
                </script>-->
        <script>
            $(function ()
                    $("#myTable").DataTable({
            responsive: true;
            }
            ;
            );
        </script>
        <title>Homework Display</title>
    </head>


    <body>
        <br>
        <div class="row d-flex justify-content-center">
            
                    <br>
                    <h1>Házi feladat értékelése</h1>
                    <br>
            
        </div>
        <br>
        <br>
        <form action="AddHomeworkResultServlet" method="post">
            <div class="row d-flex justify-content-center">
                        <input name="homeworkId" type="hidden" value="${homeworkId}">
                        <table id="example" class="table table-striped table-bordered nowrap"  style="width:50%">
                            <thead>
                                <tr>
                                    <th>Tanuló</th>
                                    <th>Pontszám</th>
                                    <th>Értékelés</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${pointsDropdowns}" var="student">
                                    <c:set var="studentid" value="${student.key.id}"/>

                                    <%request.setAttribute("resultExists", homeworkResults.isHomeworkResultExists(pageContext.getAttribute("studentid").toString(), (String) session.getAttribute("homeworkId")));%>
                                    <c:choose>
                                        <c:when test="${resultExists}"> 
                                            <tr class="table-success">
                                            </c:when>    
                                            <c:otherwise>
                                            <tr class="table-danger">        
                                            </c:otherwise>        
                                        </c:choose>    
                                        <td>

                                            <c:out value="${student.key.name}"  />
                                        </td><td>
                                            <select name="resultPoints_${student.key.id}">

                                                <c:forEach items="${student.value}"var="point">
                                                    <option values="${point}">${point}</option>
                                                </c:forEach> 

                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit" value="${student.key.id}" name="create" class="btn btn-light">Értékel</button>    
                                            </td>    
                                        </tr>
                                    </c:forEach>    
                                </tbody>
                            </table>
                    <br>
                    <br>

                </div>
            </form>
            <div class="col-md-10">
                <br>
                <br>
                <form action="UpdateHomeworkServlet" method="post">
                    <div class="d-flex justify-content-end">     
                        <input type="submit" name="back" value="Vissza" class="btn btn-outline-success col-3">  
                    </div>
                </form>        
            </div>
        </body>
    </html>
