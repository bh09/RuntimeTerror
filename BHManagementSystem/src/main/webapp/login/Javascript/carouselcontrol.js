function controlCarousel1(){
    $(document).ready(function(){
        // Activate Carousel
        $("#carouselControl").carousel({interval:2000});
          
        // Enable Carousel Indicators
        $(".item1").click(function(){
          $("#carouselControl").carousel(0);
        });
        $(".item2").click(function(){
          $("#carouselControl").carousel(1);
        });
        $(".item3").click(function(){
            $("#carouselControl").carousel(2);
          });
          $(".item4").click(function(){
            $("#carouselControl").carousel(3);
          });
          $(".item5").click(function(){
            $("#carouselControl").carousel(4);
          });    
        // Enable Carousel Controls
        $(".left").click(function(){
          $("#carouselControl").carousel("prev");
        });
        $(".right").click(function(){
          $("#carouselControl").carousel("next");
        });
      });

}
function controlCarousel2(){
    $(document).ready(function(){
        // Activate Carousel
        $("#carouselControl2").carousel({interval:2000});
          
        // Enable Carousel Indicators
        $(".item1").click(function(){
          $("#carouselControl2").carousel(0);
        });
        $(".item2").click(function(){
          $("#carouselControl2").carousel(1);
        });
        $(".item3").click(function(){
            $("#carouselControl2").carousel(2);
          });
          $(".item4").click(function(){
            $("#carouselControl2").carousel(3);
          });
          $(".item5").click(function(){
            $("#carouselControl2").carousel(4);
          });
          $(".item6").click(function(){
            $("#carouselControl2").carousel(5);
          });    
        // Enable Carousel Controls
        $(".left").click(function(){
          $("#carouselControl2").carousel("prev");
        });
        $(".right").click(function(){
          $("#carouselControl2").carousel("next");
        });
      });

}