function createMailNavBar(){
        var head=[
            '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">',
                  
            '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js">'+'<'+'/script>',
                    '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js">'+'</script>',  
            ].join("\n")
            $("head").append(head);
            
            var html = [
                '<div class="mail">',
                '<nav class="navbar navbar-expand-lg navbar-light bg-light">',
                    '<div class="navbar-text">+36 70 703 1933 |"</div>',
                    '<div class="navbar-nav">',
                    '<div class="nav-item">',
                        '<a class="nav-link" href="mailto:hello@braininghub.com"> hello@braininghub.com</a>',
                    '</div>',
                  '</div>',
              '</nav>',
          '</div>',
            ].join("\n");
            
           
            $("body").append(html);
}