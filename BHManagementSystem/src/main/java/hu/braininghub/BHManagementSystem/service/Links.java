/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class Links {
public String getURLStudent(String inputButton,String servletName){

        
if("home".equals(inputButton)&&!"StudentServlet".equals(servletName)){
    return  "StudentServlet";

    }else if("homework".equals(inputButton)){
        return "";
    }else if("quiz".equals(inputButton)&&!"StudentAnswerServlet".equals(servletName)){
    return "StudentAnswerServlet";
    }else if("homeworkpoints".equals(inputButton)&&!"DisplayHomeworkStudent".equals(servletName)){
    return "DisplayHomeworkStudent";
    }else if("zhpoints".equals(inputButton)&&!"DisplayExamsStudent".equals(servletName)){
    return "DisplayExamsStudent";
    }else if("quizpoints".equals(inputButton)&&!"StudentQuizResultServlet".equals(servletName)){
    return "StudentQuizResultServlet";
    }else if("interview".equals(inputButton)&&!"UserInterview".equals(servletName)){
    return "UserInterview";    
    }else if("profil".equals(inputButton)){
    return "";    
    }else if("quit".equals(inputButton)){
    return "Quit";    
    }else{
    return null;    
    }     
}
public String getURLTeacher(String inputButton,String servletName){

        
if("home".equals(inputButton)&&!"TeacherServlet".equals(servletName)){
    return  "TeacherServlet";

    }else if("attendance".equals(inputButton)&&!"AttendanceServlet".equals(servletName)){
        return "AttendanceServlet";
    }else if("attendancesearch".equals(inputButton)&&!"AttendanceSearch".equals(servletName)){
    return "AttendanceSearch";
    }else if("homework".equals(inputButton)&&!"HomeworkServlet".equals(servletName)){
        return "HomeworkServlet";
    } else if ("examedit".equals(inputButton)&&!"DisplayExamsServlet".equals(servletName)) {
        return "DisplayExamsServlet";
    } else if ("exam".equals(inputButton)&&!"AddExamServlet".equals(servletName)) {
    return "AddExamServlet";
    } else if ("examresult".equals(inputButton)&&!"AddExamResultServlet".equals(servletName)) {
    return "AddExamResultServlet";
    } else if("quiz".equals(inputButton)&&!"QuizQuestionServlet".equals(servletName)){
    return "QuizQuestionServlet";
    }else if("creategroup".equals(inputButton)&&!"CreateGroup".equals(servletName)){
    return "CreateGroup";   
    }else if("homeworkedit".equals(inputButton)&&!"DisplayHomeworkServlet".equals(servletName)){
    return "DisplayHomeworkServlet";    
    }else if("quizedit".equals(inputButton)&&!"TaskListServlet".equals(servletName)){
     return "TaskListServlet";
    }else if("homeworkpoints".equals(inputButton)){
    return "";
    }else if("quizpoints".equals(inputButton)&&!"TeacherAnswerServlet".equals(servletName)){
    return "TeacherAnswerServlet";    
    }else if("interview".equals(inputButton)&&!"Interview".equals(servletName)){
    return "Interview";    
    }else if("registration".equals(inputButton)&&!"RegisterStudent".equals(servletName)){
    return "RegisterStudent";  
    }else if("profil".equals(inputButton)){
    return "";    
    }else if("quit".equals(inputButton)){
    return "Quit";    
    }else{
    return null;    
    }         
}    
}
