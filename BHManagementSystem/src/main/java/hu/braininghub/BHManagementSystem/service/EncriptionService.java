package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.util.Encriptor;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.apache.commons.codec.binary.Hex;

@LocalBean
@Singleton
public class EncriptionService {

    public String getEncriptedPass(String original) {

        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Encriptor.class.getName()).log(Level.SEVERE, null, ex);
        }
        m.update(original.getBytes(Charset.forName("UTF8")));
        byte[] resultByte = m.digest();

        return new String(Hex.encodeHex(resultByte));
    }

    public String getDefaultPassword() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }

}
