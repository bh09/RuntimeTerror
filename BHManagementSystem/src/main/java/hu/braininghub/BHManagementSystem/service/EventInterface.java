/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.EventDTO;
import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.Event;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author rajnaig
 */
public interface EventInterface {
public List<EventDTO> getEventsForTeamID(Long teamID);
public void createEvent(Event event);
public String getEventsForTeamIDAndConvertToString(Long teamID);
public void deleteEventByExpiry(Timestamp expiry);
public void deleteEventByTaskID(Long taskID);
}
