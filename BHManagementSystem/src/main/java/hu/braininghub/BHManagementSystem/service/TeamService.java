package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Greg Takacs
 */
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class TeamService {

    private static final Logger log = LoggerFactory.getLogger(TeamService.class);

    @EJB
    TeamRepository teamRepository;

    public List<TeamDTO> loadTeamDropDown() {

        List<TeamDTO> dtoList = new ArrayList<>();
        List<Team> sourceList = teamRepository.getTeam();
        for (Team team : sourceList) {
            dtoList.add(getTeamDtoFromEntity(team));
        }

        return dtoList;
    }

    public List<TeamDTO> getorderedTeamDropDownByTeamId(String teamId) {

        List<TeamDTO> dtoList = loadTeamDropDown();

        TeamDTO currentTeamDto = dtoList
                .stream()
                .filter(dto -> dto.getId()
                .equals(Parser.parseStringToLong(teamId)))
                .findFirst()
                .get();
        dtoList.remove(currentTeamDto);
        dtoList.add(0, currentTeamDto);
        return dtoList;
    }

    public Team getTeamByIdService(String s) {

        return teamRepository.getTeamByTeamID(Long.parseLong(s));

    }

    public TeamDTO getTeamDtoFromEntity(Team team) {

        return new TeamDTO(team.getId(), team.getTeamName(), team.getGoogleCalendarURL());

    }

    public List<Team> getAllTeams() {
        return teamRepository.getTeam();
    }

    public List<Team> getTeams() {
        return teamRepository.getTeam();

    }

    public void createTeam(TeamDTO dTO) {

        try {
            teamRepository.addTeam(Mappers.mapEntityFromTeamDTO(dTO));
        } catch (InvocationTargetException ex) {
            log.error("createTeam: {}", ex);
        } catch (IllegalAccessException ex) {
            log.error("createTeam: {}", ex);
        }

    }

}
