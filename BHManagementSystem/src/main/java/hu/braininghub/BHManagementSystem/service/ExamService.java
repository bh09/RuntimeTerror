package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.exam.ExamDTO;
import hu.braininghub.BHManagementSystem.entities.exam.Exam;
import hu.braininghub.BHManagementSystem.entities.exam.ExamType;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.repositories.exam.ExamRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.time.LocalDate;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class ExamService {

    @EJB
    ExamRepository examRepository;
    
    @EJB
    TeamRepository teamRepository;
    
    @EJB(name = "ejb/students", description = "stateless")
    StudentInterface studentRervice;
    
    
    

    public List<ExamDTO> getAllExamDTOs() {
        return Mappers.mapExamListToDtoList(examRepository.getAllExams());
    }

    public List<ExamDTO> getAllExamDTOsByTeamId(String teamId) {
        Long teamIdLong = Long.parseLong(teamId);
        return Mappers.mapExamListToDtoList(examRepository.getAllExamsByTeamId(teamIdLong));
    }

    public List<ExamDTO> getAllExamDTOsByExamType(String examType) {
        ExamType type = ExamType.valueOf(examType);
        return Mappers.mapExamListToDtoList(examRepository.getAllExamsByExamType(type));
    }
    
    public ExamDTO getExamDTOByTeamId(String examId) {
        Long examIdLong = Long.parseLong(examId);
        return Mappers.mapSingleExamToExamDTO(examRepository.getExamByExamId(examIdLong));
    }
    
    public List<StudentDTO> getAllStudentsByExamId (String examId) {
        Long examIdLong = Long.parseLong(examId);
        Long teamIdLong = examRepository.getExamByExamId(examIdLong).getTeam().getId();
        return studentRervice.getAllStudentsOfATeam(teamIdLong);
    }
    
    
    
    

    public void saveNewExamEntity(String date, String teamId, String examType) {
        examRepository.addExam(createExamEntity(date, teamId, examType));
    }

    public Exam createExamEntity(String date, String teamId, String examType) {
        LocalDate localDate = Parser.stringToLocaldate(date);
        Long teamIDLong = Parser.parseStringToLong(teamId);
       
        return new Exam(Parser.parseLocalDateToSqlDate(localDate),
                ExamType.valueOf(examType), 
                teamRepository.getTeamByTeamID(teamIDLong));
    
    }

}
