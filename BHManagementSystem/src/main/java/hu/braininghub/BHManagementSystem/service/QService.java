/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.AnswersDTO;
import hu.braininghub.BHManagementSystem.dto.InterviewDTO;
import hu.braininghub.BHManagementSystem.dto.exam.ExamResultDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkResultDto;
import hu.braininghub.BHManagementSystem.entities.exam.ExamType;
import hu.braininghub.BHManagementSystem.util.DoubleConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class QService {
@EJB
ExamService examService;
@EJB
ExamResultService examResultService;
@EJB
HomeworkResultService homeworkResultService;
@EJB
AnswersService answersService;
@EJB
InterviewService interviewService;
public Double getExamResultsForStudent(String studentId){
List<ExamResultDTO> examResultsList=examResultService.getAllExamResultDTOsByStudentId(studentId);
    System.out.println("resultlist"+examResultsList);
List<ExamResultDTO> firstExamList=examResultsList.stream()
        .filter(results->results.getExamDTO().getExamType().equals(ExamType.ZH1))
        .collect(Collectors.toList());
    System.out.println("firstExamList"+firstExamList);
double firstExamTheoryResult=highestPassedAverageTheory(firstExamList);
double firstExamJavaResult=highestPassedAverageJava(firstExamList);
    if (firstExamTheoryResult<0.01||firstExamJavaResult<0.01) {
    return 0.0;    
    }
double firstExamResult=((firstExamTheoryResult+firstExamJavaResult)/2)*DoubleConstants.EXAMQPERCENTAGE.getConstantValue();
List<ExamResultDTO> secondExamList=examResultsList.stream()
        .filter(results->results.getExamDTO().getExamType().equals(ExamType.ZH2))
        .collect(Collectors.toList());
double secondExamTheoryResult=highestPassedAverageTheory(secondExamList);
double secondExamJavaResult=highestPassedAverageJava(secondExamList);
if (secondExamTheoryResult<0.01||secondExamJavaResult<0.01) {
    return firstExamResult;    
    }
double secondExamResult=((secondExamTheoryResult+secondExamJavaResult)/2)*DoubleConstants.EXAMQPERCENTAGE.getConstantValue();
List<ExamResultDTO> thirdExamList=examResultsList.stream()
        .filter(results->results.getExamDTO().getExamType().equals(ExamType.FINAL_EXAM))
        .collect(Collectors.toList());
double thirdExamTheoryResult=highestPassedAverageTheory(thirdExamList);
double thirdExamJavaResult=highestPassedAverageJava(thirdExamList);
double thirdExamTeamworkResult=highestPassedAverageTeamwork(thirdExamList);
if (thirdExamTheoryResult<0.01||thirdExamJavaResult<0.01||thirdExamTeamworkResult<0.01) {
    return firstExamResult+secondExamResult;    
    }
return firstExamResult+secondExamResult+(((thirdExamTheoryResult+thirdExamJavaResult+thirdExamTeamworkResult)/3)*DoubleConstants.EXAMQPERCENTAGE.getConstantValue());
}
public double highestPassedAverageTheory(List<ExamResultDTO> results){
    double max=0.0;
    for (ExamResultDTO result : results) {
    if((result.getResultPointsTheory()/result.getExamDTO().getMaxPopintsTheory())>=DoubleConstants.PASSINGPERCENT.getConstantValue()){        
        double current=(result.getResultPointsJava()/result.getExamDTO().getMaxPointsJava()+result.getResultPointsTheory()/result.getExamDTO().getMaxPopintsTheory())/2;
            if(current>max){
                max=current;
            }
    
    }    
    }
    return max;
}
public double highestPassedAverageJava(List<ExamResultDTO> results){
 double max=0.0;
    for (ExamResultDTO result : results) {
    if((result.getResultPointsJava()/result.getExamDTO().getMaxPointsJava())>=DoubleConstants.PASSINGPERCENT.getConstantValue()){      
            double current=result.getResultPointsJava()/result.getExamDTO().getMaxPointsJava();
            if(current>max){
                max=current;
            } 
        }
        }
    return max;    
}
public double highestPassedAverageTeamwork(List<ExamResultDTO> results){
 double max=0.0;
    for (ExamResultDTO result : results) {
    if((result.getResultPointsTeamwork()/result.getExamDTO().getMaxPointsTeamwork())>=DoubleConstants.PASSINGPERCENT.getConstantValue()){      
            double current=result.getResultPointsTeamwork()/result.getExamDTO().getMaxPointsTeamwork();
            if(current>max){
                max=current;
            } 
        }
        }
    return max;    
}
public double getHomeworkResultForStudents(String studentId){
   List<HomeworkResultDto> results=homeworkResultService.getAllHomeWorkResultDtosOfAStudent(studentId);
   double homeworkresultSum=0; 
    for (int i = 0; i < results.size(); i++) {
        homeworkresultSum=homeworkresultSum+(results.get(i).getResultPoints()/results.get(i).getHomework().getMaxPoints());
    }
    return (homeworkresultSum/results.size())*DoubleConstants.HOMEWORKQPERCENTAGE.getConstantValue();
}
public double getQuizResultForStudents(Long studentID){
    List<AnswersDTO> answers=answersService.getAllAnswersForAStudent(studentID);
    double answerSum=0.0;
    for (AnswersDTO answer : answers) {
        answerSum=answerSum+((answer.getScore1()+answer.getScore2()+answer.getScore3())/3);
    }
    return (answerSum/answers.size())*DoubleConstants.QUIZQPERCENTAGE.getConstantValue();
}
public double getInterviewResultForStudent(Long studentID){
    List<InterviewDTO> interviews=interviewService.getInterviewListByStudentId(studentID);
    
    if (interviews.isEmpty()) {
        return 0.0;
    }else if(interviews.size()==1){
       return ((interviews.get(0).getHrPoints()+interviews.get(0).getJavaPoints())/10)*(DoubleConstants.INTERVIEWQPERCENTAGE.getConstantValue()/2);
    }else{
        return ((interviews.get(0).getHrPoints()+interviews.get(0).getJavaPoints())/10)*(DoubleConstants.INTERVIEWQPERCENTAGE.getConstantValue()/2)+(((interviews.get(1).getHrPoints()+interviews.get(1).getJavaPoints())/10)*(DoubleConstants.INTERVIEWQPERCENTAGE.getConstantValue()/2));
    }
}
public double calculateQForStudent(Long studentID){
return getExamResultsForStudent(studentID.toString())+getHomeworkResultForStudents(studentID.toString())+getQuizResultForStudents(studentID)+getInterviewResultForStudent(studentID);
}
}
