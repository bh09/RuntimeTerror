/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class TeacherNavbarBean implements TeacherNavbarInterface {
@EJB(name="ejb/teams", description="stateless")
TeamInterface teams;

    public TeacherNavbarBean(TeamInterface teams) {
        this.teams = teams;
    }

    public TeacherNavbarBean() {
    }


public List<TeamDTO> addChooseTeam(){
    List<TeamDTO>team=teams.getAllTeams();
    TeamDTO teamDTO=new TeamDTO();
            teamDTO.setTeamName("Válassz csoportot");
            teamDTO.setId(-1L);
            team.add(0, teamDTO);
    return team;
}
public List<TeamDTO> getSelectedTeam(Long teamID,List<TeamDTO> teamDTOs){ 
        TeamDTO team=teamDTOs.stream()
                .filter(t -> t.getId().equals(teamID))
            .findFirst()
            .get();
        teamDTOs.remove(team);
        teamDTOs=teamDTOs.stream()
                .sorted(Comparator.comparing(t->t.getId()))
                .collect(Collectors.toList());
        teamDTOs.add(0,team);
        return teamDTOs;
}
}
