/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeacherDTO;
import hu.braininghub.BHManagementSystem.repositories.TeacherRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class TeacherBean implements TeacherInterface {
@EJB
TeacherRepository teacherRepository;

    public TeacherBean(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public TeacherBean() {
    }

@Override
public TeacherDTO getTeacherByTeacherID(Long teacherID){
    return Mappers.mapTeacherFromEntity(teacherRepository.getTeacherByTeacherID(teacherID));
}    
}
