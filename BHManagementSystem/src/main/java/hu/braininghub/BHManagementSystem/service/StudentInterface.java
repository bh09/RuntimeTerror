/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import java.util.List;

/**
 *
 * @author rajnaig
 */
public interface StudentInterface {
 public StudentDTO getStudentByStudentID(Long studentID);
 public List<StudentDTO> getAllStudentsOfATeam(Long teamId);
}
