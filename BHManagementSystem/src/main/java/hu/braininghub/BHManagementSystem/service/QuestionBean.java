
package hu.braininghub.BHManagementSystem.service;


import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.repositories.quiz.QuestionRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author rajnaig
 */
@Stateless
public class QuestionBean {
@EJB 
QuestionRepository questionRepository;

    public QuestionBean(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public QuestionBean() {
    }

public void createQuestion(String firstQuestion,String secondQuestion,String thirdQuestion,TeamDTO teamDTO){

    try {
        questionRepository.createQuestion(firstQuestion,secondQuestion,thirdQuestion,Mappers.mapEntityFromTeamDTO(teamDTO));
    } catch (IllegalAccessException | InvocationTargetException ex) {
        Logger.getLogger(QuestionBean.class.getName()).log(Level.SEVERE, null, ex);//Loggert ide
    }

    
}
/*public boolean questionExistsForClassDay(Long classDayID){
    return questionRepository.getQuestionByClassDay(classDayID).isPresent();
}*/
public List<QuestionsDTO> getAllQuestionsForTeams(Long teamID){
    return questionRepository.getQuestionsForTeam(teamID).stream()
                .map(quest->Mappers.mapQuestionsFromEntity(quest))
                .collect(Collectors.toList());  
}
public Long getQuestionsForTeamLastIndex(Long teamID){
    Long questionID=questionRepository.getQuestionsForTeamLastIndex(teamID);
    return  (questionID==null) ? 0L : questionID;
}
public boolean isAddedToDatabase(Long teamID,Long previousTeamID){
    return !previousTeamID.equals(getQuestionsForTeamLastIndex(teamID));
}
 public List<QuestionsDTO> searchQuestionsForTeam(Long teamID,String searchText){
     return questionRepository.searchQuestionsForTeam(teamID, searchText).stream()
                .map(quest->Mappers.mapQuestionsFromEntity(quest))
                .collect(Collectors.toList());
 }
 public void overwriteQuestion(Long questionID,String firstQuestion, String secondQuestion,String thirdQuestion){
     questionRepository.overwriteQuestion(questionID, firstQuestion, secondQuestion, thirdQuestion);
 }
 public void deleteQuestionByQuestionID(Long questionID){
     questionRepository.deleteQuestionByQuestionID(questionID);
 }
 public void fireQuestion(Long questionID){
     Timestamp fireQuestion=Timestamp.valueOf(LocalDateTime.now().plusMinutes(30));
     questionRepository.fireQuestion(questionID, fireQuestion);
 }
 public boolean isQuestionFired(Long questionID){
     return questionRepository.isQuestionAlreadyFired(questionID);
 }
 public QuestionsDTO getQuestionForQuestionID(Long questionID){
     return Mappers.mapQuestionsFromEntity(questionRepository.getQuestionByQuestionID(questionID));
 }
}
