/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import java.util.List;

/**
 *
 * @author rajnaig
 */
public interface TeamInterface {
public TeamDTO getTeamByID(Long teamID);
public List<TeamDTO> getAllTeams();
}
