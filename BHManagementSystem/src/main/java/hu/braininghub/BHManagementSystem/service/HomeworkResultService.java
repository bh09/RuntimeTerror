package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkDto;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkResultDto;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.homework.HomeworkResult;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.repositories.homework.HomeworkResultRepository;
import hu.braininghub.BHManagementSystem.repositories.homework.HomeworkRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class HomeworkResultService {

    @EJB
    HomeworkResultRepository homeworkResultRepository;

    @EJB
    StudentRepo studentRepo;

    @EJB
    HomeworkRepository homeworkRepository;

    @EJB(name="ejb/students", description="stateless")
    StudentInterface studentService;

    @EJB
    HomeworkService homeworkService;

    public List<HomeworkResultDto> getAllHomeWorkResultDtos() {
        List<HomeworkResultDto> dtoList = new ArrayList<>();
        List<HomeworkResult> sourceList = homeworkResultRepository.getAllHomeworkResults();

        for (HomeworkResult hr : sourceList) {
            HomeworkResultDto dto = new HomeworkResultDto(
                    Mappers.mapStudentFromEntity(hr.getStudent()),
                    Mappers.mapHomeworkDtofromEntity(hr.getHomework()),
                    hr.getResultPoints()
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public List<HomeworkResultDto> getAllHomeWorkResultDtosOfATeam(String teamId) {
        Long teamIdLong = Parser.parseStringToLong(teamId);
        List<HomeworkResultDto> dtoList = new ArrayList<>();
        List<HomeworkResult> sourceList = homeworkResultRepository.getAllHomeworkResults();
        List<HomeworkResult> allResultsByTeam = sourceList
                .stream()
                .filter(p -> p.getStudent().getTeam().getId().equals(teamIdLong))
                .collect(Collectors.toList());

        for (HomeworkResult hr : allResultsByTeam) {
            HomeworkResultDto dto = new HomeworkResultDto(
                    Mappers.mapStudentToStudentDTO(hr.getStudent()),
                    Mappers.mapHomeworkDtofromEntity(hr.getHomework()),
                    hr.getResultPoints()
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public List<HomeworkResultDto> getCustomHomeWorkResultDtosByStudentIdAndHomeworkId(String studentId, String homeworkId) {
        System.out.println("hwService homeworkId: " + homeworkId);
        System.out.println("hwService studentid: " + studentId);
        List<HomeworkResultDto> dtoList = new ArrayList<>();
        List<HomeworkResult> sourceList = homeworkResultRepository.getCustomHomeworkResult(studentId, homeworkId);

        for (HomeworkResult hr : sourceList) {
            HomeworkResultDto dto = new HomeworkResultDto(
                    Mappers.mapStudentToStudentDTO(hr.getStudent()),
                    Mappers.mapHomeworkDtofromEntity(hr.getHomework()),
                    hr.getResultPoints()
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public Integer calculateSumOfResultPoints(List<HomeworkResultDto> list) {
        return list.stream().mapToInt(p -> (int) p.getResultPoints()).sum();
    }

    public Integer calculateSumOfMaxPoints(List<HomeworkDto> list) {
        return list.stream().mapToInt(p -> (int) p.getMaxPoints()).sum();
    }

    public List<HomeworkResultDto> filterResultListByHomeworkId(List<HomeworkResultDto> source, String homeworkId) {
        List<HomeworkResultDto> sourcelist = source;
        Long homeworkIdLong = Parser.parseStringToLong(homeworkId);

        List<HomeworkResultDto> filteredList = sourcelist
                .stream()
                .filter(p -> p.getHomework().getId().equals(homeworkIdLong))
                .collect(Collectors.toList());
        return filteredList;
    }

    public List<HomeworkResultDto> filterResultListByStudentId(List<HomeworkResultDto> source, String StudentId) {
        List<HomeworkResultDto> sourcelist = source;
        Long studentIdLong = Parser.parseStringToLong(StudentId);

        List<HomeworkResultDto> filteredList = sourcelist
                .stream()
                .filter(p -> p.getStudentDTO().getId().equals(studentIdLong))
                .collect(Collectors.toList());
        return filteredList;
    }

    public List<HomeworkResultDto> getResultList(String teamId, String studentId, String homeworkId) {

        List<HomeworkResultDto> homeworkResultsCustomList;

        if ("-1".equalsIgnoreCase(homeworkId) && "-1".equalsIgnoreCase(studentId)) {
            homeworkResultsCustomList = getAllHomeWorkResultDtosOfATeam(teamId);                //all record by team
        } else if (!"-1".equalsIgnoreCase(studentId) && "-1".equalsIgnoreCase(homeworkId)) {  //by student, all homeworkIds
            homeworkResultsCustomList = getAllHomeWorkResultDtosOfAStudent(studentId);
        } else if ("-1".equalsIgnoreCase(studentId) && !"-1".equalsIgnoreCase(homeworkId)) {  //all students, by homeworkId
            homeworkResultsCustomList = filterResultListByHomeworkId(getAllHomeWorkResultDtosOfATeam(teamId), homeworkId);
        } else {
            homeworkResultsCustomList = filterResultListByStudentId(filterResultListByHomeworkId(getAllHomeWorkResultDtosOfATeam(teamId), homeworkId), studentId);
        }
        return homeworkResultsCustomList;
    }

    public boolean checkStudentIdCoherency(String teamId, String studentId) {
        Long teamIdLong = Parser.parseStringToLong(teamId);
        Long studentIdLong = Parser.parseStringToLong(studentId);
        System.out.println("a szerviceben van a studentId koherencia vizsgálattal");

        if ("-1".equals(studentId)) {
            return true;
        } else if (studentService.getStudentByStudentID(studentIdLong).getTeam().getId().equals(teamIdLong)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean checkHomeworkIdCoherency(String teamId, String homeworkId) {
        Long teamIdLong = Parser.parseStringToLong(teamId);
        Long homeworkIdLong = Parser.parseStringToLong(homeworkId);

        if ("-1".equals(homeworkId)) {
            return true;
        } else if (homeworkService.getHomeworkDtoById(homeworkIdLong).getTeam().getId().equals(teamIdLong)) {
            return true;
        } else {
            return false;
        }
    }

    public List<StudentDTO> getStudentDropdownOfATeam(String teamId) {
        Long teamIdLong = Parser.parseStringToLong(teamId);
        List<StudentDTO> studentDropdown = constructStudetnDropdown(teamIdLong); //adding the "ALL" menu element too
        return studentDropdown;
    }

    public List<StudentDTO> getOrderedStudentDropdown(String teamId, String studentId) {
        Long studentIdLong = Parser.parseStringToLong(studentId);

        //if not present, set studentId to -1L (=meaning ALL)
        if (!checkIfStudentPresentInTeam(studentIdLong, teamId)) {
            studentIdLong = -1L;
        }

        List<StudentDTO> orderedStudentDropdown
                = orderStudentDropDownById(getStudentDropdownOfATeam(teamId), studentIdLong);
        return orderedStudentDropdown;
    }

    public List<StudentDTO> orderStudentDropDownById(List<StudentDTO> list, Long studentIdLong) {
        List<StudentDTO> sourceList = list;
        StudentDTO currentStudentDTO = sourceList.
                stream().filter(p -> p.getId().equals(studentIdLong)).findFirst().get();
        sourceList.remove(currentStudentDTO);
        sourceList.add(0, currentStudentDTO);
        return sourceList;
    }
    
    public Boolean checkIfTeamHasHomeworkIdPresent (Long homeworkIdLong, String teamId) {
        List<HomeworkDto> homeworkList = homeworkService.getAllHomeworkDtosByTeamId(Parser.parseStringToLong(teamId));
        boolean isHomeworkPresentWithId = homeworkList
                .stream()
                .filter(p -> p.getId().equals(homeworkIdLong))
                .findFirst()
                .isPresent();
        if (isHomeworkPresentWithId) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean checkIfStudentPresentInTeam(Long studentIdLong, String teamId) {
        List<StudentDTO> studentList = getStudentDropdownOfATeam(teamId);
        boolean isStudentPresentWithId = studentList
                .stream()
                .filter(p -> p.getId().equals(studentIdLong))
                .findFirst()
                .isPresent();
        if (isStudentPresentWithId) {
            return true;
        } else {
            return false;
        }
    }

    public List<StudentDTO> constructStudetnDropdown(Long teamIdLong) {
        List<StudentDTO> studentList = studentService.getAllStudentsOfATeam(teamIdLong);
        StudentDTO menuItem = new StudentDTO();
        menuItem.setName("Összes");
        menuItem.setId(-1L);
        studentList.add(0, menuItem);
        return studentList;
    }

    public List<HomeworkResultDto> getAllHomeWorkResultDtosOfAStudent(String studentId) {
        Long studentIdLong = Parser.parseStringToLong(studentId);
        List<HomeworkResultDto> dtoList = new ArrayList<>();
        List<HomeworkResult> sourceList = homeworkResultRepository.getAllHomeworkResultsofStudentId(studentIdLong);
        for (HomeworkResult hr : sourceList) {
            HomeworkResultDto dto = new HomeworkResultDto(
                    Mappers.mapStudentToStudentDTO(hr.getStudent()),
                    Mappers.mapHomeworkDtofromEntity(hr.getHomework()),
                    hr.getResultPoints()
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public List<HomeworkResultDto> getAllHomeWorkResultDtosByHomeworkId(String homeworkId) {
        Long homeworkIdLong = Parser.parseStringToLong(homeworkId);
        List<HomeworkResultDto> dtoList = new ArrayList<>();
        List<HomeworkResult> sourceList = homeworkResultRepository.getAllHomeworkResulsByHomeworkId(homeworkIdLong);
        for (HomeworkResult hr : sourceList) {
            HomeworkResultDto dto = new HomeworkResultDto(
                    Mappers.mapStudentFromEntity(hr.getStudent()),
                    Mappers.mapHomeworkDtofromEntity(hr.getHomework()),
                    hr.getResultPoints()
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public void addNewHomeWorkResult(Long studentId, Long homeworkId, int points) {
        Student student = studentRepo.getStudentEntityById(studentId);
        Homework homework = homeworkRepository.getHomeworkById(homeworkId);
        HomeworkResult homeworkResult = new HomeworkResult(student, homework, points);
        homeworkResultRepository.addHomeworkResult(homeworkResult);
    }

    public void addNewHomeworkResultsFromList(List<HomeworkResult> homeworkResults) {
        for (HomeworkResult hr : homeworkResults) {
            homeworkResultRepository.addHomeworkResult(hr);
        }
    }
    
    public void deleteAllHomeworkResultByHomeworkId (String homeworkId) {
        List<HomeworkResult> homeworkResults = homeworkResultRepository
                .getAllHomeworkResulsByHomeworkId(Parser.parseStringToLong(homeworkId));
        for (HomeworkResult homeworkResult : homeworkResults) {
            homeworkResultRepository.removeHomeworkResult(homeworkResult);
        }
    } 

    public double getAllHomeworkResultPointsOfAStudent(String studentId) {
        double sumOfResultPoints = homeworkResultRepository
                .getAllHomeworkResultsofStudentId(Long.parseLong(studentId))
                .stream()
                .mapToInt(p -> p.getResultPoints())
                .sum();
        return sumOfResultPoints;
    }
    
    public double getAllHomeworMaxPointsOfAStudent(String studentId) {
        double sumOfMaxPoints = homeworkResultRepository
                .getAllHomeworkResultsofStudentId(Long.parseLong(studentId))
                .stream()
                .mapToInt(p -> p.getHomework().getMaxPoints())
                .sum();
        return sumOfMaxPoints;
    }
    
    public double getHomeWorkResultPercentageOfAStudent (String studentId) {
        
        return getAllHomeworkResultPointsOfAStudent(studentId)/
                getAllHomeworMaxPointsOfAStudent(studentId)
                *100;
    }
    
    
    
    public boolean isHomeworkResultExists(String studentID,String homeworkID){
    if(homeworkResultRepository.getCustomHomeworkResult(studentID, homeworkID).size()==0)
        return false;
    else
        return true;
    }
    public void updateHomeworkResult(Integer points,Long studentID,Long homeworkID){
        homeworkResultRepository.updateHomeworkResult(points,studentID,homeworkID);
    }
}
