/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import java.util.List;

/**
 *
 * @author rajnaig
 */
public interface TeacherNavbarInterface {
public List<TeamDTO> addChooseTeam();
public List<TeamDTO> getSelectedTeam(Long teamID,List<TeamDTO> teamDTOs);
}
