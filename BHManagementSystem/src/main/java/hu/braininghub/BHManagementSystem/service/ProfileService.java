package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.util.Mappers;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class ProfileService {

    @EJB
    StudentRepo sr;

    public StudentDTO getStudentById(long studentid) {
        return Mappers.mapStudentTodSTudentDtoCustom(sr.getStudentEntityById(studentid));
    }

    public void updateStudentProfile(StudentDTO stdent) {
        Student student = sr.getStudentByStudentID(stdent.getId());

        sr.updateStudent(Mappers.mapStudentDTOToStudent(stdent, student));
    }

}
