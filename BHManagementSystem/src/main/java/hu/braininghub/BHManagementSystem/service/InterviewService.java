package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.InterviewDTO;
import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.Interview;
import hu.braininghub.BHManagementSystem.entities.events.EventType;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.InterviewRepo;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.util.Mappers;

import java.util.Comparator;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class InterviewService {

    private static final Logger log = LoggerFactory.getLogger(InterviewService.class);

    @EJB
    StudentRepo sr;

    @EJB
    InterviewRepo ir;


    @EJB(name = "ejb/events", description = "stateless")
    EventInterface eventBean;


    public InterviewService() {
    }

    public InterviewService(StudentRepo sr, InterviewRepo ir) {
        this.sr = sr;
        this.ir = ir;
    }

    public void addInterview(Interview interv) {
        log.info("addInterview: interview: {}", interv);
        ir.addInterView(interv);

    }

    public void addStudentIdToTheInterview(int studentId, Interview interv) {
        log.info("addStudentIdToTheInterview: student_id: {}, interview: {}", studentId, interv);
        Long studentID=(long)studentId;
        interv.setStudent(sr.getStudentByStudentID(studentID));
        ir.addInterView(interv);
        //ir.addStudentIdToTheCorrectInterview(studentId, interv.getId());

        eventBean.createEvent(EventType.INTERVIEW.createEvent(Timestamp.valueOf(LocalDateTime.now().plusDays(3)),interv ));
        


    }

    public void updateStudentInterviewCulomn(int studentId, int interviewNumber) {
        log.info("updateStudentInterviewCulomn: student_id: {}, interviewNumber: {}", studentId, interviewNumber);
        ir.updateStudentInterviewColumn(interviewNumber, studentId);
    }

    public List<StudentDTO> getStudents(Long teamId) {
        log.info("getStudents: tema_id: {}", teamId);
        return sr.getStudentByTeamId(teamId).stream().map(p -> Mappers.mapStudentToStudentDTO((Student) p)).collect(Collectors.toList());

    }


    

    public StudentDTO getStudent(Long sudentId) {
        log.info("getStudent: studentID: {}", sudentId);
        return Mappers.mapStudentToStudentDTO(ir.getStudentByStudentID(sudentId));
    }

    
    public List<InterviewDTO> getInterviewListByStudentId(long sudentId) {
        List<Interview> interviewList = ir.getInterviewByStudentId(sudentId);
        log.info("getInterviewListByStudentId: interviewList: {}", interviewList);

        if (interviewList != null) {

            return Mappers.mapInterviewToInterviewDTO(interviewList.stream().sorted(Comparator.comparing(Interview::getId)).collect(Collectors.toList()));
        } else {
            return Collections.emptyList();
        }
    }

}
