/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.repositories.homework.HomeworkResultRepository;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class HomeworkResultBean implements HomeworkResultInterface{
@EJB
HomeworkResultRepository homeworkResultRepository;
public boolean isHomeworkResultExists(String studentID,String homeworkID){
    return !homeworkResultRepository.getCustomHomeworkResult(studentID, homeworkID).isEmpty();
    }    
}
