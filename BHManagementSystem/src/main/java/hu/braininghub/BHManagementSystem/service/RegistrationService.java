package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.entities.user.Teacher;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.repositories.TeacherRepository;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class RegistrationService {
    
    @EJB
    StudentRepo sr;
    
    @EJB
    TeacherRepository tr;
    
   public void addStudent(Student student){
   
       sr.addStudent(student);
       
   }
   
   public void addTeacher(Teacher teacher){
       tr.addTeacher(teacher);
   
   }

}
