/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.EventDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.Event;
import hu.braininghub.BHManagementSystem.entities.events.ImportanceType;
import hu.braininghub.BHManagementSystem.repositories.eventrepository.EventRepository;
import hu.braininghub.BHManagementSystem.repositories.quiz.QuestionRepository;
import hu.braininghub.BHManagementSystem.util.DateFactory;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class EventBean implements EventInterface {
    @EJB
EventRepository eventRepository;

    public EventBean() {
    }

    public EventBean(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public List<EventDTO> getEventsForTeamID(Long teamID){
    List<Event> eventList=eventRepository.getEventsForTeamID(teamID);
    eventList=privateEventsAfterExpiry(eventList);
    eventList=orderEventsByImportance(eventList);
    return eventList.stream()
            .map(evenT->Mappers.mapEventDTOFromEntity(evenT))
            .collect(Collectors.toList());
}
    @Override
    public String getEventsForTeamIDAndConvertToString(Long teamID){
    List<Event> eventList=eventRepository.getEventsForTeamID(teamID);
    eventList=privateEventsAfterExpiry(eventList);
    eventList=orderEventsByImportance(eventList);
    StringBuffer eventString=new StringBuffer();
        for (Event event : eventList) {
        eventString.append(event.getMessage());
        eventString.append(" ");
        eventString.append(event.getExpiry().toLocalDateTime().getYear());
        eventString.append("-");
        eventString.append(DateFactory.getDate(event.getExpiry().toLocalDateTime().getMonth().getValue()));
        eventString.append("-");
        eventString.append(event.getExpiry().toLocalDateTime().getDayOfMonth());
        eventString.append(" ");
        eventString.append(formatTimes(event.getExpiry().toLocalDateTime().getHour()));
        eventString.append(":");
        eventString.append(formatTimes(event.getExpiry().toLocalDateTime().getMinute()));
        eventString.append(" ");
        }
    return eventString.toString();
    }
    @Override
    public void createEvent(Event event){
    eventRepository.addEvent(event);
}
public List<Event> privateEventsAfterExpiry(List<Event>eventList){
removeExpiredEvents(eventList);
return eventList.stream()
        .filter(event->event.getExpiry().after(Timestamp.valueOf(LocalDateTime.now())))
        .collect(Collectors.toList());
}
private void removeExpiredEvents(List<Event> eventList){
eventList.stream().
        filter(event->event.getExpiry().before(Timestamp.valueOf(LocalDateTime.now())))
        .forEach(evenT->eventRepository.deleteEvent(evenT.getId()));
}
    @Override
    public void deleteEventByExpiry(Timestamp expiry){
    eventRepository.deleteEventByExpiry(expiry);
}
    @Override
    public void deleteEventByTaskID(Long taskID){
    eventRepository.deleteEventByTaskID(taskID);
}
public String formatTimes(int time){
    if(time<10){
    return ("0"+time);
    }else{
    return ""+time;
    }
}
public List<Event>orderEventsByImportance(List<Event> eventlist){
List<Event> urgentEvent=getUrgentEvents(eventlist);
List<Event> importantEvent=getImportantEvents(eventlist);
List<Event> generalEvent=getGeneralEvents(eventlist);
List<Event> newEventList=new ArrayList<>();
    for (Event event : urgentEvent) {
        newEventList.add(event);
    }
    for (Event event : importantEvent) {
        newEventList.add(event);
    }
    for (Event event : generalEvent) {
        newEventList.add(event);
    }
return newEventList;    
}
public List<Event>getUrgentEvents(List<Event> eventlist){
return eventlist.stream().filter(event->event.getImportance().equals(ImportanceType.URGENT.toString())).collect(Collectors.toList());
}
public List<Event>getImportantEvents(List<Event> eventlist){
return eventlist.stream().filter(event->event.getImportance().equals(ImportanceType.IMPORTANT.toString())).collect(Collectors.toList());
}
public List<Event>getGeneralEvents(List<Event> eventlist){
return eventlist.stream().filter(event->event.getImportance().equals(ImportanceType.GENERAL.toString())).collect(Collectors.toList());
}
}
