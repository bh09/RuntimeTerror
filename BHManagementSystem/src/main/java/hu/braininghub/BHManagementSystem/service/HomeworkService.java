package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkDto;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.EventType;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.homework.HomeworkResult;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.repositories.homework.HomeworkRepository;
import hu.braininghub.BHManagementSystem.repositories.homework.HomeworkResultRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class HomeworkService {

    @EJB
    HomeworkRepository homeworkrepository;

    @EJB
    TeamRepository teamRepository;
    
    @EJB
    HomeworkResultService homeworkResultService;
     @EJB(name = "ejb/events", description = "stateless")
    EventInterface eventBean;
    @EJB
    HomeworkResultRepository homeworkResultRepository;
    public List<HomeworkDto> getAllHomeworkDtos() {
        List<HomeworkDto> dtoList = new ArrayList<>();
        List<Homework> sourceList = homeworkrepository.getAllHomework();
        for (Homework hw : sourceList) {
            HomeworkDto dto = new HomeworkDto(
                    hw.getId(),
                    hw.getTitle(),
                    hw.getDescription(),
                    hw.getMaxPoints(),
                    hw.getDateReleased().toLocalDate(),
                    hw.getDeadline().toLocalDate(),
                    getTeamDtoFromEntity(hw.getTeam())
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public TeamDTO getTeamDtoFromEntity(Team team) {
        return new TeamDTO(team.getId(), team.getTeamName(), team.getGoogleCalendarURL());
    }

    public HomeworkDto getHomeworkDtoFromEntity(Homework hw) {
        System.out.println("bejött a getHomeworkDtoFromEntity metódusba ");
        return new HomeworkDto(hw.getId(),
                hw.getTitle(),
                hw.getDescription(),
                hw.getMaxPoints(),
                hw.getDateReleased().toLocalDate(),
                hw.getDeadline().toLocalDate(),
                getTeamDtoFromEntity(hw.getTeam()));
    }
    
    public List<HomeworkDto> getHomeworkDtosByTeamId(String Id) {
        Long teamId = Parser.parseStringToLong(Id);
        return homeworkrepository.getAllHomeworkByTeamId(teamId)
                .stream()
                .map(homework -> Mappers.mapHomeworkDtofromEntity(homework))
                .collect(Collectors.toList());
    }

    public List<HomeworkDto> getAllHomeworkDtosByTeamId(Long teamId) {
        return homeworkrepository.getAllHomeworkByTeamId(teamId)
                .stream()
                .map(homework -> Mappers.mapHomeworkDtofromEntity(homework))
                .collect(Collectors.toList());
    }
    
    public List<HomeworkDto> getAllHomeworkDtosOfATeam(Team team) {
        List<HomeworkDto> dtoList = new ArrayList<>();
        List<Homework> sourceList = homeworkrepository.getAllHomeWorkOfTeam(team);

        for (Homework hw : sourceList) {
            HomeworkDto dto = new HomeworkDto(
                    hw.getId(),
                    hw.getTitle(),
                    hw.getDescription(),
                    hw.getMaxPoints(),
                    hw.getDateReleased().toLocalDate(),
                    hw.getDeadline().toLocalDate(),
                    getTeamDtoFromEntity(hw.getTeam())
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public List<HomeworkDto> getAllHomeworkDtosOfAStudent(Student student) {

        List<HomeworkDto> dtoList = new ArrayList<>();
        List<Homework> sourceList = homeworkrepository.getAllHomeworkofStudent(student);
        for (Homework hw : sourceList) {
            HomeworkDto dto = new HomeworkDto(
                    hw.getId(),
                    hw.getTitle(),
                    hw.getDescription(),
                    hw.getMaxPoints(),
                    hw.getDateReleased().toLocalDate(),
                    hw.getDeadline().toLocalDate(),
                    getTeamDtoFromEntity(hw.getTeam())
            );
            dtoList.add(dto);
        }
        return dtoList;
    }

    public HomeworkDto getHomeworkDtoById(Long id) {
        System.out.println("bejön az első függvénybe");
        Homework homework = homeworkrepository.getHomeworkById(id);
        System.out.println("lefutott az első függvény");
        HomeworkDto dto = getHomeworkDtoFromEntity(homework);
        System.out.println("lefutott a harmadik függvény");
        return dto;
    }

    public void updateHomeworkDbRecord(Homework homework) {
        eventBean.deleteEventByTaskID(homework.getId());
        Timestamp timestamp=Timestamp.valueOf(LocalDateTime.of(homework.getDeadline().toLocalDate(),LocalTime.now()));
        eventBean.createEvent(EventType.HOMEWORKEDIT.createEvent(timestamp, homework));
        homeworkrepository.updateHomework(homework);
    }
    
    public void deleteHomeworkDBRecord (String homeworkId) {
        Homework homework=homeworkrepository.getHomeworkById(
                        Long.parseLong(homeworkId));
        removeHomeworkResultEvents(homework);
        eventBean.deleteEventByTaskID(Long.parseLong(homeworkId));
        homeworkrepository.removeHomwork(
                homework
                );
    }
    

    public Homework createHomeworkEntityToBeMerged(Long id, String releaseDate, String deadline, String title, Team team, String desc, int maxpoints) {
        
        Homework hw = new Homework();
        hw.setId(id);
        hw.setDateReleased(Date.valueOf(stringToLocalDateNormal(releaseDate)));
        hw.setDeadline(Date.valueOf(stringToLocaldate(deadline)));
        hw.setTitle(title);
        hw.setTeam(team);
        hw.setDescription(desc);
        hw.setMaxPoints(maxpoints);
        return hw;
    }
    
    public List<TeamDTO> orderTeamDTOs (List<TeamDTO> dtos, String teamId) {
    
        TeamDTO currentTeamDto = dtos
                .stream()
                .filter(dto -> dto.getId()
                .equals(parseStringToLong(teamId)))
                .findFirst()
                .get();
        dtos.remove(currentTeamDto);
        dtos.add(0, currentTeamDto);
        return dtos;
    
    }
    
        public List<HomeworkDto> getOrderedHomeworkDropdown (String teamId, String homeworkId) {
            Long homeworkIdLong = Parser.parseStringToLong(homeworkId);
            
            //if not present, set studentId to -1L (=meaning ALL)
        if (!homeworkResultService.checkIfTeamHasHomeworkIdPresent(homeworkIdLong, teamId)) {
            homeworkIdLong = -1L;
        }
            List<HomeworkDto> dtoList = getAllHomeworkDtosByTeamId(Parser.parseStringToLong(teamId));
            
            HomeworkDto menuItem = new HomeworkDto();
            menuItem.setId(-1L);
            menuItem.setTitle("Összes");
            dtoList.add(menuItem);
            
            HomeworkDto currentHomeworkDto = getCurrentHomeworkDto(dtoList, homeworkIdLong);
            
            dtoList.remove(currentHomeworkDto);
            dtoList.add(0, currentHomeworkDto);
            
            return dtoList;
    }
        
        public HomeworkDto getCurrentHomeworkDto (List<HomeworkDto> source, Long homeworkId) {
            HomeworkDto result;
            List<HomeworkDto> sourceList = source;
            HomeworkDto currentHomeworkDto = sourceList
                    .stream()
                    .filter(dto -> dto.getId()
                    .equals(homeworkId))
                    .findFirst()
                    .get();
            sourceList.remove(currentHomeworkDto);
            sourceList.add(0, currentHomeworkDto);
            return currentHomeworkDto;
            
        }

    public LocalDate stringToLocaldate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return localDate;
    }
    
    public LocalDate stringToLocalDateNormal (String date) {
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return localDate;
    }
    
    public String getDatePickerFormatFromLocalDate (LocalDate localdate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        String result = localdate.format(formatter).toString();
        return result;
    }

    public int getNumberOfHomeworksinDb() {
        return (int) getAllHomeworkDtos().stream().count();
    }

    public Long parseStringToLong(String s) {
        Long resultLong = Long.parseLong(s);
        return resultLong;
    }

    public int parseStringToInt(String s) {
        int resultInt = Integer.parseInt(s);
        return resultInt;
    }

    public void createHomeworkRecord(LocalDate date, Team team, String title, String hwDesc, int maxPoints) {
        Homework homework=new Homework(title, hwDesc, maxPoints, date, team);
        homeworkrepository.addHomework(homework);
        Timestamp timestamp=Timestamp.valueOf(LocalDateTime.of(date,LocalTime.now()));
        eventBean.createEvent(EventType.HOMEWORK.createEvent(timestamp,homework));
    }
    
    public List<Integer> createHomeworkResultPointsDropDown (String homeworkId) {
        Integer homeworkMaxPoint = getHomeworkDtoById(parseStringToLong(homeworkId)).getMaxPoints();
        List<Integer> dropdown = new ArrayList<>();
        for (int i = 0; i <= homeworkMaxPoint; i++) {
            dropdown.add((Integer) i);
        }
        return dropdown;
    }
    
    public List<Integer> createPointsDropDownBySelectedPoint (int point) {
        
        List<Integer> dropdown = new ArrayList();
        dropdown.add(Integer.valueOf(point));
        for (int i = 1; i <= 5; i++) {
            if (i != point) {
                dropdown.add(Integer.valueOf(i));
            } 
        }
        return dropdown;
    
    }
public Map<StudentDTO,List<Integer>> createPointsDropDownByStudentID (List<StudentDTO> students,String homeworkID) {
    Map<StudentDTO,List<Integer>> studentsAndDropdowns=new LinkedHashMap<StudentDTO,List<Integer>>();
    List<HomeworkResult> homeworkList=new ArrayList<>();
    List<List<Integer>> dropdowns=new ArrayList<>();
    Integer homeworkMaxPoint = getHomeworkDtoById(parseStringToLong(homeworkID)).getMaxPoints();
    for (int i = 0; i < students.size(); i++) {
        List<HomeworkResult> tempList=homeworkResultRepository.getCustomHomeworkResult(students.get(i).getId().toString(), homeworkID);
        if (tempList.size()==0) {
        homeworkList.add(null);    
        }else{
        homeworkList.add((homeworkResultRepository.getCustomHomeworkResult(students.get(i).getId().toString(), homeworkID)).get(0));
        }
        }
    for (int i = 0; i < homeworkList.size(); i++) {
        if (homeworkList.get(i)==null) {
        dropdowns.add(createPointsDropDownBySelectedPointAndMaxPoint(0,homeworkMaxPoint));   
        }else{
        dropdowns.add(createPointsDropDownBySelectedPointAndMaxPoint(homeworkList.get(i).getResultPoints(),homeworkMaxPoint));
        }
        }
    for (int i = 0; i < students.size(); i++) {
    studentsAndDropdowns.put(students.get(i), dropdowns.get(i));
    }
 
        return studentsAndDropdowns;
    
    }
public List<Integer> createPointsDropDownBySelectedPointAndMaxPoint (int point, Integer maxPoint) {
        
        List<Integer> dropdown = new ArrayList();
        dropdown.add(Integer.valueOf(point));
        for (int i = 0; i <=maxPoint; i++) {
            if (i != point) {
                dropdown.add(Integer.valueOf(i));
            } 
        }
        return dropdown;
    
    }
public void removeHomeworkResultEvents(Homework homework){
    List<HomeworkResult>results=homework.getHomeworkResults();
    for (HomeworkResult result : results) {
        eventBean.deleteEventByTaskID(result.getId());
    }
}
}
