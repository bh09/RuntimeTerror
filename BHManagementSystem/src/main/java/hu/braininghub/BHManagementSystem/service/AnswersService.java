/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.AnswersDTO;
import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.repositories.quiz.AnswerRepository;
import hu.braininghub.BHManagementSystem.repositories.quiz.QuestionRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class AnswersService {
@EJB
AnswerRepository answerRepository;
@EJB
QuestionRepository questionRepository;
@EJB
StudentRepo studentRepository;
public void createAnswerForStudent(String answer1,String answer2,String answer3,Long questionID,Long studentID){
Questions question=questionRepository.getQuestionByQuestionID(questionID);
Student student=studentRepository.getStudentByStudentID(studentID);
answerRepository.createAnswerForStudent(answer1, answer2, answer3, question, student);
}
public boolean isAnswerExistsForQuestionAndStudent(Long questionID,Long studentID){
    return answerRepository.isAnswerExistsForQuestionAndStudent(questionID, studentID);
}
public List<AnswersDTO> getAllAnswersForATeam(Long teamID){
    List<AnswersDTO> answersDTO=new ArrayList<>();
    List<Student> students=studentRepository.getStudentByTeamId(teamID);
    for (Student student : students) {
    List<Answers> answers=student.getAnswerList();
        for (Answers answer : answers) {
        answersDTO.add(Mappers.mapAnswersFromEntity(answer));
        }
    }
    return answersDTO;
}
public List<AnswersDTO> getAllAnswersForAStudent(Long studentID){

    return answerRepository.getAllAnswersForAStudent(studentID)
            .stream()
            .map(answer->Mappers.mapAnswersFromEntity(answer))
            .collect(Collectors.toList());
}
public void updateAnswers(Double score1,Double score2,Double score3,String comment1,String comment2,String comment3,Long answerID){
    answerRepository.updateAnswers(score1, score2, score3, comment1, comment2, comment3, answerID);
}
public List<AnswersDTO> checkedAnswers(List<AnswersDTO>allAnswers, int limit){
    return allAnswers.stream()
            .filter(answer->!equal(answer.getScore1())&&!equal(answer.getScore2())&&!equal(answer.getScore3()))
            .sorted(Comparator.comparing((AnswersDTO ans)->ans.getQuestions().getFireQuestion()))
            .limit(limit)
                    .collect(Collectors.toList());
}
public boolean equal(double input){
   return Math.abs(input+1.0)<0.1;
}
}
