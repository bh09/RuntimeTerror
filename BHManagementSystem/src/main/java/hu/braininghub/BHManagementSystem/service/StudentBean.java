/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import java.util.List;
import hu.braininghub.BHManagementSystem.util.Mappers;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
//public class StudentBean {
//
//    @EJB
//    StudentRepo studentRepository;
//
//    public StudentDTO getStudentByStudentID(Long studentID) {
//        return studentRepository.getStudentByStudentID(studentID);
//    }
//
//    public List<StudentDTO> getAllStudentsOfATeam(Long teamId) {
//        return studentRepository.getStudentDtoByTeamId(teamId);
//    }
@Stateless
//@Remote(StudentInterface.class)
//@Local(StudentInterface.class)
public class StudentBean implements StudentInterface {

    @EJB
    StudentRepo studentRepository;

    @Override
    public StudentDTO getStudentByStudentID(Long studentID) {
        //System.out.println(moduleName+" module");
        //System.out.println(applicationName+" application");
        return Mappers.mapStudentToStudentDTO(studentRepository.getStudentByStudentID(studentID));
    }
    
    @Override
    public List<StudentDTO> getAllStudentsOfATeam(Long teamId) {
        return studentRepository.getStudentDtoByTeamId(teamId);
    }
}
