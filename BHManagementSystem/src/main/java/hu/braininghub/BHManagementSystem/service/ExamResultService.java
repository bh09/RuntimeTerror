package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.exam.ExamResultDTO;
import hu.braininghub.BHManagementSystem.entities.exam.ExamResult;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.repositories.exam.ExamRepository;
import hu.braininghub.BHManagementSystem.repositories.exam.ExamResultRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class ExamResultService {

    @EJB
    ExamResultRepository examResultRepository;

    @EJB
    StudentRepo studentRepo;

    @EJB
    ExamRepository examRepository;

    @EJB
    StudentRepo studentRepository;

    public List<ExamResultDTO> getAllExamResultDTOs() {
        return Mappers.mapExamResultListToDTOs(examResultRepository.getAllExamResults());
    }

    public List<ExamResultDTO> getAllExamResultDTOsByExamId(String examId) {
        Long examIdLong = Long.parseLong(examId);
        return Mappers.mapExamResultListToDTOs(examResultRepository.getAllExamResultsByExam(examIdLong));
    }

    public List<ExamResultDTO> getAllExamResultDTOsByStudentId(String studentId) {
        Long studentIdLong = Long.parseLong(studentId);
        return Mappers.mapExamResultListToDTOs(examResultRepository.getAllExamResultsByStudentId(studentIdLong));
    }

    public Map<Long, ExamResultDTO> createExamResultMap(Long teamId, String examId) {
        List<StudentDTO> students = studentRepository.getStudentDtoByTeamId(teamId);
        
        Map<Long, ExamResultDTO> resultMap = new HashMap<>();
        for (StudentDTO student : students) {
            if (examResultRepository.isExamResutltPresentByExamIdAndStudentID(student.getId(), Long.parseLong(examId))) {
                resultMap.put(student.getId(), Mappers.mapSingleExamResultToDTO(examResultRepository.getExamResultByExamIdAndAtudentId(
                                        student.getId(), Long.parseLong(examId)
                                )));
            }
        }
        return resultMap;
    }
    

    public boolean isThereExamResultPresentToExam(String examId) {
        return examResultRepository.isExamResultOfSecificExamPresent(Long.parseLong(examId));
    }

    public boolean isThereExamResultPresentOfAStudentToExam(String examId, String studentId) {
        return examResultRepository.isExamResutltPresentByExamIdAndStudentID(Long.parseLong(studentId), Long.parseLong(examId));
    }

    public void createAndSaveExamResult(String studentId, String examId, String theoryPoints, String javaPoints, String teamworkpoints) {
        examResultRepository.persistExamResult(createExamResult(studentId, examId, theoryPoints, javaPoints, teamworkpoints));
    }

    public void updateExamResult(String studentId, String examId, String theoryPoints, String javaPoints, String teamworkpoints) {
        ExamResult source = examResultRepository.getExamResultByExamIdAndAtudentId(Long.parseLong(studentId), Long.parseLong(examId));
        ExamResult updated = resetExamResult(source, theoryPoints, javaPoints, teamworkpoints);
        examResultRepository.mergeExamResult(updated);
    }

    public ExamResult resetExamResult(ExamResult source, String theoryPoints, String javaPoints, String teamworkpoints) {
        ExamResult sourceExamResult = source;
        sourceExamResult.setResultPointsTheory(Double.parseDouble(theoryPoints));
        sourceExamResult.setResultPointsJava(Double.parseDouble(javaPoints));
        sourceExamResult.setResultPointsTeamwork(treatTeamworkPoints(teamworkpoints));
        return sourceExamResult;
    }

    public ExamResult createExamResult(String studentId, String examId, String theoryPoints, String javaPoints, String teamworkpoints) {
        ExamResult er = new ExamResult();
        er.setStudent(studentRepo.getStudentEntityById(Long.parseLong(studentId)));
        er.setExam(examRepository.getExamByExamId(Long.parseLong(examId)));
        er.setResultPointsTheory(Double.parseDouble(theoryPoints));
        er.setResultPointsJava(Double.parseDouble(javaPoints));
        er.setResultPointsTeamwork(treatTeamworkPoints(teamworkpoints));
        return er;
    }

    public Double treatTeamworkPoints(String tpoints) {
        if (null == tpoints || "".equals(tpoints)) {
            return 0d;
        } else {
            return Double.parseDouble(tpoints);
        }
    }

    public List<ExamResultDTO> getNormalExamResultsOfAStudent(String studentId) {
        List<ExamResultDTO> listNormal = new ArrayList<>();
        listNormal.addAll(getAllExamResultDTOsByStudentId(studentId)
                .stream()
                .filter(p -> p.getExamDTO()
                .getExamType()
                .getDesc()
                .equalsIgnoreCase("Első ZH"))
                .collect(Collectors.toList()));
        listNormal.addAll(getAllExamResultDTOsByStudentId(studentId)
                .stream()
                .filter(p -> p.getExamDTO()
                .getExamType()
                .getDesc()
                .equalsIgnoreCase("Második ZH"))
                .collect(Collectors.toList()));
        return listNormal;

    }
    
    public List<ExamResultDTO> getFinalExamResultsOfAStudent(String studentId) {
        List<ExamResultDTO> listFinal = new ArrayList<>();
        listFinal.addAll(getAllExamResultDTOsByStudentId(studentId)
                .stream()
                .filter(p -> p.getExamDTO()
                .getExamType()
                .getDesc()
                .equalsIgnoreCase("Záróvizsga"))
                .collect(Collectors.toList()));
        
        return listFinal;

    }
    
    

}
