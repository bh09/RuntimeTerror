/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;


import hu.braininghub.BHManagementSystem.repositories.UsersRepository;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class UsersBean {

    @EJB
    UsersRepository usersRepository;

    public Long getUserIDbyEmail(String email) {
        return usersRepository.getUserIDbyEmail(email);
    }

   
}
