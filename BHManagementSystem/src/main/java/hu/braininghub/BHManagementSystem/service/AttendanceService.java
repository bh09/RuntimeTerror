package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.entities.Attendance;
import hu.braininghub.BHManagementSystem.repositories.AttendanceRepository;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@LocalBean
@Stateless
public class AttendanceService {

    private static final Logger log = LoggerFactory.getLogger(AttendanceService.class);

    @EJB
    AttendanceRepository ar;

    public void saveAttendances(Attendance at) {
        log.debug("saveAttendances: {}", at);
        ar.saveAttendance(at);

    }

    public List<Attendance> getStudentsByDate(String classDay) {
        return ar.getAttendanceByDate(classDay);
    }
    public int getMissedClassById(long studentID){
        return ar.getMissedClassById(studentID);
    }
    public int calculateMissingPercentage(int missedHours){
        return (int)(missedHours*100)/(35*60);
    }

}
