/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author rajnaig
 */
@Stateless
public class TeamBean implements TeamInterface{

    @EJB
    TeamRepository teamRepository;

    @Override
    public TeamDTO getTeamByID(Long teamID) {
        return Mappers.mapTeamFromEntity(teamRepository.getTeamByTeamID(teamID));
    }

    @Override
    public List<TeamDTO> getAllTeams() {
        return teamRepository.getAllTeam();
    }
}
