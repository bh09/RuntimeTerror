/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.entities.user;

/**
 *
 * @author rajnaig
 */
import hu.braininghub.BHManagementSystem.entities.Interview;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.exam.ExamResult;
import hu.braininghub.BHManagementSystem.entities.homework.HomeworkResult;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

@Entity
public class Student extends User implements Serializable {

    public Student(Date birtDate, int firstInterview, int secondInterview, List<HomeworkResult> homeworkResults, Team team) {
        this.birtDate = birtDate;
        this.firstInterview = firstInterview;
        this.secondInterview = secondInterview;
        this.homeworkResults = homeworkResults;
        this.team = team;
    }

    public Student() {
    }

    @Column(name = "BIRTHDATE")
    protected Date birtDate;
    /**/
    @ColumnDefault("0")
    @Column(name = "FIRSTINTERVIEW")
    protected int firstInterview;
    @ColumnDefault("0")
    @Column(name = "SECONDINTERVIEW")
    private int secondInterview;

    @OneToMany(mappedBy = "student")
    private List<Answers> answerList = new ArrayList<>();

    @OneToMany(mappedBy = "student")
    private List<HomeworkResult> homeworkResults;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    private Team team;

    @OneToMany(mappedBy = "student")
    private List<Interview> interviewList = new ArrayList<>();

    @OneToMany(mappedBy = "student")
    private List<ExamResult> examResults;

    public Student(Date birtDate, int firstInterview, int secondInterview, List<HomeworkResult> homeworkResults, Team team, List<ExamResult> examResults) {
        this.birtDate = birtDate;
        this.firstInterview = firstInterview;
        this.secondInterview = secondInterview;
        this.homeworkResults = homeworkResults;
        this.team = team;
        this.examResults = examResults;
    }

    public Date getBirtDate() {
        return birtDate;
    }

    public void setBirtDate(Date birtDate) {
        this.birtDate = birtDate;
    }

    public int getFirstInterview() {
        return firstInterview;
    }

    public void setFirstInterview(int firstInterview) {
        this.firstInterview = firstInterview;
    }

    public int getSecondInterview() {
        return secondInterview;
    }

    public void setSecondInterview(int secondInterview) {
        this.secondInterview = secondInterview;
    }

    public List<Answers> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answers> answerList) {
        this.answerList = answerList;
    }

    public List<HomeworkResult> getHomeworkResults() {
        return homeworkResults;
    }

    public void setHomeworkResults(List<HomeworkResult> homeworkResults) {
        this.homeworkResults = homeworkResults;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<Interview> getInterviewList() {
        return interviewList;
    }

    @Override
    public String toString() {
        return "Student{" + "birtDate=" + birtDate + ", firstInterview=" + firstInterview + ", secondInterview=" + secondInterview + ", answerList=" + answerList + ", homeworkResults=" + homeworkResults + ", team=" + team + ", interviewList=" + interviewList + '}';
    }

    public void setInterviewList(List<Interview> interviewList) {
        this.interviewList = interviewList;
    }

    public List<ExamResult> getExamResults() {
        return examResults;
    }

    public void setExamResults(List<ExamResult> examResults) {
        this.examResults = examResults;
    }

}
