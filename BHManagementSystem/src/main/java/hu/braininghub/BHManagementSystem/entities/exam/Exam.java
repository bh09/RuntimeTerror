
package hu.braininghub.BHManagementSystem.entities.exam;

import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Greg Takacs
 */
@Entity
public class Exam extends Task implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date examDate;
    @Enumerated(EnumType.STRING)
    private ExamType examType;
    @ManyToOne
    private Team team;
    private Double maxPopintsTheory;
    private Double maxPointsJava;
    private Double maxPointsTeamwork;
    @OneToMany(mappedBy = "exam")
    private List<ExamResult> examResults;

    public Exam() {
    }

    public Exam(Date examDate, ExamType examType, Team team) {
        this.examDate = examDate;
        this.examType = examType;
        this.team = team;
        this.maxPopintsTheory = 30D;
        this.maxPointsJava = 30D;
        if (examType.equals(ExamType.FINAL_EXAM)) {
            this.maxPointsTeamwork = 30D;
        } else {
            this.maxPointsTeamwork = 0D;
        }
        
        
    }


    public Long getId() {
        return id;
    }

    
    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }


    public ExamType getExamType() {
        return examType;
    }

    public void setExamType(ExamType examType) {
        this.examType = examType;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Double getMaxPopintsTheory() {
        return maxPopintsTheory;
    }


    public Double getMaxPointsJava() {
        return maxPointsJava;
    }

    public Double getMaxPointsTeamwork() {
        return maxPointsTeamwork;
    }


    public List<ExamResult> getExamResults() {
        return examResults;
    }

    public void setExamResults(List<ExamResult> examResults) {
        this.examResults = examResults;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Exam other = (Exam) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Exam{" + "id=" + id + ", examDate=" + examDate + ", examType=" + examType + ", team=" + team.getTeamName() + ", maxPopintsTheory=" + maxPopintsTheory + ", maxPointsJava=" + maxPointsJava + ", maxPointsTeamwork=" + maxPointsTeamwork + ", examResults=" + examResults + '}';
    }

    
    
        

    

}
