
package hu.braininghub.BHManagementSystem.entities.exam;

import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Greg Takacs
 */
@Entity
public class ExamResult extends Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    private Student student;
    
    @ManyToOne
    private Exam exam;
    
    private Double resultPointsTheory;
    private Double resultPointsJava;
    private Double resultPointsTeamwork;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Double getResultPointsTheory() {
        return resultPointsTheory;
    }

    public void setResultPointsTheory(Double resultPointsTheory) {
        this.resultPointsTheory = resultPointsTheory;
    }

    public Double getResultPointsJava() {
        return resultPointsJava;
    }

    public void setResultPointsJava(Double resultPointsJava) {
        this.resultPointsJava = resultPointsJava;
    }

    public Double getResultPointsTeamwork() {
        return resultPointsTeamwork;
    }

    public void setResultPointsTeamwork(Double resultPointsTeamwork) {
        this.resultPointsTeamwork = resultPointsTeamwork;
    }

    @Override
    public String toString() {
        return "ExamResult{" + "id=" + id + ", student=" + student + ", exam=" + exam + ", resultPointsTheory=" + resultPointsTheory + ", resultPointsJava=" + resultPointsJava + ", resultPointsTeamwork=" + resultPointsTeamwork + '}';
    }
    
    

    

}
