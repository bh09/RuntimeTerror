/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.entities.events;

import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import java.sql.Timestamp;

/**
 *
 * @author rajnaig
 */
public enum EventType {
INTERVIEW{
    @Override
    public Event createEvent(Timestamp expiry,Task task) {
     return  EventFactory.createInterviewEvent(expiry, task);
    }
},
/*EXAM{
    @Override
    public Event createEvent(String importance, Timestamp expiry,Task task) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 
},*/
EXAMRESULT{
     @Override
     public Event createEvent( Timestamp expiry,Task task) {
       return EventFactory.createExamResultEvent(expiry, task);
    }
},
HOMEWORK{
     @Override
     public Event createEvent(Timestamp expiry,Task task) {
       return EventFactory.createHomeworkEvent(expiry, task);
    }
},
HOMEWORKRESULT{
     @Override
     public Event createEvent(Timestamp expiry,Task task) {
        return EventFactory.createHomeworkResultEvent(expiry, task);
    }
},
ANSWERS{
     @Override
     public Event createEvent(Timestamp expiry,Task task) {
        return EventFactory.createAnswersEvent(expiry, task);
    }
},
QUESTIONS{
     @Override
     public Event createEvent(Timestamp expiry,Task task) {
       return EventFactory.createQuizEvent(expiry, task);
    }
},
HOMEWORKEDIT{
    @Override
    public Event createEvent(Timestamp expiry, Task task) {
        return EventFactory.createHomeworkEditEvent(expiry, task);
    }
    
};

public abstract Event createEvent( Timestamp expiry,Task task);

}
