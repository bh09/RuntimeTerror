
package hu.braininghub.BHManagementSystem.entities.exam;

/**
 *
 * @author Greg Takacs
 */
public enum ExamType {
    ZH1 ("Első ZH"),
    ZH2 ("Második ZH"),
    FINAL_EXAM ("Záróvizsga");
    
    private final String desc;

    private ExamType(String desc) {
        this.desc = desc;
    }

    public static ExamType getZH1() {
        return ZH1;
    }

    public static ExamType getZH2() {
        return ZH2;
    }

    public static ExamType getFINAL_EXAM() {
        return FINAL_EXAM;
    }

    public String getDesc() {
        return desc;
    }
    
    
    

}
