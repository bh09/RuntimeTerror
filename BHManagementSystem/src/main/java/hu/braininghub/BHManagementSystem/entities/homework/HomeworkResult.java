package hu.braininghub.BHManagementSystem.entities.homework;

import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author Greg Takacs
 */
@Entity
public class HomeworkResult extends Task implements Serializable {

    private static final long serialVersionUID = 1L;


    @ManyToOne
    private Student student;
    
    @ManyToOne
    private Homework homework;
    
    private Integer resultPoints;
    
    
    
    

    public HomeworkResult() {
    }

    public HomeworkResult(Student student, Homework homework, int resultPoints) {
        this.student = student;
        this.homework = homework;
        this.resultPoints = resultPoints;
    }
    

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Homework getHomework() {
        return homework;
    }

    public void setHomework(Homework homework) {
        this.homework = homework;
    }

    public int getResultPoints() {
        return resultPoints;
    }

    public void setResultPoints(int resultPoints) {
        this.resultPoints = resultPoints;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HomeworkResult other = (HomeworkResult) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HomeworkResult{" + "id=" + id + ", student=" + student.getName() + ", homework=" + homework.getTitle() + ", resultPoints=" + resultPoints + '}';
    }

    
    
    
    

}
