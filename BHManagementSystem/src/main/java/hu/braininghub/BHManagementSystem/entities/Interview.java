package hu.braininghub.BHManagementSystem.entities;

import hu.braininghub.BHManagementSystem.entities.user.Student;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
public class Interview extends Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "HRPOINTS")
    private double hrPoints;
    @Column(name = "HRCOMMENTS")
    private String hrComments;
    @Column(name = "JAVAPOINTS")
    private double javaPoints;
    @Column(name = "JAVACOMMENTS")
    private String javaComments;

    @ManyToOne(targetEntity = Student.class)
    private Student student;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getHrPoints() {
        return hrPoints;
    }

    public void setHrPoints(double hrPoints) {
        this.hrPoints = hrPoints;
    }

    public String getHrComments() {
        return hrComments;
    }

    public void setHrComments(String hrComments) {
        this.hrComments = hrComments;
    }

    public double getJavaPoints() {
        return javaPoints;
    }

    public void setJavaPoints(double javaPoints) {
        this.javaPoints = javaPoints;
    }

    public String getJavaComments() {
        return javaComments;
    }

    public void setJavaComments(String javaComments) {
        this.javaComments = javaComments;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.hrPoints) ^ (Double.doubleToLongBits(this.hrPoints) >>> 32));
        hash = 17 * hash + Objects.hashCode(this.hrComments);
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.javaPoints) ^ (Double.doubleToLongBits(this.javaPoints) >>> 32));
        hash = 17 * hash + Objects.hashCode(this.javaComments);
        hash = 17 * hash + Objects.hashCode(this.student);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Interview other = (Interview) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Interview{" + "id=" + id + ", hrPoints=" + hrPoints + ", hrComments=" + hrComments + ", javaPoints=" + javaPoints + ", javaComments=" + javaComments + ", student=" + student + '}';
    }

}
