/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.entities.events;

import hu.braininghub.BHManagementSystem.entities.Interview;
import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.exam.ExamResult;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.homework.HomeworkResult;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import java.sql.Timestamp;

/**
 *
 * @author rajnaig
 */
public class EventFactory {
public static Event createInterviewEvent(Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.GENERAL.toString());
        String studentName=((Interview)task).getStudent().getName();
        event.setMessage(studentName+" "+"interjúja értékelve");
        event.setTask(task);
        event.setTeam(((Interview)task).getStudent().getTeam());
return event;
}
public static Event createExamResultEvent( Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.GENERAL.toString());
        String studentName=((ExamResult)task).getStudent().getName();
        event.setMessage(studentName+" "+"zh-ja értékelve");
        event.setTask(task);
        event.setTeam(((ExamResult)task).getStudent().getTeam());
return event;
}
public static Event createHomeworkEvent( Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.IMPORTANT.toString());
        String homeWorkTitle=((Homework)task).getTitle();
        event.setMessage(homeWorkTitle+" "+"házi kiadva Határidö:");
        event.setTask(task);
        event.setTeam(((Homework)task).getTeam());
return event;
}
public static Event createHomeworkResultEvent( Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.GENERAL.toString());
        String studentName=((HomeworkResult)task).getStudent().getName();
        String homeworkName=((HomeworkResult)task).getHomework().getTitle();
        event.setMessage(studentName+" "+homeworkName+" "+"házija értékelve");
        event.setTask(task);
        event.setTeam(((HomeworkResult)task).getStudent().getTeam());
return event;
}
public static Event createAnswersEvent(Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.GENERAL.toString());
        String studentName=((Answers)task).getStudent().getName();
        event.setMessage(studentName+" "+"röpdolgozata értékelve");
        event.setTask(task);
        event.setTeam(((Answers)task).getQuestions().getTeam());
return event;
}
public static Event createQuizEvent( Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.URGENT.toString());
        event.setMessage("Új röpdolgozat kiírva");
        event.setTask(task);
        event.setTeam(((Questions)task).getTeam());
return event;
}
public static Event createHomeworkEditEvent( Timestamp expiry,Task task){
    Event event=new Event();
        event.setExpiry(expiry);
        event.setImportance(ImportanceType.IMPORTANT.toString());
        String homeWorkTitle=((Homework)task).getTitle();
        event.setMessage(homeWorkTitle+" "+"házi szerkesztve Határidö:");
        event.setTask(task);
        event.setTeam(((Homework)task).getTeam());
return event;
}
}
