/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.entities.quiz;



import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author rajnaig
 */
@Data//Beadja az összes gettert settert, equals hashcode tostring
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Questions extends Task implements Serializable {

    private static final long serialVersionUID = 1L;

    private String firstQuestion;
    private String secondQuestion;
    private String thirdQuestion;
    private Timestamp fireQuestion;        
    @OneToMany(mappedBy = "questions", cascade = CascadeType.REMOVE)
    private List<Answers> answerList=new ArrayList<>();
    @ManyToOne
    private Team team;
    
}
