    package hu.braininghub.BHManagementSystem.entities.homework;

import hu.braininghub.BHManagementSystem.entities.Task;
import hu.braininghub.BHManagementSystem.entities.Team;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Greg Takacs
 */
@Entity
public class Homework extends Task implements Serializable {

    private static final long serialVersionUID = 1L;

    
  
    private String title;
    private String description;
    private int maxPoints;
    private Date dateReleased;
    private Date deadline;
    
    
    @ManyToOne
    private Team team;
    
    @OneToMany(mappedBy = "homework", cascade = CascadeType.REMOVE)
    private List<HomeworkResult> homeworkResults;

    public Homework() {
    }

    public Homework(String title, String description, int maxPoints, LocalDate deadline, Team team) {
        this.title = title;
        this.description = description;
        this.maxPoints = maxPoints;
        this.dateReleased = Date.valueOf(LocalDate.now());
        this.deadline = Date.valueOf(deadline);
        this.team = team;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Date getDateReleased() {
        return dateReleased;
    }

    public void setDateReleased(Date dateReleased) {
        this.dateReleased = dateReleased;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<HomeworkResult> getHomeworkResults() {
        return homeworkResults;
    }

    public void setHomeworkResults(List<HomeworkResult> homeworkResults) {
        this.homeworkResults = homeworkResults;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Homework other = (Homework) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Homework{" + "id=" + id + ", title=" + title + ", description=" + description + ", maxPoints=" + maxPoints + ", dateReleased=" + dateReleased + ", deadline=" + deadline + ", team=" + team + ", homeworkResults=" + homeworkResults + '}';
    }

    
    
    

   
    
    

   
}
