/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.entities;

import hu.braininghub.BHManagementSystem.entities.events.Event;
import hu.braininghub.BHManagementSystem.entities.exam.Exam;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Proxy;

/**
 *
 * @author rajnaig
 */
@Data//Beadja az összes gettert settert, equals hashcode tostring
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Proxy(lazy=false)
public class Team implements Serializable {

    @OneToMany(mappedBy = "team")
    private List<Homework> homeworks;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String teamName;
    private String googleCalendarURL;

    
    @OneToMany(mappedBy = "team")
    private List<Student> studentList=new ArrayList<>();
    
    @OneToMany(mappedBy = "team")
    private List<Exam> exams;
    

    @OneToMany(mappedBy="team")
    private List<Event> eventList=new ArrayList<>();
    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }


    public String getGoogleCalendarURL() {
        return googleCalendarURL;
    }

    public void setGoogleCalendarURL(String googleCalendarURL) {
        this.googleCalendarURL = googleCalendarURL;
    }



    public List<Student> getStudentList() {
        return studentList;
    }
    
    public Student getStudent (Student student) {
    
    Student resultStudent = (Student) getStudentList().stream().filter(p -> p.equals(student));
    return resultStudent;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Team)) {
            return false;
        }
        Team other = (Team) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Team{" + "id=" + id + ", teamName=" + teamName + ", googleCalendarURL=" + googleCalendarURL + '}';
    }

    
    

    @OneToMany(mappedBy = "team")
    private List<Questions> questionList=new ArrayList<>();
   




}
