/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories;

import hu.braininghub.BHManagementSystem.entities.user.Teacher;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author rajnaig
 */
@Stateless
public class TeacherRepository {
@PersistenceContext
EntityManager em;
public Teacher getTeacherByTeacherID(Long teacherID){
    Query q=em.createQuery("From Teacher Where id=:teacherID",Teacher.class);
    q.setParameter("teacherID", teacherID);
    return (Teacher)q.getSingleResult();
}

 public void addTeacher(Teacher teacher) {
        em.persist(teacher);
    }
}
