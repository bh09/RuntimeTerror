package hu.braininghub.BHManagementSystem.repositories.homework;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class HomeworkRepository {

    @PersistenceContext
    EntityManager em;

    public List<Homework> getAllHomework() {
        Query q = em.createQuery("Select g from Homework g", Homework.class);
        return q.getResultList();
    }

    public Homework getHomeworkById(Long hwId) {
        Query q = em.createQuery("from Homework where id=:homeworkId");
        q.setParameter("homeworkId", hwId);
        return (Homework) q.getResultList().stream().findAny().get();
    }

    public List<Homework> getAllHomeworkByTeamId(Long teamId) {
        Query q = em.createQuery("from Homework where team_id=:teamId");
        q.setParameter("teamId", teamId);
        return q.getResultList();
    }

    public List<Homework> getAllHomeWorkOfTeam(Team team) {
        List<Homework> resultList = getAllHomework().stream()
                .filter(p -> p.getTeam().equals(team))
                .collect(Collectors.toList());
        return resultList;
    }

    public List<Homework> getAllHomeworkofStudent(Student student) {
        List<Homework> resultList = getAllHomework().stream()
                .filter(p -> p.getTeam().getStudent(student).equals(student))
                .collect(Collectors.toList());
        return resultList;
    }
    
    @Transactional
    public void addHomework(Homework homework) {
        em.persist(homework);
    }

    @Transactional
    public void removeHomwork(Homework homework) {
        em.remove(homework);
    }

    @Transactional
    public void updateHomework(Homework homework) {
        em.merge(homework);
    }

}
