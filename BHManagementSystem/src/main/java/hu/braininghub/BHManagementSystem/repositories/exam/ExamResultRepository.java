package hu.braininghub.BHManagementSystem.repositories.exam;

import hu.braininghub.BHManagementSystem.entities.exam.ExamResult;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class ExamResultRepository {

    @PersistenceContext
    EntityManager em;

    public List<ExamResult> getAllExamResults() {
        Query q = em.createQuery("SELECT g from ExamResult g", ExamResult.class);
        return q.getResultList();
    }

    public List<ExamResult> getAllExamResultsByExam(Long examId) {
        Query q = em.createQuery("from ExamResult where exam_id=:examId");
        q.setParameter("examId", examId);
        return q.getResultList();
    }

    public List<ExamResult> getAllExamResultsByStudentId(Long studentId) {
        Query q = em.createQuery("from ExamResult where student_id=:studentId");
        q.setParameter("studentId", studentId);
        return q.getResultList();
    }

    public ExamResult getExamResultByExamIdAndAtudentId(Long studentId, Long examId) {
        Query q = em.createQuery("from ExamResult where student_id=:studentId and exam_id=:examId");
        q.setParameter("studentId", studentId);
        q.setParameter("examId", examId);
        return (ExamResult) q.getSingleResult();
    }

    public boolean isExamResutltPresentByExamIdAndStudentID(Long studentId, Long examId) {
        Query q = em.createQuery("select count(e) from ExamResult e where student_id=:studentId and exam_id=:examId");
        q.setParameter("studentId", studentId);
        q.setParameter("examId", examId);
        Long count = (Long) q.getSingleResult();
        return ((count.equals(0L)) ? false : true );
    }

    public boolean isExamResultOfSecificExamPresent(Long examId) {
        List<ExamResult> listByTeam = getAllExamResultsByExam(examId);
        if (listByTeam.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    @Transactional
    public void persistExamResult(ExamResult examResult) {
        em.persist(examResult);
    }

    @Transactional
    public void mergeExamResult(ExamResult examResult) {
        em.merge(examResult);
    }

}
