/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories;


import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class TeamRepository {
  @PersistenceContext//(unitName = "Quiz")
    EntityManager em;
    public List<TeamDTO> getAllTeam(){
        Query q = em.createQuery("From Team", Team.class);
        return (List<TeamDTO>)q.getResultList().stream().map(team->Mappers.mapTeamFromEntity((Team)team)).collect(Collectors.toList());
    }
    public List<Team> getTeam() {
        Query q = em.createQuery("From Team", Team.class);
        return q.getResultList();
    }
     public Team getTeamByTeamID(Long teamID) {
        Query q = em.createQuery("From Team WHERE id=:teamID", Team.class);
        q.setParameter("teamID", teamID);
        return (Team)q.getSingleResult();
    }
     
    @Transactional
    public void addTeam(Team team) {
        System.out.println(team);
        em.persist(team);
    }      
}
