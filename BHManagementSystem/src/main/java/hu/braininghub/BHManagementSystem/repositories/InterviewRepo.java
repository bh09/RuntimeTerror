package hu.braininghub.BHManagementSystem.repositories;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.Interview;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class InterviewRepo {

    private static final Logger log = LoggerFactory.getLogger(InterviewRepo.class);

    @PersistenceContext
    private EntityManager em;

    public void addInterView(Interview interv) {

        log.info("addInterivew: {}", interv);
        em.persist(interv);
    }

    public void addStudentIdToTheCorrectInterview(int studentId, long interviewId) {

        log.info("addStudentIdToTheCorrectInterview: Student_id: {} , interview_Id: {}", studentId, interviewId);
        Query q = em.createQuery("UPDATE Interview set student_id=:studentId Where id=:interviewId");
        q.setParameter("studentId", studentId);
        q.setParameter("interviewId", interviewId);
        q.executeUpdate();

    }

    public void updateStudentInterviewColumn(int interviewNumber, long studentId) {

        if (interviewNumber == 1) {
            log.info("updateStudentInterviewColumn/true: interview_id: {} , student_id: {}", interviewNumber, studentId);
            Query q = em.createQuery("UPDATE Student set firstInterview=:interviewNumber Where id=:studentId");
            q.setParameter("interviewNumber", interviewNumber);
            q.setParameter("studentId", studentId);
            q.executeUpdate();
        } else {
            log.info("updateStudentInterviewColumn/false: interview_id: {} , student_id: {}", interviewNumber, studentId);
            Query q = em.createQuery("UPDATE Student set secondInterview=:interviewNumber Where id=:studentId");
            q.setParameter("interviewNumber", 1);
            q.setParameter("studentId", studentId);
            q.executeUpdate();
        }

    }

    public Student getStudentByStudentID(long studentID) {
        Query q = em.createQuery("From Student Where id=:studentID");
        q.setParameter("studentID", studentID);
        return (Student) q.getSingleResult();
    }

    public List<Interview> getInterviewByStudentId(long studentId) {
        Query q = em.createQuery("From Interview Where student_id=:studentID");
        q.setParameter("studentID", studentId);
        return q.getResultList();
    }
    public Interview getInterviewByInterviewID(Long interviewID){
    Query q = em.createQuery("From Interview Where id=:interviewID");
        q.setParameter("interviewID", interviewID);
        return (Interview)q.getSingleResult();    
    }

}
