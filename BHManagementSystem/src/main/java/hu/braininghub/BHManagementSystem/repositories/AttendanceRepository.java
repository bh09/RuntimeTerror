package hu.braininghub.BHManagementSystem.repositories;

import hu.braininghub.BHManagementSystem.entities.Attendance;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class AttendanceRepository {

    private static final Logger log = LoggerFactory.getLogger(AttendanceRepository.class);

    @PersistenceContext
    private EntityManager em;

    public void saveAttendance(Attendance at) {
        log.debug("saveAttendance: attendance: {}", at);
        em.persist(at);
    }

    public List<Attendance> getAttendanceByDate(String classDay) {

        Query q = em.createQuery("From Attendance Where CLASSDAY like :classDay");
        q.setParameter("classDay", classDay + "%");
        return q.getResultList();

    }

    public int getMissedClassById(long studentID) {
        Query q = em.createNativeQuery(
                "select sum(MISSINGTIME) from Attendance where STUDENTID=:studentId");
        q.setParameter("studentId", studentID);
        try{
        return ((BigDecimal)q.getSingleResult()).intValue();
        }catch(Exception ex){
        return 0;    
        }
    }

}
