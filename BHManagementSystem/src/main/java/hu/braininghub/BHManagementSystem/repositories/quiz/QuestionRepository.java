/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories.quiz;


import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.EventType;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.service.EventBean;
import hu.braininghub.BHManagementSystem.service.EventInterface;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class QuestionRepository {
    @EJB(name="ejb/events", description="stateless")
    EventInterface eventBean;
@PersistenceContext//(unitName = "Quiz")
    EntityManager em;
@EJB
AnswerRepository answerRepository;
    /*public Optional<Questions> getQuestionByClassDay(Long classDayID){
        Query q = em.createQuery("From Questions Where classDay_id=:classDayID", Questions.class);
    q.setParameter("classDayID", classDayID);
    Questions question=null;
    try{
      question=(Questions)q.getSingleResult();  
    }catch(NoResultException ex){
        
    }    
    return  Optional.ofNullable(question);  
    }*/
    /*public List<Questions> getQuestions(){
        
        Query q = em.createQuery("Select q from Questions q", Questions.class);
        List<Questions> questions= q.getResultList();

    return questions;
    }*/
    public QuestionRepository(){
    }

    public QuestionRepository(EntityManager em) {
        this.em = em;
    }

    public List<Questions> getQuestionsForTeam(Long teamID) {
        Query q = em.createQuery("From Questions Where team_id=:teamID", Questions.class);
        q.setParameter("teamID", teamID);
        List<Questions> questions=q.getResultList();
        return questions;
    }
    public List<Questions> searchQuestionsForTeam(Long teamID,String searchText){
        Query q;
        
        try{
            System.out.println(teamID);    
        Long id=Long.parseLong(searchText);
        q = em.createQuery("From Questions Where team_id=:teamID AND id = :searchNumber", Questions.class);    
        q.setParameter("teamID", teamID);
        q.setParameter("searchNumber",id);
        }catch(NumberFormatException ex){
             q = em.createQuery("From Questions Where team_id=:teamID AND firstQuestion Like :searchText OR secondQuestion Like :searchText OR thirdQuestion Like :searchText", Questions.class);
             q.setParameter("teamID", teamID);
             q.setParameter("searchText", new StringBuffer().append("%").append(searchText).append("%").toString());    
        }
        
        List<Questions> questions=q.getResultList();
        if(teamID==-1)
        return questions;
        else
        return questions.stream().filter(question->question.getTeam().getId()==teamID).collect(Collectors.toList());    

    }
    public Long getQuestionsForTeamLastIndex(Long teamID){
        Query q = em.createNativeQuery("Select max(id) from questions Where team_id=:teamID");
        q.setParameter("teamID", teamID);

    try{   
     return ((BigInteger)q.getSingleResult()).longValue();
    }catch(NullPointerException ex){
        
    }
    return null;
    }
    public void overwriteQuestion(Long questionID,String firstQuestion,String secondQuestion,String thirdQuestion){
    Query q = em.createQuery("UPDATE Questions set firstQuestion=:firstQuestion, secondQuestion=:secondQuestion, thirdQuestion=:thirdQuestion Where id=:questionID");
            q.setParameter("firstQuestion", firstQuestion);
            q.setParameter("secondQuestion", secondQuestion);
            q.setParameter("thirdQuestion", thirdQuestion);
            q.setParameter("questionID", questionID);
            q.executeUpdate();
    
    }
    public void deleteQuestionByQuestionID(Long questionID){
        Query q = em.createQuery("FROM Questions Where id=:questionID");
            q.setParameter("questionID", questionID);
            Questions question=(Questions)q.getSingleResult();
            if(question.getFireQuestion()!=null){
                eventBean.deleteEventByTaskID(questionID);
            }
            removeAnswersForQuestion(question.getAnswerList());
            removeQuestion(question);
    }
    @Transactional
    public void removeQuestion(Questions question) {
        em.remove(question);
    }
    public boolean isQuestionAlreadyFired(Long questionID){ 
     Query q = em.createNativeQuery("Select fireQuestion from questions Where id=:questionID");
        q.setParameter("questionID", questionID);
        try{
         q.getSingleResult();
         return true;
        }catch(NullPointerException ex){
         return false;   
        }
    }
    public Team getTeamByQuestionID(Long questionID){
    Query q=em.createQuery("From Questions where id=:questionID");
    q.setParameter("questionID", questionID);
    Questions question=(Questions)q.getSingleResult();
    return question.getTeam();
    }
    public Questions getQuestionByQuestionID(Long questionID){
    Query q=em.createQuery("From Questions where id=:questionID");
    q.setParameter("questionID", questionID);
    try{
    return (Questions)q.getSingleResult();
    }catch(Exception ex){
    return null;    
    }        
    }
    public void fireQuestion(Long questionID,Timestamp fireQuestion){
      Query q = em.createQuery("UPDATE Questions set fireQuestion=:fireQuestion Where id=:questionID");
            q.setParameter("fireQuestion", fireQuestion);
            q.setParameter("questionID", questionID);
            q.executeUpdate();
            
            eventBean.createEvent(EventType.QUESTIONS.createEvent(fireQuestion, getQuestionByQuestionID(questionID)));
    }
    public void createQuestion(String firstQuestion,String secondQuestion,String thirdQuestion, Team team){
        Questions question=new Questions();
        question.setFirstQuestion(firstQuestion);
        question.setSecondQuestion(secondQuestion);
        question.setThirdQuestion(thirdQuestion);
        question.setTeam(team);
        addQuestion(question);
    }
    @Transactional
    public void addQuestion(Questions question) {
        System.out.println(question);
        em.persist(question);
    }
public void removeAnswersForQuestion(List<Answers> answers){
    for (Answers answer : answers) {
        eventBean.deleteEventByTaskID(answer.getId());
    }
    
}    
}
