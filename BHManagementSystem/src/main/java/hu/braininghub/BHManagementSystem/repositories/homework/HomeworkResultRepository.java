package hu.braininghub.BHManagementSystem.repositories.homework;

import hu.braininghub.BHManagementSystem.entities.events.EventType;
import hu.braininghub.BHManagementSystem.entities.homework.HomeworkResult;
import hu.braininghub.BHManagementSystem.service.EventInterface;
import hu.braininghub.BHManagementSystem.service.HomeworkResultService;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.swing.event.DocumentEvent;
import javax.transaction.Transactional;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class HomeworkResultRepository {
    @EJB(name="ejb/events", description="stateless")
    EventInterface eventBean;
    @EJB
    HomeworkResultService homeworkResultService;
    @PersistenceContext
    EntityManager em;

    public List<HomeworkResult> getAllHomeworkResults() {
        Query q = em.createQuery("SELECT g from HomeworkResult g", HomeworkResult.class);
        return q.getResultList();
    }
    
    public List<HomeworkResult> getAllHomeworkResulsByHomeworkId (Long homeworkId) {
        Query q = em.createQuery("from HomeworkResult where homework_id=:homeworkId");
        q.setParameter("homeworkId", homeworkId);
        return q.getResultList();
    }
    
    
    public List<HomeworkResult> getAllHomeworkResultsofStudentId(Long studentId) {
        Query q = em.createQuery("from HomeworkResult where student_id=:studentId");
        q.setParameter("studentId", studentId);
        return q.getResultList();
    }
    
     public List<HomeworkResult> getCustomHomeworkResult(String sId, String hId) {
         Long studentId = Parser.parseStringToLong(sId);
         Long homeworkId = Parser.parseStringToLong(hId); 
         System.out.println("most kezdődik a lekérdezés");
         
        Query q = em.createQuery("from HomeworkResult where student_id=:studentId and homework_id=:homeworkId");
        q.setParameter("studentId", studentId);
        q.setParameter("homeworkId", homeworkId);
        return q.getResultList();
    }
     public HomeworkResult getHomeworkResultByHomeworkID(Long homeworkResultID) {

        Query q = em.createQuery("from HomeworkResult where id=:homeworkResultID");
        q.setParameter("homeworkResultID", homeworkResultID);
        return (HomeworkResult)q.getSingleResult();
    }
    
    @Transactional
    public void addHomeworkResult (HomeworkResult homeworkResult) {
        eventBean.createEvent(EventType.HOMEWORKRESULT.createEvent(Timestamp.valueOf(LocalDateTime.now().plusDays(2L)),homeworkResult));
        em.persist(homeworkResult);
    }
    
    @Transactional
    public void removeHomeworkResult (HomeworkResult homeworkResult) {
        em.remove(homeworkResult);
    }
    public void updateHomeworkResult(Integer points,Long studentID,Long homeworkID){
        if(homeworkResultService.isHomeworkResultExists(studentID.toString(), homeworkID.toString())){
        eventBean.deleteEventByTaskID(getCustomHomeworkResult(studentID.toString(), homeworkID.toString()).get(0).getId());
        }
        Query q = em.createQuery("UPDATE HomeworkResult set resultPoints=:resultPoints Where homework_id=:homeworkID AND student_id=:studentID");
            q.setParameter("resultPoints", points);
            q.setParameter("homeworkID", homeworkID);
            q.setParameter("studentID", studentID);
            q.executeUpdate();
        System.out.println("ResultID"+getCustomHomeworkResult(studentID.toString(), homeworkID.toString()).get(0).getId());  
        eventBean.createEvent(EventType.HOMEWORKRESULT.createEvent(Timestamp.valueOf(LocalDateTime.now().plusDays(2L)), getCustomHomeworkResult(studentID.toString(), homeworkID.toString()).get(0)));
    }
}
