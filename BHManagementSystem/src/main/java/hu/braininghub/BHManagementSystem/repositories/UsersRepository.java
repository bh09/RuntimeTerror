/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories;

import hu.braininghub.BHManagementSystem.entities.user.Users;
import java.math.BigInteger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class UsersRepository {

    @PersistenceContext//(unitName = "Quiz")
    EntityManager em;

    public Long getUserIDbyEmail(String email) {
        Query q = em.createNativeQuery("Select user_id From users Where EMAIL =:email");
        q.setParameter("email", email);
        return ((BigInteger) q.getSingleResult()).longValue();
    }

    @Transactional
    public void addUsers(Users users) {
        em.persist(users);
    }
}
