/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories.eventrepository;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.Event;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author rajnaig
 */
@Stateless
public class EventRepository {    
    
    @PersistenceContext
EntityManager em;
public List<Event> getEventsForTeamID(Long teamID){
  Query q=em.createQuery("From Event where team_id=:teamID");
  q.setParameter("teamID", teamID);
  return q.getResultList();
}
public void createEvent(String message,Timestamp expiry,Team team){
 Event event=new Event();
 event.setMessage(message);
 event.setExpiry(expiry);
 event.setTeam(team);
 addEvent(event);
}
public void deleteEvent(Long eventID){
 Query q = em.createQuery("DELETE FROM Event Where id=:eventID");
            q.setParameter("eventID", eventID);
            q.executeUpdate();    
}
public void addEvent(Event event){
    em.persist(event);
}
public void deleteEventByExpiry(Timestamp expiry){
Query q = em.createQuery("DELETE FROM Event Where expiry BETWEEN :expiryLower AND :expiryUpper");
            q.setParameter("expiryLower", Timestamp.valueOf(expiry.toLocalDateTime().minusSeconds(1)));
            q.setParameter("expiryUpper", Timestamp.valueOf(expiry.toLocalDateTime().plusSeconds(1)));
            q.executeUpdate();       
}
public void deleteEventByTaskID(Long taskID){
Query q = em.createQuery("DELETE FROM Event Where task_id=:taskID");
            q.setParameter("taskID",taskID);
            
            q.executeUpdate();       
}    
}


