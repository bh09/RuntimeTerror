/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories;

import hu.braininghub.BHManagementSystem.entities.user.GroupREALM;
import hu.braininghub.BHManagementSystem.entities.user.Users;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class GroupsRepository {
 @PersistenceContext//(unitName = "Quiz")
    EntityManager em;
  @Transactional
    public void addGroups(GroupREALM groups) {
        em.persist(groups);
    }     
}
