package hu.braininghub.BHManagementSystem.repositories.exam;

import hu.braininghub.BHManagementSystem.entities.exam.Exam;
import hu.braininghub.BHManagementSystem.entities.exam.ExamType;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author Greg Takacs
 */
@Stateless
public class ExamRepository {

    @PersistenceContext
    EntityManager em;

    public List<Exam> getAllExams() {
        Query q = em.createQuery("SELECT g from Exam g", Exam.class);
        return q.getResultList();
    }

    public List<Exam> getAllExamsByTeamId(Long teamId) {
        Query q = em.createQuery("from Exam where team_id=:teamId");
        q.setParameter("teamId", teamId);
        return q.getResultList();
    }
    
    public Exam getExamByExamId(Long examId) {
        Query q = em.createQuery("from Exam where id=:examId");
        q.setParameter("examId", examId);
        return (Exam) q.getSingleResult();
    }
    
    
    public List<Exam> getAllExamsByExamType(ExamType examType) {
        Query q = em.createQuery("from Exam where examType=:examType");
        q.setParameter("examType", examType.name());
        return q.getResultList();
    }
    
    @Transactional
    public void addExam (Exam exam) {
        em.persist(exam);
    }
    
    
    
    
    

}
