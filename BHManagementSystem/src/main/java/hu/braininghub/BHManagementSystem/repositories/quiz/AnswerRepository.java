/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories.quiz;


import hu.braininghub.BHManagementSystem.entities.events.EventType;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.service.AnswersService;
import hu.braininghub.BHManagementSystem.service.EventInterface;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

/**
 *
 * @author rajnaig
 */
@Stateless
@LocalBean
public class AnswerRepository {
@EJB(name="ejb/events", description="stateless")
    EventInterface eventBean;
@EJB
AnswersService answersService;
@PersistenceContext//(unitName = "Quiz")
    EntityManager em;

    public List<Answers> getAnswers() {
        Query q = em.createQuery("Select g from Answers g", Answers.class);
        return q.getResultList();
    }
    public boolean isAnswerExistsForQuestionAndStudent(Long questionID,Long studentID){
        Query q = em.createQuery("From Answers Where questions_id=:questionID AND student_id=:studentID", Answers.class);
    q.setParameter("questionID",questionID);
    q.setParameter("studentID", studentID);
        try{
       q.getSingleResult();
       return true;
    }catch(Exception ex){
       return false; 
    } 
    }
    public void createAnswerForStudent(String answer1,String answer2,String answer3,Questions question,Student student){
        Answers answer=new Answers();
        answer.setAnswer1(answer1);
        answer.setAnswer2(answer2);
        answer.setAnswer3(answer3);
        answer.setQuestions(question);
        answer.setStudent(student);
        addAnswer(answer);
    }
    public Answers getAnswerByAnswerID(Long answerID){
        Query q = em.createQuery("From Answers Where id=:answerID", Answers.class);
    q.setParameter("answerID",answerID);

       return (Answers)q.getSingleResult();


    }
    public void updateAnswers(Double score1,Double score2,Double score3,String comment1,String comment2,String comment3, Long answerID){
    eventBean.deleteEventByTaskID(answerID);
        Query q = em.createQuery("UPDATE Answers set score1=:score1, score2=:score2, score3=:score3, comment1=:comment1, comment2=:comment2, comment3=:comment3 Where id=:answerID");
            q.setParameter("score1", score1);
             q.setParameter("score2", score2);
              q.setParameter("score3", score3);
               q.setParameter("comment1", comment1);
               q.setParameter("comment2", comment2);
               q.setParameter("comment3", comment3);
               q.setParameter("comment3", comment3);
               q.setParameter("answerID", answerID);
            q.executeUpdate();
        if (!(answersService.equal(score1)||answersService.equal(score2)||answersService.equal(score3))) {
            eventBean.createEvent(EventType.ANSWERS.createEvent(Timestamp.valueOf(LocalDateTime.now().plusDays(2L)),getAnswerByAnswerID(answerID)));
        }
    }
   public List<Answers> getAllAnswersForAStudent(Long studentID){
    Query q = em.createQuery("From Answers Where student_id=:studentID", Answers.class);
    q.setParameter("studentID", studentID);
   return (List<Answers>)q.getResultList();
   }
    @Transactional
    public void addAnswer(Answers answer) {
        System.out.println(answer);
        em.persist(answer);
    }    
}
