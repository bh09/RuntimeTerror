package hu.braininghub.BHManagementSystem.repositories;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
@LocalBean
public class StudentRepo {

    public Student getStudentByStudentID(Long studentID) {
        Query q = em.createQuery("From Student Where id=:studentID");
        q.setParameter("studentID", studentID);
        return (Student) q.getSingleResult();
    }

    @PersistenceContext
    private EntityManager em;

    public void addStudent(Student student) {
        em.persist(student);
    }

    public List<StudentDTO> getStudentDtoByTeamId(Long teamId) {

        Query q = em.createQuery("From Student where team_id=:teamId", Student.class);
        q.setParameter("teamId", teamId);

        return (List<StudentDTO>) q.getResultList().stream().map(p -> Mappers.mapStudentToStudentDTO((Student) p)).collect(Collectors.toList());

    }

    public List<Student> getStudentByTeamId(Long teamId) {

        Query q = em.createQuery("From Student where team_id=:teamId", Student.class);
        q.setParameter("teamId", teamId);

        return (List<Student>) q.getResultList();
    }

    public Student getStudentEntityById(Long studentId) {
        TypedQuery<Student> q = em.createQuery("SELECT a From Student a Where a.id  = :studentID", Student.class);
        Student s = q.setParameter("studentID", studentId).getSingleResult();
        System.out.println(s.getBirtDate());
        return s;

    }

    public void updateStudent(Student student) {
        em.merge(student);
    }

    public List<Long> getAllStudentIDsByTeamId(Long teamId) {

        Query q = em.createQuery("From Student where team_id=:teamId", Student.class);
        q.setParameter("teamId", teamId);
        List<Student> sList = q.getResultList();
        return sList.stream().map(p -> p.getId()).collect(Collectors.toList());

    }

}
