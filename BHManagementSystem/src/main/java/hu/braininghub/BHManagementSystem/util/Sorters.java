
package hu.braininghub.BHManagementSystem.util;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import java.util.List;

/**
 *
 * @author Greg Takacs
 */
public class Sorters {
    
    public static List<TeamDTO> orderTeamDTOs (List<TeamDTO> dtos, String teamId) {
    
        TeamDTO currentTeamDto = dtos
                .stream()
                .filter(dto -> dto.getId()
                .equals(Parser.parseStringToLong(teamId)))
                .findFirst()
                .get();
        dtos.remove(currentTeamDto);
        dtos.add(0, currentTeamDto);
        return dtos;
    
    }

}
