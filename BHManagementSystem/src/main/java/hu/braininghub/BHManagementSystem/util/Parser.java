package hu.braininghub.BHManagementSystem.util;

import java.sql.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Greg Takacs
 */
public class Parser {

    public static Long parseStringToLong(String s) {
        Long resultLong = Long.parseLong(s);
        return resultLong;
    }

    public static int parseStringToInt(String s) {
        int resultInt = Integer.parseInt(s);
        return resultInt;
    }

    public static LocalDate stringToLocaldate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return localDate;

    }

    public static LocalDate stringToLocalDateNormal(String date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return localDate;

    }

    public static java.sql.Date parseLocalDateToSqlDate(LocalDate date) {
        return Date.valueOf(date);
    }

    public static Double convertStringToValidDoubleFormat(String source) {
        if (isFormatWithComma(source)) {
            return convertFormat(source);
        } else {
            return Double.parseDouble(source);
        }
    }

    public static Double convertFormat(String s) {
        Number number = 0;
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        try {
            number = format.parse(s);
        } catch (ParseException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return number.doubleValue();
    }

    public static boolean isFormatWithComma(String s) {
        boolean isCommaPresent = false;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ',') {
                isCommaPresent = true;
            }
        }
        return isCommaPresent;
    }
}
