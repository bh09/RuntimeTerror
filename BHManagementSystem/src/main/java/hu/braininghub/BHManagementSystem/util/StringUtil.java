package hu.braininghub.BHManagementSystem.util;

import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;

public final class StringUtil {

    private StringUtil() {

    }

    public static boolean noneEmpty(String... inputs) {

        if (inputs == null) {
            return false;
        }

        List<String> inputList = Optional.ofNullable(Arrays.asList(inputs))
                .orElse(Collections.EMPTY_LIST);
        if (inputList.isEmpty()) {
            return false;
        }

        return inputList.stream().allMatch(p -> p != null && !"".equalsIgnoreCase(p));

    }

}
