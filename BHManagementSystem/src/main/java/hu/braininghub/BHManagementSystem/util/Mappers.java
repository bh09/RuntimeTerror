/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.util;

import hu.braininghub.BHManagementSystem.dto.AnswersDTO;

import hu.braininghub.BHManagementSystem.dto.InterviewDTO;

import hu.braininghub.BHManagementSystem.dto.EventDTO;

import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.TeacherDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.dto.exam.ExamDTO;
import hu.braininghub.BHManagementSystem.dto.exam.ExamResultDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkDto;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkResultDto;
import hu.braininghub.BHManagementSystem.entities.Interview;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.Event;
import hu.braininghub.BHManagementSystem.entities.exam.Exam;
import hu.braininghub.BHManagementSystem.entities.exam.ExamResult;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.homework.HomeworkResult;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.entities.user.Teacher;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rajnaig
 */
public class Mappers {

    private static final Logger log = LoggerFactory.getLogger(Mappers.class);

    public static TeamDTO mapTeamFromEntity(Team team) {

        TeamDTO teamDTO = new TeamDTO();
        try {
            BeanUtils.copyProperties(teamDTO, team);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("mapTeamFromEntity: {}", ex);
        }
        return teamDTO;
    }

    public static TeamDTO mapTeamFromEntityNormal(Team team) {
        TeamDTO teamDTO = new TeamDTO();
        teamDTO.setId(team.getId());
        teamDTO.setGoogleCalendarURL(team.getGoogleCalendarURL());
        teamDTO.setTeamName(team.getTeamName());

        return teamDTO;
    }

    public static HomeworkDto mapHomeworkDtofromEntity(Homework homework) {
        HomeworkDto dto = new HomeworkDto();
        dto = new HomeworkDto(
                homework.getId(),
                homework.getTitle(),
                homework.getDescription(),
                homework.getMaxPoints(),
                homework.getDateReleased().toLocalDate(),
                homework.getDeadline().toLocalDate(),
                mapTeamFromEntity(homework.getTeam())); //ide rakjunk Loggert
        return dto;
    }

    public static HomeworkResultDto mapHomeworkResultDtoFromEntity(HomeworkResult homeworkResult) throws IllegalAccessException, InvocationTargetException {

        return new HomeworkResultDto(
                mapStudentFromEntity(homeworkResult.getStudent()),
                mapHomeworkDtofromEntity(homeworkResult.getHomework()),
                homeworkResult.getResultPoints());
    }

    public static AnswersDTO mapAnswersFromEntity(Answers answers) {

        AnswersDTO answersDTO = new AnswersDTO();
        answersDTO.setAnswer1(answers.getAnswer1());
        answersDTO.setAnswer2(answers.getAnswer2());
        answersDTO.setAnswer3(answers.getAnswer3());
        answersDTO.setComment1(answers.getComment1());
        answersDTO.setComment2(answers.getComment2());
        answersDTO.setComment3(answers.getComment3());
        answersDTO.setId(answers.getId());
        answersDTO.setQuestions(Mappers.mapQuestionsFromEntity(answers.getQuestions()));
        answersDTO.setScore1(answers.getScore1());
        answersDTO.setScore2(answers.getScore2());
        answersDTO.setScore3(answers.getScore3());
        answersDTO.setStudent(Mappers.mapStudentToStudentDTO(answers.getStudent()));
        return answersDTO;
    }

    public static StudentDTO mapStudentFromEntity(Student student) {
        System.out.println("Elkezdődik a Student entity mappelése DTO-vá");
        StudentDTO studentDTO = new StudentDTO();
        try {
            BeanUtils.copyProperties(studentDTO, student);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            //Loggert ide
        }
        System.out.println("véget ért a Student entity mappelése DTO-vá");
        return studentDTO;
    }

    public static QuestionsDTO mapQuestionsFromEntity(Questions questions) {

        QuestionsDTO questionsDTO = new QuestionsDTO();
        /*
        try {
            BeanUtils.copyProperties(questionsDTO, questions);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(Mappers.class.getName()).log(Level.SEVERE, null, ex);//Loggert ide
        }
         */
        questionsDTO.setFirstQuestion(questions.getFirstQuestion());
        questionsDTO.setSecondQuestion(questions.getSecondQuestion());
        questionsDTO.setThirdQuestion(questions.getThirdQuestion());
        questionsDTO.setId(questions.getId());
        questionsDTO.setTeamDTO(Mappers.mapTeamFromEntity(questions.getTeam()));
        questionsDTO.setFireQuestion(questions.getFireQuestion());

        return questionsDTO;
    }

    public static Questions mapEntityFromQuestionsDTO(QuestionsDTO questionsDTO)
            throws IllegalAccessException, InvocationTargetException {

        Questions questions = new Questions();
        BeanUtils.copyProperty(questions, "id", questionsDTO);
        BeanUtils.copyProperty(questions, "firstQuestion", questionsDTO);
        BeanUtils.copyProperty(questions, "secondQuestion", questionsDTO);
        BeanUtils.copyProperty(questions, "thirdQuestion", questionsDTO);

        BeanUtils.copyProperty(questions, "team", mapEntityFromTeamDTO(questionsDTO.getTeam()));
        return questions;
    }

    public static Team mapEntityFromTeamDTO(TeamDTO teamDTO)
            throws IllegalAccessException, InvocationTargetException {

        Team team = new Team();
        BeanUtils.copyProperties(team, teamDTO);
        return team;
    }

    public static StudentDTO mapStudentToStudentDTO(Student student) {
        System.out.println("Bejött a mapper-be");
        StudentDTO students = new StudentDTO();
        students.setBirtDate(student.getBirtDate());
        students.setId(student.getId());
        students.setName(student.getName());
        students.setPhoneNumber(student.getPhoneNumber());

        students.setFirstInterview(mapStudentDtoHelper(student.getFirstInterview()));
        students.setSecondInterview(mapStudentDtoHelper(student.getSecondInterview()));

        students.setTeam(mapTeamFromEntityNormal(student.getTeam())); //logger
        return students;

    }

    public static StudentDTO mapStudentTodSTudentDtoCustom(Student student) {
        StudentDTO students = new StudentDTO();
        students.setBirtDate(student.getBirtDate());
        students.setId(student.getId());
        students.setName(student.getName());
        students.setPhoneNumber(student.getPhoneNumber());
        return students;

    }

    ;

    public static EventDTO mapEventDTOFromEntity(Event event) {
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId(event.getId());
        eventDTO.setExpiry(event.getExpiry());
        eventDTO.setMessage(event.getMessage());
        eventDTO.setTeamDTO(Mappers.mapTeamFromEntityNormal(event.getTeam()));
        return eventDTO;
    }

    public static TeacherDTO mapTeacherFromEntity(Teacher teacher) {
        TeacherDTO teacherDTO = new TeacherDTO();

        try {
            BeanUtils.copyProperties(teacherDTO, teacher);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            //Loggert ide
        }
        return teacherDTO;
    }

    public static boolean mapStudentDtoHelper(int interview) {

        if (interview == 0) {
            return false;
        } else {

            return true;
        }

    }

    
    
    public static List<InterviewDTO> mapInterviewToInterviewDTO(List<Interview> interviewList) {

        if (interviewList == null) {
            return Collections.emptyList();

        }

        List<InterviewDTO> returningDtoList = new ArrayList<>();

        interviewList.forEach(p -> {
            InterviewDTO interviewdto = new InterviewDTO();
            interviewdto.setId(p.getId());
            interviewdto.setHrComment(p.getHrComments());
            interviewdto.setHrPoints(p.getHrPoints());
            interviewdto.setJavaComment(p.getJavaComments());
            interviewdto.setJavaPoints(p.getJavaPoints());
            returningDtoList.add(interviewdto);

        });

        log.info("mapInterviewToInterviewDTO: returningDtoList: {}", returningDtoList);

        return returningDtoList;

    }

    public static Student mapStudentDTOToStudent(StudentDTO sDTo, Student target) {
        target.setName(sDTo.getName());
        target.setBirtDate(sDTo.getBirtDate());
        target.setPhoneNumber(sDTo.getPhoneNumber());

        return target;
    }

    public static ExamDTO mapSingleExamToExamDTO(Exam e) {

        ExamDTO dto = new ExamDTO();
        dto.setId(e.getId());
        dto.setExamDate(e.getExamDate().toLocalDate());
        dto.setExamType(e.getExamType());
        dto.setTeamDTO(Mappers.mapTeamFromEntityNormal(e.getTeam()));
        dto.setMaxPopintsTheory(e.getMaxPopintsTheory());
        dto.setMaxPointsJava(e.getMaxPointsJava());
        dto.setMaxPointsTeamwork(e.getMaxPointsTeamwork());
        //dto.setExamResultDTOs(mapExamResultListToDTOs(e.getExamResults()));

        return dto;
    }

    public static ExamResultDTO mapSingleExamResultToDTO(ExamResult e) {

        ExamResultDTO dto = new ExamResultDTO();
        dto.setId(e.getId());
        dto.setExamDTO(mapSingleExamToExamDTO(e.getExam()));
        dto.setStudentDTO(mapStudentToStudentDTO(e.getStudent()));
        dto.setResultPointsJava(e.getResultPointsJava());
        dto.setResultPointsTheory(e.getResultPointsTheory());
        dto.setResultPointsTeamwork(e.getResultPointsTeamwork());

        return dto;

    }

    public static List<ExamDTO> mapExamListToDtoList(List<Exam> sourceList) {
        List<ExamDTO> dtoList = new ArrayList<>();
        for (Exam exam : sourceList) {
            dtoList.add(mapSingleExamToExamDTO(exam));
        }
        return dtoList;
    }

    public static List<ExamResultDTO> mapExamResultListToDTOs(List<ExamResult> sourceList) {
        List<ExamResultDTO> dtoList = new ArrayList<>();

        for (ExamResult e : sourceList) {
            dtoList.add(mapSingleExamResultToDTO(e));
        }
        return dtoList;

    }

}
