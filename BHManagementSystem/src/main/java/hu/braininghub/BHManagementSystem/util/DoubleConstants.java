/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.util;

/**
 *
 * @author rajnaig
 */
public enum DoubleConstants {
PASSINGPERCENT{
    @Override
    public Double getConstantValue() {
        return 0.6;
    }    
},
EXAMQPERCENTAGE{
    @Override
    public Double getConstantValue() {
        return 0.2;
    }
    
},
HOMEWORKQPERCENTAGE{
    @Override
    public Double getConstantValue() {
       return 0.2;
    }
    
},
QUIZQPERCENTAGE{
    @Override
    public Double getConstantValue() {
        return 0.1;
    }
    
},
INTERVIEWQPERCENTAGE{
    @Override
    public Double getConstantValue() {
     return 0.1;
    }
    
}
;
public abstract Double getConstantValue();
}
