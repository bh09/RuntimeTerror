/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.service.ProfileService;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bence58
 */
@WebServlet(name = "ProfileServlet", urlPatterns = {"/Profile"})
public class ProfileServlet extends HttpServlet {

    @EJB
    ProfileService ps;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("student", ps.getStudentById(10L));
        // itt sessionbol kéne az id azalapjan keresni a dbbol és visszaadni egy objektumot megfelelő adatokkal, majd besettelni attributosan.
        request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StudentDTO stdto = new StudentDTO();
        stdto.setId(Long.parseLong(request.getParameter("id")));
        stdto.setName(request.getParameter("name"));
        stdto.setBirtDate(Date.valueOf(request.getParameter("birthDate")));
        stdto.setPhoneNumber(Integer.parseInt(request.getParameter("phoneNumber")));

        ps.updateStudentProfile(stdto);

        // ha megnyomom a gombot akkor a formbol megkapok mindent és updatelem a megfelelő idnel
        request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
