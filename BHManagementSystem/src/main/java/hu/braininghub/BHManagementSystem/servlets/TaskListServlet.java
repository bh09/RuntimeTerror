/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.QuestionBean;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "TaskListServlet", urlPatterns = {"/TaskListServlet"})
public class TaskListServlet extends HttpServlet {
 
@EJB
QuestionBean questions;
@EJB
Links links;        
HttpServletRequest req;
    public TaskListServlet() {
    }


    public TaskListServlet(QuestionBean questions,HttpServletRequest req) {
        this.questions = questions;
        this.req=req;
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "TaskListServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        request.setAttribute("teamID", teamID);
        request.setAttribute("questionList", questions.getAllQuestionsForTeams(teamID));
        
        request.setAttribute("id", 1);
        request.setAttribute("firstQuestion", 2);
        request.setAttribute("secondQuestion", 3);
        request.setAttribute("thirdQuestion", 4);
        
        request.setAttribute("idText", "Kérdések száma");
        request.setAttribute("firstQuestionText", new String("Első kérdés".getBytes(),Charset.forName("Latin1")));
        request.setAttribute("secondQuestionText", new String("Második kérdés".getBytes(),Charset.forName("Latin1")));
        request.setAttribute("thirdQuestionText", new String("Harmadik kérdés".getBytes(),Charset.forName("Latin1")));
         
        request.setAttribute("limit", 10);
    request.getRequestDispatcher("/WEB-INF/TaskList.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("Latin2");
         HttpSession session = request.getSession();
        session.setAttribute("servletName", "TaskListServlet");
        Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);    
        }else{
        teamID=(Long)session.getAttribute("teamID");
                }
        String orderInput=request.getParameter("quiztable");
        Integer order=orderInput(orderInput);
        String limitString=request.getParameter("limitTable");
        Integer limit;
        if(limitString==null){
            limit=10;
        }else{
            limit=Integer.parseInt(limitString);
        }
 
    request.setAttribute("limit", limit);
    
    String search=request.getParameter("search");
    
    List<QuestionsDTO> question;
    if(search==null){
      question=questions.getAllQuestionsForTeams(teamID);
    }else{
      question=questions.searchQuestionsForTeam(teamID, search);
    }
    request.setAttribute("id", 1);
        request.setAttribute("firstQuestion", 2);
        request.setAttribute("secondQuestion", 3);
        request.setAttribute("thirdQuestion", 4);
        
        request.setAttribute("idText", new String("Kérdések száma".getBytes(),Charset.forName("Latin1")));
        request.setAttribute("firstQuestionText", new String("Első kérdés".getBytes(),Charset.forName("Latin1")));
        request.setAttribute("secondQuestionText", new String("Második kérdés".getBytes(),Charset.forName("Latin1")));
        request.setAttribute("thirdQuestionText", new String("Harmadik kérdés".getBytes(),Charset.forName("Latin1")));
        
     switch (order) {
        case -1: question=sortedByIDReverse(question,limit);
            
            
            break;
        case -2: question=sortedByFirstQuestionReverse(question,limit);
            request.setAttribute("firstQuestion", 2);
            
            break;
        case -3: question=sortedBySecondQuestionReverse(question,limit);
        request.setAttribute("secondQuestion", 3);
        
            break;
        case -4: question=sortedByThirdQuestionReverse(question,limit);
        request.setAttribute("thirdQuestion", 4);
        
            break;
         case 1: question=sortedByID(question,limit);
            request.setAttribute("id", -1);
            
            break;
        case 2: question=sortedByFirstQuestion(question,limit);
            request.setAttribute("firstQuestion", -2);
            
            break;
        case 3: question=sortedBySecondQuestion(question,limit);
        request.setAttribute("secondQuestion", -3);
        
            break;
        case 4: question=sortedByThirdQuestion(question,limit);
        request.setAttribute("thirdQuestion", -4);
        
            break;   
    }   
    
    request.setAttribute("questionList", question);
        String overwriteID=request.getParameter("overwriteQuestion");
        if (overwriteID!=null) {
            Long questionID=Long.parseLong(overwriteID);
            String firstQuestion=request.getParameter(new StringBuffer().append("firstQuestion").append(questionID).toString());
            String secondQuestion=request.getParameter(new StringBuffer().append("secondQuestion").append(questionID).toString());
            String thirdQuestion=request.getParameter(new StringBuffer().append("thirdQuestion").append(questionID).toString());
            questions.overwriteQuestion(questionID, firstQuestion, secondQuestion, thirdQuestion);
        request.setAttribute("questionList", questions.getAllQuestionsForTeams(teamID));
        }
       String deleteQuestion=request.getParameter("deleteQuestion");
        if (deleteQuestion!=null) {
            Long questionID=Long.parseLong(deleteQuestion);
            
            questions.deleteQuestionByQuestionID(questionID);
        request.setAttribute("questionList", questions.getAllQuestionsForTeams(teamID));
        }
    String fireQuestion=request.getParameter("fireQuestion");
        if (fireQuestion!=null) {
            Long questionID=Long.parseLong(fireQuestion);
            
                String areYouSure=request.getParameter("areYouSureFireQuestion");
                System.out.println(areYouSure+"areYouSure");
                if ("I".equals(areYouSure)) {
                questions.fireQuestion(questionID);    
                }    
        request.setAttribute("questionList", questions.getAllQuestionsForTeams(teamID));
        }
    //Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/TaskList.jsp").forward(request, response);   
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége    
    }
    public List<QuestionsDTO> sortedByIDReverse(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->{return ques2.getId().intValue()-ques1.getId().intValue();})
            .limit(limit)
            .collect(Collectors.toList());
            
    }
    public List<QuestionsDTO> sortedByID(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->{return ques1.getId().intValue()-ques2.getId().intValue();})
            .limit(limit)
                .collect(Collectors.toList());
            
    }
    public List<QuestionsDTO> sortedByFirstQuestionReverse(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->ques2.getFirstQuestion().compareTo(ques1.getFirstQuestion()))
            .limit(limit)
                .collect(Collectors.toList());
            
    }
    public List<QuestionsDTO> sortedByFirstQuestion(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->ques1.getFirstQuestion().compareTo(ques2.getFirstQuestion()))
            .limit(limit)
                .collect(Collectors.toList());
            
    }
    public List<QuestionsDTO> sortedBySecondQuestionReverse(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->ques2.getSecondQuestion().compareTo(ques1.getSecondQuestion()))
            .limit(limit)
                .collect(Collectors.toList());
            
    }
    public List<QuestionsDTO> sortedBySecondQuestion(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->ques1.getSecondQuestion().compareTo(ques2.getSecondQuestion()))
            .limit(limit)
                .collect(Collectors.toList());
            
    }
     public List<QuestionsDTO> sortedByThirdQuestionReverse(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->ques2.getThirdQuestion().compareTo(ques1.getThirdQuestion()))
            .limit(limit)
                .collect(Collectors.toList());
            
    }
     public List<QuestionsDTO> sortedByThirdQuestion(List<QuestionsDTO> question,Integer limit){
        return question.stream()
            .sorted((QuestionsDTO ques1,QuestionsDTO ques2 )->ques1.getThirdQuestion().compareTo(ques2.getThirdQuestion()))
            .limit(limit)
                .collect(Collectors.toList());
            
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
public Integer orderInput(String orderInput){
    return orderInput!=null ? Integer.parseInt(orderInput) : 1; 
}

}
