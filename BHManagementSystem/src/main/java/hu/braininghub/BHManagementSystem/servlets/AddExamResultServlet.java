/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.service.ExamResultService;
import hu.braininghub.BHManagementSystem.service.ExamService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.StudentInterface;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "AddExamResultServlet", urlPatterns = {"/AddExamResultServlet"})
public class AddExamResultServlet extends HttpServlet {

    @EJB
    TeamService teamService;

    @EJB
    Links links;

    @EJB(name = "ejb/students", description = "stateless")
    StudentInterface studentRervice;

    @EJB
    ExamService examService;

    @EJB
    ExamResultService examResultService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String examIDstring = (String) session.getAttribute("examId");
        request.setAttribute("examId", examIDstring);
        session.setAttribute("servletName", "AddExamResultServlet");
        Long teamID = (Long) session.getAttribute("teamID");

        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }

        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }

        request.setAttribute("exam", examService.getExamDTOByTeamId(examIDstring));
        request.setAttribute("studentList", examService.getAllStudentsByExamId(examIDstring));
        request.setAttribute("ExamResultMap", examResultService.createExamResultMap(teamID, examIDstring));

        String inputLink = request.getParameter("inputLink");

        String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
        if (null == redirectURL) {
            request.getRequestDispatcher("/WEB-INF/AddExamResult.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AddExamResultServlet");

        Long teamID = (Long) session.getAttribute("teamID");

        String examIDstring = (String) session.getAttribute("examId");
        request.setAttribute("examId", examIDstring);
        String redirectURL = null;

        String studentId = request.getParameter("evaluate");
        String theoryPoints = request.getParameter("theoryPoints");
        String javaPoints = request.getParameter("javaPoints");
        String teamworkPoints = request.getParameter("teamworkPoints");

        if (request.getParameter("evaluate") == null) {
            redirectURL = links.getURLTeacher("examedit", session.getAttribute("servletName").toString());

        } else {

            if (!"".equals(request.getParameter("evaluate"))) {
                if (!examResultService.isThereExamResultPresentOfAStudentToExam(examIDstring, studentId)) {
                    examResultService.createAndSaveExamResult(studentId, examIDstring, theoryPoints, javaPoints, teamworkPoints);

                } else {
                    System.out.println("studentId "  + studentId + "examId " + examIDstring + "theory: " +  theoryPoints + "java: " + javaPoints + "team: " + teamworkPoints);
                    
                    
                    examResultService.updateExamResult(studentId, examIDstring, theoryPoints, javaPoints, teamworkPoints);

                }
                request.setAttribute("evaluate", "");
            }
            session.setAttribute("examId", examIDstring);
            request.setAttribute("exam", examService.getExamDTOByTeamId(examIDstring));
            request.setAttribute("studentList", examService.getAllStudentsByExamId(examIDstring));
            request.setAttribute("ExamResultMap", examResultService.createExamResultMap(teamID, examIDstring));
            String inputLink = request.getParameter("inputLink");
            redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());

        }
        session.setAttribute("examId", examIDstring);
        request.setAttribute("exam", examService.getExamDTOByTeamId(examIDstring));
        request.setAttribute("studentList", examService.getAllStudentsByExamId(examIDstring));
        request.setAttribute("ExamResultMap", examResultService.createExamResultMap(teamID, examIDstring));
        
        

        if (null == redirectURL) {
            request.getRequestDispatcher("/WEB-INF/AddExamResult.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
