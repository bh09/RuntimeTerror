package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.service.ExamResultService;
import hu.braininghub.BHManagementSystem.service.ExamService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "DisplayExamsServlet", urlPatterns = {"/DisplayExamsServlet"})
public class DisplayExamsServlet extends HttpServlet {

    @EJB
    ExamService examService;

    @EJB
    ExamResultService examResultService;

    @EJB
    Links links;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AddExamServlet");
        Long teamID = (Long) session.getAttribute("teamID");

        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }

        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }

        request.setAttribute("examList", examService.getAllExamDTOsByTeamId(String.valueOf(teamID)));
        request.getRequestDispatcher("/WEB-INF/DisplayExams.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("bejött a postba");
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AddExamServlet");
        Long teamID = null;

        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }

        if (request.getParameter("evaluate") != null) {
            System.out.println("bejött az evaluate-b az displayExamban");
            session.setAttribute("examId", request.getParameter("evaluate"));
            response.sendRedirect("AddExamResultServlet");

        } else {

            request.setAttribute("examList", examService.getAllExamDTOsByTeamId(String.valueOf(teamID)));

            String inputLink = request.getParameter("inputLink");

            String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
            if (null == redirectURL) {
                request.getRequestDispatcher("/WEB-INF/DisplayExams.jsp").forward(request, response);
            } else {
                response.sendRedirect(redirectURL);
            }

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
