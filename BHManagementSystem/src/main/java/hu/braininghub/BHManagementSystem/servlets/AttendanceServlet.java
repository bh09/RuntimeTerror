package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.Attendance;
import hu.braininghub.BHManagementSystem.service.AttendanceService;
import hu.braininghub.BHManagementSystem.service.InterviewService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "AttendanceServlet", urlPatterns = {"/AttendanceServlet"})
public class AttendanceServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(AttendanceServlet.class);

    @EJB
    InterviewService is;

    @EJB
    AttendanceService as;

    @EJB
    Links links;

    private List<StudentDTO> peoplesAttendance = new ArrayList();
    Attendance attendance = new Attendance();

    @Override

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AttendanceServlet");
        Long teamID = (Long) session.getAttribute("teamID");
        //Kell a navbarhoz vége
        if (teamID == -1L) {
            //ide a válassz csoporto szöveg
        } else {
            peoplesAttendance = getStudentsByGrupId(teamID);
            request.setAttribute("listOfStudents", peoplesAttendance);
        }

        request.getRequestDispatcher("WEB-INF/attendance.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //ez kell hozzá, hogy működjön a navbar
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AttendanceServlet");
        Long teamID = null;
        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }
        if (null == request.getParameter("studentName")) {
            if (teamID == -1L) {
                //ide a válassz csoportot szöveg
            } else {
                peoplesAttendance = getStudentsByGrupId(teamID);
                request.setAttribute("listOfStudents", peoplesAttendance);
            }
           
        } else {
            attendance.setId(null);
            attendance.setName(request.getParameter("studentName"));
            attendance.setMissingTime(Integer.parseInt(request.getParameter("missing")));
            attendance.setDate(Timestamp.valueOf(LocalDateTime.now()));
            attendance.setStudentId(Integer.parseInt(request.getParameter("studentId")));

            peoplesAttendance.removeIf(StudentDTO -> StudentDTO.getId() == Integer.parseInt(request.getParameter("studentId")));

            request.setAttribute("listOfStudents", peoplesAttendance);
            log.debug("post attendance: {}", attendance);
            System.out.println("studentsAttendance: " + request.getParameter("studentName"));
            System.out.println("studentsAttendance késés: " + request.getParameter("missing"));

            as.saveAttendances(attendance);
        }
        //Kell a navbarhoz
        String inputLink = request.getParameter("inputLink");

        String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
        if (null == redirectURL) {
            request.getRequestDispatcher("WEB-INF/attendance.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }
        //kell a navbarhoz vége

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public List<StudentDTO> getStudentsByGrupId(Long teamID) {

        return is.getStudents(teamID);

    }

}
