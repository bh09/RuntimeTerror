/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.service.HomeworkResultService;
import hu.braininghub.BHManagementSystem.service.HomeworkService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.StudentBean;
import hu.braininghub.BHManagementSystem.service.StudentInterface;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "AddHomeworkResultServlet", urlPatterns = {"/AddHomeworkResultServlet"})
public class AddHomeworkResultServlet extends HttpServlet {

    @EJB
    TeamService teamService;

    @EJB
    HomeworkService homeworkService;

    @EJB
    Links links;

    @EJB(name = "ejb/students", description = "stateless")
    StudentInterface studentRervice;

    @EJB
    HomeworkResultService homeworkResultService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Long teamId;
        HttpSession session = request.getSession();
        String homeworkIDstring = (String) session.getAttribute("homeworkId");
        request.setAttribute("homeworkId", homeworkIDstring);
        session.setAttribute("servletName", "AddHomeworkResultServlet");

        //getting the team to this Homework
        teamId = homeworkService.getHomeworkDtoById(
                homeworkService.parseStringToLong(homeworkIDstring))
                .getTeam()
                .getId();
        //getting the Students of this team and setting them as attribute
        List<StudentDTO> students = studentRervice.getAllStudentsOfATeam(teamId);
        //request.setAttribute("students", students);

        //creating the Maxpoints dropdown from the maxPoint of homework
        Map<StudentDTO, List<Integer>> dropDowns = homeworkService.createPointsDropDownByStudentID(students, homeworkIDstring);
        request.setAttribute("pointsDropdowns", dropDowns);

        System.out.println("átadódnak az adatok az oldla megrajzolásáshoz");
        String inputLink = request.getParameter("inputLink");

        String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
        if (null == redirectURL) {
            request.getRequestDispatcher("/WEB-INF/addHomeworkResult.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AddHomeworkResultServlet");
        System.out.println(request.getParameter("create") + "create");
        Long teamId;

        /*if (request.getParameter("evaluate") != null) {

            //getting homeworkId from post and setting as attribute
            String homeworkId = request.getParameter("evaluate");
            request.setAttribute("homeworkId", homeworkId);
            

            //creating the Maxpoints dropdown from the maxPoint of homework
            List<Integer> dropDown = homeworkService.createHomeworkResultPointsDropDown(homeworkId);
            request.setAttribute("pointsDropdown", dropDown);
            

            //getting the team to this Homework
            teamId = homeworkService.getHomeworkDtoById(
                    homeworkService.parseStringToLong(homeworkId))
                    .getTeam()
                    .getId();

            //getting the Students of this team and setting them as attribute
            List<StudentDTO> students = studentRervice.getAllStudentsOfATeam(teamId);
            request.setAttribute("students", students);
            
            System.out.println("átadódnak az adatok az oldla megrajzolásáshoz");
            request.getRequestDispatcher("/WEB-INF/addHomeworkResult.jsp").forward(request, response);

        }*/
        String homeworkIDstring = (String) session.getAttribute("homeworkId");
        request.setAttribute("homeworkId", homeworkIDstring);
        String redirectURL = null;
        if (request.getParameter("create") == null) {
            redirectURL = links.getURLTeacher("homeworkedit", session.getAttribute("servletName").toString());

        } else {

            if (!"".equals(request.getParameter("create"))) {

                //for this persist I need Student ---- (Homework + Point)
                //getting homeworkId
                String homeworkId = request.getParameter("homeworkId");
                System.out.println("ELSŐ BLOKK KÉSZ");

                //getting List of Students who belong to the Team regarding this Homework
                teamId = homeworkService.getHomeworkDtoById(
                        homeworkService.parseStringToLong(homeworkId))
                        .getTeam()
                        .getId();
                System.out.println("A lekérdezett teamId: " + teamId);
                List<StudentDTO> students = studentRervice.getAllStudentsOfATeam(teamId);
                System.out.println("A studentek száma: " + students.size());
                System.out.println("MÁSODIK BLOKK KÉSZ");

                StudentDTO student = studentRervice.getStudentByStudentID(Long.parseLong(request.getParameter("create")));
                String attributeName = "resultPoints_" + student.getId();
                System.out.println("atrributeName: " + attributeName);
                if (!homeworkResultService.isHomeworkResultExists(request.getParameter("create"), homeworkId)) {
                    homeworkResultService.addNewHomeWorkResult(
                            student.getId(),
                            homeworkService.parseStringToLong(homeworkId),
                            Integer.valueOf(request.getParameter(attributeName)));
                } else {
                    Long homeworkID = homeworkService.parseStringToLong(homeworkId);
                    Long studentID = homeworkService.parseStringToLong(request.getParameter("create"));
                    homeworkResultService.updateHomeworkResult(Integer.parseInt(request.getParameter("resultPoints_" + studentID)), studentID, homeworkID);
                }
                request.setAttribute("create", "");
            }

            //getting the team to this Homework
            teamId = homeworkService.getHomeworkDtoById(
                    homeworkService.parseStringToLong(homeworkIDstring))
                    .getTeam()
                    .getId();
            //getting the Students of this team and setting them as attribute
            List<StudentDTO> students = studentRervice.getAllStudentsOfATeam(teamId);
            //request.setAttribute("students", students);

            //creating the Maxpoints dropdown from the maxPoint of homework
            Map<StudentDTO, List<Integer>> dropDowns = homeworkService.createPointsDropDownByStudentID(students, homeworkIDstring);
            request.setAttribute("pointsDropdowns", dropDowns);
            System.out.println("átadódnak az adatok az oldla megrajzolásáshoz");
            String inputLink = request.getParameter("inputLink");

            redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
        }
        if (null == redirectURL) {
            request.getRequestDispatcher("/WEB-INF/addHomeworkResult.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }

        //response.sendRedirect("DisplayHomeworkResultServlet");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
