/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.service.StudentInterface;
import hu.braininghub.BHManagementSystem.service.TeacherInterface;
import hu.braininghub.BHManagementSystem.service.UsersBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "index", urlPatterns = {"/index"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"teacher","student"}))
@Log4j2
public class StudentOrTeacher extends HttpServlet {
@EJB
UsersBean users;
@EJB(name = "ejb/students", description = "stateless")
    StudentInterface studentRervice;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();     
        if (request.isUserInRole("teacher")) {    
             Long teacherID=users.getUserIDbyEmail(request.getRemoteUser());
             log.info(request.getRemoteUser()+" user entered with role: teacher sessionID= "+session.getId());
             session.setAttribute("teacherID", teacherID);
                 response.sendRedirect("TeacherServlet");
    } else {
            Long studentID=users.getUserIDbyEmail(request.getRemoteUser());
             log.info(request.getRemoteUser()+" user entered with role: student sessionID= "+session.getId());
             session.setAttribute("studentID", studentID);
             StudentDTO student=studentRervice.getStudentByStudentID(studentID);
             TeamDTO team=student.getTeam();
              session.setAttribute("teamID", team.getId());
                 response.sendRedirect("StudentServlet");
    }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
