/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkDto;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkResultDto;
import hu.braininghub.BHManagementSystem.service.HomeworkResultService;
import hu.braininghub.BHManagementSystem.service.HomeworkService;
import hu.braininghub.BHManagementSystem.service.StudentBean;
import hu.braininghub.BHManagementSystem.service.StudentInterface;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "DisplayHomeworkResultServlet", urlPatterns = {"/DisplayHomeworkResultServlet"})
public class DisplayHomeworkResultServlet extends HttpServlet {
    
    @EJB
    HomeworkResultService homeworkResultService;
    
    @EJB
    HomeworkService homeworkService;
    
    @EJB
    TeamService teamService;
    
    @EJB(name="ejb/students", description="stateless")
    StudentInterface studentService;

    //Gábor ide kell bekötni a navbar-t, hogy a navbar egy teamId-t adjon paraméterként/attributumként
    //a tanár egyik team-jének az id-je kellene
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String teamId;
        if (null == request.getParameter("teamId")) {
            teamId = "7";
        } else {
            teamId = request.getParameter("teamId");
        }

        //setting up team select dropdown
        List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), teamId);
        request.setAttribute("teamList", orderedTeamList);

        //setting up student select dropdown
        List<StudentDTO> studentList = homeworkResultService.getStudentDropdownOfATeam(teamId);
        request.setAttribute("studentList", studentList);

        //setting up Homework select dropdown, with "-1L" meaning "ShowAll" in dropDown 
        List<HomeworkDto> homeworkList = homeworkService.getOrderedHomeworkDropdown(teamId, "-1");
        request.setAttribute("homeworkList", homeworkList);

        //setting up custom homeworkResult List by student and homework
        List<HomeworkResultDto> homeworkResultsCustomList = homeworkResultService.getAllHomeWorkResultDtosOfATeam(teamId);
        request.setAttribute("homeworkResultList", homeworkResultsCustomList);
        
        request.getRequestDispatcher("/WEB-INF/DisplayHomeworkResult.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String teamId = request.getParameter("teamDropdown");
        String studentId = request.getParameter("studentDropdown");
        String homeworkId = request.getParameter("homeworkDropdown");

        //setting up team select dropdown
        List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), teamId);
        request.setAttribute("teamList", orderedTeamList);

        //setting up student select dropdown
        List<StudentDTO> studentList = homeworkResultService.getOrderedStudentDropdown(teamId, studentId);
        request.setAttribute("studentList", studentList);

        //setting up custom Homework select dropdown
        List<HomeworkDto> homeworkList = homeworkService.getOrderedHomeworkDropdown(teamId, homeworkId);
        request.setAttribute("homeworkList", homeworkList);

        //getting resultList and setting up as Attribute
        List<HomeworkResultDto> homeworkResultsCustomList = homeworkResultService.getResultList(teamId, studentId, homeworkId);
        request.setAttribute("homeworkResultList", homeworkResultsCustomList);
        
        request.setAttribute("teamid", teamId);
        request.setAttribute("studentId", studentId);
        request.setAttribute("homeworkId", homeworkId);
        
        if (homeworkResultService.checkHomeworkIdCoherency(teamId, homeworkId)
                && homeworkResultService.checkStudentIdCoherency(teamId, studentId)) {
            request.getRequestDispatcher("/WEB-INF/DisplayHomeworkResult.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/DisplayHomeworkResultServlet" + "?teamId=" + String.valueOf(teamId));
        }
        
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
