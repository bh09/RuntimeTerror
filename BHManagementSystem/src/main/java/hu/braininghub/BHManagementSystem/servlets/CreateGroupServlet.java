/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bence58
 */
@WebServlet(name = "CreateGroup", urlPatterns = {"/CreateGroup"})
public class CreateGroupServlet extends HttpServlet {
    
    @EJB
    TeamService ts;
    @EJB
    Links links;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    HttpSession session = request.getSession();
        session.setAttribute("servletName", "CreateGroup");    
        request.getRequestDispatcher("WEB-INF/creategroup.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    HttpSession session = request.getSession();
        session.setAttribute("servletName", "CreateGroup");        
        if (null!=request.getParameter("GroupSubmit")&&""!=request.getParameter("GroupSubmit")) {
        TeamDTO dTO = new TeamDTO();
        //

        dTO.setGoogleCalendarURL(request.getParameter("googleCallendar"));
        dTO.setTeamName(request.getParameter("groupname"));
        ts.createTeam(dTO);    
        }
         String inputLink = request.getParameter("inputLink");

            String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
            if (null == redirectURL) {
                request.getRequestDispatcher("WEB-INF/creategroup.jsp").forward(request, response);
            } else {
                response.sendRedirect(redirectURL);
            }
        
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}

