/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.user.GroupREALM;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.entities.user.Teacher;
import hu.braininghub.BHManagementSystem.entities.user.Users;
import hu.braininghub.BHManagementSystem.service.EncriptionService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.RegistrationService;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bence58
 */
@WebServlet(name = "RegisterStudent", urlPatterns = {"/RegisterStudent"})
public class RegisterStudent extends HttpServlet {

    @EJB
    Links links;

    @EJB
    EncriptionService es;

    @EJB
    RegistrationService rs;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "RegisterStudent");
        Long teamID = (Long) session.getAttribute("teamID");
        //Kell a navbarhoz vége
        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }
        request.setAttribute("randomPass", es.getDefaultPassword());

        request.getRequestDispatcher("/WEB-INF/registerstudent.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "RegisterStudent");
        Long teamID = null;
        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }
        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }
        request.setAttribute("randomPass", es.getDefaultPassword());
        System.out.println(request.getParameter("select"));
        System.out.println(request.getParameter("email"));
        if (null != request.getParameter("select") && request.getParameter("select").equalsIgnoreCase("student")) {
            Team team = new Team();
            team.setId(teamID); // sessionbol id
            Student newStudent = new Student();
            newStudent.setBirtDate(Date.valueOf(request.getParameter("birthDate")));

            Users users = new Users();
            users.setEmail(request.getParameter("email"));

            GroupREALM groups = new GroupREALM();
            groups.setGroupId("student");
            groups.setUsers(users);

            users.setGroups(groups);
            users.setPassword(es.getEncriptedPass(request.getParameter("password")));
            users.setUser(newStudent);

            System.out.println("group" + groups);

            newStudent.setUsers(users);
            newStudent.setName(request.getParameter("name"));
            newStudent.setPhoneNumber(Integer.parseInt(request.getParameter("phoneNumber")));
            newStudent.setTeam(team);

            System.out.println("tanulo" + newStudent);

            rs.addStudent(newStudent);

        } else if (null != request.getParameter("select") && request.getParameter("select").equalsIgnoreCase("teacher")) {

            Teacher teacher = new Teacher();

            teacher.setName(request.getParameter("name"));
            teacher.setBirtDate(Date.valueOf(request.getParameter("birthDate")));
            Users users = new Users();
            users.setEmail(request.getParameter("email"));

            GroupREALM groups = new GroupREALM();
            groups.setGroupId("teacher");
            groups.setUsers(users);

            users.setGroups(groups);
            users.setPassword(es.getEncriptedPass(request.getParameter("password")));

            users.setUser(teacher);
            teacher.setUsers(users);
            teacher.setPhoneNumber(Integer.parseInt(request.getParameter("phoneNumber")));
            rs.addTeacher(teacher);

        }
        //Kell a navbarhoz
        String inputLink = request.getParameter("inputLink");

        String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
        if (null == redirectURL) {
            request.getRequestDispatcher("/WEB-INF/registerstudent.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }
        //kell a navbarhoz vége

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
