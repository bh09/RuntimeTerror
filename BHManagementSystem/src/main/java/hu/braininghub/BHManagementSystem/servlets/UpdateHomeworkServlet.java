/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkDto;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.service.HomeworkService;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import org.apache.velocity.runtime.directive.Foreach;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "UpdateHomeworkServlet", urlPatterns = {"/UpdateHomeworkServlet"})
public class UpdateHomeworkServlet extends HttpServlet {

    @EJB
    HomeworkService homeworkService;

    @EJB
    TeamService teamService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "UpdateHomeworkServlet");
        String homeworkId = (String) session.getAttribute("homeworkId");
        HomeworkDto selectedHomework = homeworkService.getHomeworkDtoById(homeworkService.parseStringToLong(homeworkId));

        request.setAttribute("homework", selectedHomework);
        TeamDTO team = selectedHomework.getTeam();
        request.setAttribute("teamList", team);

        //getting the deeadline date to datepicker format
        String datePickerDate = homeworkService.getDatePickerFormatFromLocalDate(selectedHomework.getDeadline());
        request.setAttribute("datePickerDate", datePickerDate);

//        List<Integer> list = homeworkService.createPointsDropDownBySelectedPoint(selectedHomework.getMaxPoints());
        request.setAttribute("pointsDropDown", homeworkService.createPointsDropDownBySelectedPoint(selectedHomework.getMaxPoints()));

        List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), String.valueOf(selectedHomework.getTeam().getId()));
        request.setAttribute("teamList", orderedTeamList);
        request.getRequestDispatcher("/WEB-INF/updateHomework.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        String homeworkId = (String) session.getAttribute("homeworkId");
        //ez csak az updaten belülm van
        if (null == request.getParameter("back")) {
            if (request.getParameter("modify") != null) {
                Homework hw = homeworkService.createHomeworkEntityToBeMerged(
                        homeworkService.parseStringToLong(request.getParameter("homeworkId")),
                        request.getParameter("DateReleased"),
                        request.getParameter("date"),
                        request.getParameter("hwtitle"),
                        teamService.getTeamByIdService(request.getParameter("teamDropdown")),
                        request.getParameter("hwdesc"),
                        homeworkService.parseStringToInt(request.getParameter("maxPoints")));
                homeworkService.updateHomeworkDbRecord(hw);
//                
                //creating Homework DTO & setting as attribute
                //String homeworkId = (String) session.getAttribute("homeworkId");
                HomeworkDto selectedHomework = homeworkService.getHomeworkDtoById(homeworkService.parseStringToLong(homeworkId));
                request.setAttribute("homework", selectedHomework);

                //getting the deeadline date to datepicker format
                String datePickerDate = homeworkService.getDatePickerFormatFromLocalDate(selectedHomework.getDeadline());
                request.setAttribute("datePickerDate", datePickerDate);

                //creating ordered points dropdown & setting as attribute
                request.setAttribute("pointsDropDown", homeworkService.createPointsDropDownBySelectedPoint(selectedHomework.getMaxPoints()));

                //creating ordered teams dropdown & setting as attribute
                TeamDTO team = selectedHomework.getTeam();
                request.setAttribute("teamList", team);
                List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), String.valueOf(selectedHomework.getTeam().getId()));
                request.setAttribute("teamList", orderedTeamList);
                response.sendRedirect("DisplayHomeworkServlet");
            } else {

                System.out.println("az edit legelején paraméterként megkapott homeworkId: " + request.getParameter("edit"));
                //String homeworkId;
                if ((String) request.getParameter("edit") == null) {
                    homeworkId = (String) request.getParameter("homeworkId");
                    System.out.println("az első ágba megyünk be");
                } else {
                    homeworkId = (String) request.getParameter("edit");
                    System.out.println("a második ágba fututnk be");
                }

                //creating Homework DTO & setting as attribute 
                HomeworkDto selectedHomework = homeworkService.getHomeworkDtoById(homeworkService.parseStringToLong(homeworkId));
                request.setAttribute("homework", selectedHomework);

                //getting the deeadline date to datepicker format
                String datePickerDate = homeworkService.getDatePickerFormatFromLocalDate(selectedHomework.getDeadline());
                request.setAttribute("datePickerDate", datePickerDate);

                //creating ordered points dropdown & setting as attribute
                request.setAttribute("pointsDropDown", homeworkService.createPointsDropDownBySelectedPoint(selectedHomework.getMaxPoints()));

                //creating ordered teams dropdown & setting as attribute
                TeamDTO team = selectedHomework.getTeam();
                request.setAttribute("teamList", team);
                List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), String.valueOf(selectedHomework.getTeam().getId()));
                request.setAttribute("teamList", orderedTeamList);
                request.getRequestDispatcher("/WEB-INF/updateHomework.jsp").forward(request, response);
                //opening jsp
            }
            System.out.println("ez a végén lefut");
            
        } else {
            System.out.println("ez is lefut");

            response.sendRedirect("DisplayHomeworkServlet");
        }

    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
