/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.homework.Homework;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.entities.user.GroupREALM;
import hu.braininghub.BHManagementSystem.entities.user.Teacher;
import hu.braininghub.BHManagementSystem.entities.user.Users;
import hu.braininghub.BHManagementSystem.repositories.GroupsRepository;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.repositories.quiz.AnswerRepository;
import hu.braininghub.BHManagementSystem.repositories.quiz.QuestionRepository;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.repositories.TeacherRepository;

import hu.braininghub.BHManagementSystem.repositories.homework.HomeworkRepository;
import hu.braininghub.BHManagementSystem.repositories.UsersRepository;
import hu.braininghub.BHManagementSystem.util.Parser;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "TestServlet", urlPatterns = {"/TestServlet"})
public class CreateDummyDBServlet extends HttpServlet {
 
    @EJB
    QuestionRepository questionRepository;
    @EJB
    AnswerRepository answerRepository;
    @EJB
    TeamRepository teamRepository;
    @EJB
    StudentRepo studentRepository;
    
    @EJB
    TeacherRepository teacherRepository;
    
    @EJB
    HomeworkRepository homeworkRepository;
    
    
    
    //@EJB
    //UsersRepository usersRepository;
    //@EJB
    //GroupsRepository groupsRepository;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Questions question = new Questions();

        Answers answers = new Answers();
        Student studentIbiri = new Student();
        Student studentMityu = new Student();
        Student studentAkarki = new Student();
        Student studentAkarkiCsoda = new Student();
        Student studentNoname = new Student();
        Student studentNewName = new Student();
//
        Team team1 = new Team();
        Team team2 = new Team();
        Team team3 = new Team();
//        
        Teacher teacherZoli=new Teacher();
        Teacher teacherAttila=new Teacher();
        Teacher teacherBorisz=new Teacher();
        
//Teachers
        
        teacherZoli.setName("Zoli");
        Users usersZoli=new Users();
        usersZoli.setEmail("zoli@zoli.com");

        GroupREALM groupsZoli=new GroupREALM();
        groupsZoli.setGroupId("teacher");
        groupsZoli.setUsers(usersZoli);
        //groupsRepository.addGroups(groupsZoli);
        usersZoli.setGroups(groupsZoli);
        usersZoli.setPassword("97a82c02c7adc8a889979ddeb122ca7a");
//        usersRepository.addUsers(usersZoli);
        usersZoli.setUser(teacherZoli);
        teacherZoli.setUsers(usersZoli);
        teacherZoli.setPhoneNumber(123456);
        teacherRepository.addTeacher(teacherZoli);
        
        teacherAttila.setName("Attila");
        Users usersAttila=new Users();
        usersAttila.setEmail("attila@attila.com");

        GroupREALM groupsAttila=new GroupREALM();
        groupsAttila.setGroupId("teacher");
        groupsAttila.setUsers(usersAttila);
        //groupsRepository.addGroups(groupsAttila);
        usersAttila.setPassword("b6fb2e8cec7ca5915ba9d5fa31b60b3c");
        usersAttila.setGroups(groupsAttila);
        usersAttila.setUser(teacherAttila);
//        usersRepository.addUsers(usersAttila);
        teacherAttila.setUsers(usersAttila);
        teacherAttila.setPhoneNumber(123456);
        teacherRepository.addTeacher(teacherAttila);
        
        teacherBorisz.setName("Borisz");
        Users usersBorisz=new Users();
        usersBorisz.setEmail("borisz@borisz.com");

        GroupREALM groupsBorisz=new GroupREALM();
        groupsBorisz.setGroupId("teacher");
        groupsBorisz.setUsers(usersBorisz);
        //groupsRepository.addGroups(groupsBorisz);
        usersBorisz.setGroups(groupsBorisz);
        usersBorisz.setPassword("dbea186feb31395a25b1087d623edc4e");
        usersBorisz.setUser(teacherBorisz);
//        usersRepository.addUsers(usersBorisz);
        teacherBorisz.setUsers(usersBorisz);
        teacherBorisz.setPhoneNumber(123456);
        teacherRepository.addTeacher(teacherBorisz);
////Teams 
        team1.setGoogleCalendarURL("https://calendar.google.com/calendar/embed?src=po2j1j2alufm5ea8q5jkre1b50%40group.calendar.google.com&ctz=Europe%2FBudapest" );//style="border: 0" width="800" height="600" frameborder="0" scrolling="no"
        team1.setTeamName("bh09");
        teamRepository.addTeam(team1);
        team2.setGoogleCalendarURL("https://calendar.google.com/calendar/embed?src=0ubaudgna2pb7a9pcpqsbskksk%40group.calendar.google.com&ctz=Europe%2FBudapest");
        team2.setTeamName("bh10");
        teamRepository.addTeam(team2);
        team3.setGoogleCalendarURL("https://calendar.google.com/calendar/embed?src=hdg65vfjat7rk9i7ihp7plhhi8%40group.calendar.google.com&ctz=Europe%2FBudapest");
        team3.setTeamName("bh11");
        teamRepository.addTeam(team3);
//
//        //classDays
//
//
        //Questions
//        question.setFirstQuestion("testquestion#1");
//        question.setSecondQuestion("testquestion#2");
//        question.setThirdQuestion("testquestion#3");
//        question.setClassDay(classDay);
//        questionRepository.addQuestion(question);
        
        //Students
        
        LocalDate birthDate = LocalDate.of(1983, Month.MARCH, 22);
        studentIbiri.setBirtDate(Date.valueOf(birthDate));
        Users usersIbiri=new Users();
        usersIbiri.setEmail("ibiri@ibiri.com");

        GroupREALM groupsIbiri=new GroupREALM();
        groupsIbiri.setGroupId("student");
        groupsIbiri.setUsers(usersIbiri);
        //groupsRepository.addGroups(groupsIbiri);
        usersIbiri.setGroups(groupsIbiri);
        usersIbiri.setPassword("66802984e666697c0d053d8f51856ff3");
        usersIbiri.setUser(studentIbiri);
//        usersRepository.addUsers(usersIbiri);
        studentIbiri.setUsers(usersIbiri);
        studentIbiri.setName("Ibiri");
        studentIbiri.setPhoneNumber(123456);
        studentIbiri.setTeam(team1);
        studentRepository.addStudent(studentIbiri);

        birthDate = LocalDate.of(1984, Month.MARCH, 22);
        studentMityu.setBirtDate(Date.valueOf(birthDate));
        studentMityu.setName("Mityu");
        
        Users usersMityu=new Users();
        usersMityu.setEmail("mityu@mityu.com");

        GroupREALM groupsMityu=new GroupREALM();
        groupsMityu.setGroupId("student");
        groupsMityu.setUsers(usersMityu);
        //groupsRepository.addGroups(groupsMityu);
        usersMityu.setGroups(groupsMityu);
        usersMityu.setPassword("06f3bef8e051122615790830d38fc8c4");
//        usersRepository.addUsers(usersMityu);
        usersMityu.setUser(studentMityu);
        studentMityu.setUsers(usersMityu);
        
        studentMityu.setPhoneNumber(123456);
        studentMityu.setTeam(team1);
        studentRepository.addStudent(studentMityu);

        birthDate = LocalDate.of(1985, Month.MARCH, 22);
        studentAkarki.setBirtDate(Date.valueOf(birthDate));
        
        Users usersAkarki=new Users();
        usersAkarki.setEmail("akarki@akarki.com");

        GroupREALM groupsAkarki=new GroupREALM();
        groupsAkarki.setGroupId("student");
        groupsAkarki.setUsers(usersAkarki);
        //groupsRepository.addGroups(groupsAkarki);
        usersAkarki.setGroups(groupsAkarki);
        usersAkarki.setPassword("9e46f389aff6f6314b13d061c45adc5a");
//        usersRepository.addUsers(usersAkarki);
        usersAkarki.setUser(studentAkarki);
        studentAkarki.setUsers(usersAkarki);
        
        
        
        studentAkarki.setName("Akarki");
        studentAkarki.setPhoneNumber(123456);
        studentAkarki.setTeam(team2);
        studentRepository.addStudent(studentAkarki);

        birthDate = LocalDate.of(1986, Month.MARCH, 22);
        studentAkarkiCsoda.setBirtDate(Date.valueOf(birthDate));
        
        Users usersAkarkiCsoda=new Users();
        usersAkarkiCsoda.setEmail("akarkicsoda@akarkicsoda.com");

        GroupREALM groupsAkarkiCsoda=new GroupREALM();
        groupsAkarkiCsoda.setGroupId("student");
        groupsAkarkiCsoda.setUsers(usersAkarkiCsoda);
        //groupsRepository.addGroups(groupsAkarkiCsoda);
        usersAkarkiCsoda.setGroups(groupsAkarkiCsoda);
        usersAkarkiCsoda.setPassword("f00135e8c2f27ba8043b1844f772179e");
        usersAkarkiCsoda.setUser(studentAkarkiCsoda);
//        usersRepository.addUsers(usersAkarkiCsoda);
        studentAkarkiCsoda.setUsers(usersAkarkiCsoda);
        

        studentAkarkiCsoda.setName("AkarkiCsoda");
        studentAkarkiCsoda.setPhoneNumber(123456);
        studentAkarkiCsoda.setTeam(team2);
        studentRepository.addStudent(studentAkarkiCsoda);

        birthDate = LocalDate.of(1987, Month.MARCH, 22);
        studentNoname.setBirtDate(Date.valueOf(birthDate));
        
        Users usersNoname=new Users();
        usersNoname.setEmail("noname@noname.com");

        GroupREALM groupsNoname=new GroupREALM();
        groupsNoname.setGroupId("student");
        groupsNoname.setUsers(usersNoname);
        //groupsRepository.addGroups(groupsNoname);
        usersNoname.setGroups(groupsNoname);
        usersNoname.setPassword("2e93e6485a88921f00d0bd5170c07a42");
        usersNoname.setUser(studentNoname);
//        usersRepository.addUsers(usersNoname);
        studentNoname.setUsers(usersNoname);
        
        studentNoname.setName("Noname");
        studentNoname.setPhoneNumber(123456);
        studentNoname.setTeam(team3);
        studentRepository.addStudent(studentNoname);
//
//     
//        
        studentNewName.setBirtDate(Date.valueOf(birthDate));
        
        Users usersNewname=new Users();
        usersNewname.setEmail("newname@newname.com");

        GroupREALM groupsNewname=new GroupREALM();
        groupsNewname.setGroupId("student");
        groupsNewname.setUsers(usersNewname);
        //groupsRepository.addGroups(groupsNewname);
        usersNewname.setGroups(groupsNewname);
        usersNewname.setPassword("6a6b6484fae9e9403ad4320435b66286");
//        usersRepository.addUsers(usersNewname);
        usersNewname.setUser(studentNewName);
        studentNewName.setUsers(usersNewname);
        
        studentNewName.setName("Newname");
        studentNewName.setPhoneNumber(123456);
        studentNewName.setTeam(team3);
        studentRepository.addStudent(studentNewName);
//        
//        //Answers
//
//        answers.setAnswer1("testanswer1");
//        answers.setAnswer2("testAnswer2");
//        answers.setAnswer3("testAnswer3");
//        answers.setComment1("testComment1");
//        answers.setComment2("testComment2");
//        answers.setComment3("testComment3");
//        answers.setScore1(0);
//        answers.setScore2(0);
//        answers.setScore3(0);
//        answers.setQuestions(question);
//        answers.setStudent(student1);
//        answerRepository.addAnswer(answers);
//        
//        //  System.out.println(questionRepository.getQuestions());
        
        
        
        Homework h = new Homework();
        h.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h.setTeam(team1);
        h.setTitle("Bootstrap");
        h.setDescription("Használjatok bootstrap megoldásokat az órai feladat befejezéséhez");
        h.setMaxPoints(3);
        
        Homework h2 = new Homework();
        h2.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h2.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h2.setTeam(team1);
        h2.setTitle("Login");
        h2.setDescription("Mindenkinek működjön a loginvasárnapig");
        h2.setMaxPoints(2);
        
        Homework h3 = new Homework();
        h3.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h3.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h3.setTeam(team1);
        h3.setTitle("ToDo Hibernate");
        h3.setDescription("oldjátok meg az órai feladatot Hibernate segítségével");
        h3.setMaxPoints(5);
        
        Homework h4 = new Homework();
        h4.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h4.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h4.setTeam(team2);
        h4.setTitle("Szálkezelés");
        h4.setDescription("A bankos házit oldjátok meg szálkezeléssel");
        h4.setMaxPoints(5);
        
        Homework h5 = new Homework();
        h5.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h5.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h5.setTeam(team2);
        h5.setTitle("Bank értékpapír");
        h5.setDescription("Oldjátok meg a bankos háziban az értékpapír vásárlás opciót");
        h5.setMaxPoints(3);
        
        Homework h6 = new Homework();
        h6.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h6.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h6.setTeam(team3);
        h6.setTitle("Grafikon");
        h6.setDescription("A grafikon vízszintes kiírás helyett legyen függőleges");
        h6.setMaxPoints(2);
        
         Homework h7 = new Homework();
        h7.setDateReleased(Parser.parseLocalDateToSqlDate(LocalDate.now(ZoneId.systemDefault())));
        h7.setDeadline(Parser.parseLocalDateToSqlDate(LocalDate.of(2019, Month.JULY, 14)));
        h7.setTeam(team3);
        h7.setTitle("Programozási tételek");
        h7.setDescription("Oldjátok meg a feladott példákat");
        h7.setMaxPoints(4);
        homeworkRepository.addHomework(h7);
        homeworkRepository.addHomework(h6);
        homeworkRepository.addHomework(h5);
        homeworkRepository.addHomework(h4);
        homeworkRepository.addHomework(h3);
        homeworkRepository.addHomework(h2);
        homeworkRepository.addHomework(h);
        
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static java.sql.Date localTimeToDate(LocalDateTime lt) {
        return new java.sql.Date(lt.atZone(ZoneId.systemDefault()).toInstant()
                .toEpochMilli());
    }
}
