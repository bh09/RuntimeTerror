/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.AnswersDTO;
import hu.braininghub.BHManagementSystem.service.AnswersService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "StudentQuizResultServlet", urlPatterns = {"/StudentQuizResultServlet"})
public class StudentQuizResultServlet extends HttpServlet {
@EJB
AnswersService answersService;
@EJB
Links links;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession();
        session.setAttribute("servletName", "StudentQuizResultServlet");
        Long studentID=(Long)session.getAttribute("studentID");
        List<AnswersDTO> allAnswersForAstudent=answersService.getAllAnswersForAStudent(studentID);
        Integer limit=10;
        request.setAttribute("limit", limit);
        request.setAttribute("checkedList",answersService.checkedAnswers(allAnswersForAstudent, limit));
        request.getRequestDispatcher("/WEB-INF/studentPage/StudentsQuizResult.jsp").forward(request, response);   
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    HttpSession session=request.getSession();
        session.setAttribute("servletName", "StudentQuizResultServlet");    
        String inputButton=request.getParameter("inputLink");
    
    String redirectURL=links.getURLStudent(inputButton, session.getAttribute("servletName").toString()); 
        if(null==redirectURL){
    request.getRequestDispatcher("/WEB-INF/studentPage/StudentsQuizResult.jsp").forward(request, response);     
    }else{
    response.sendRedirect(redirectURL);   
    } 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
