/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.dto.homework.HomeworkDto;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.service.HomeworkResultService;
import hu.braininghub.BHManagementSystem.service.HomeworkService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "DisplayHomeworkServlet", urlPatterns = {"/DisplayHomeworkServlet"})
public class DisplayHomeworkServlet extends HttpServlet {

    @EJB
    HomeworkService homeworkService;

    @EJB
    TeamService teamService;
    
    @EJB
    HomeworkResultService homeworkResultService;
    
    @EJB
    Links links;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            HttpSession session = request.getSession();
        session.setAttribute("servletName", "DisplayHomeworkServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        //Kell a navbarhoz vége
        if (teamID==-1L) {
            request.setAttribute("fieldsVisible", false);
        }else{
        request.setAttribute("fieldsVisible", true);
        }
        /*String teamId;
        if (null == request.getParameter("teamId")) {
            teamId = "7";
        } else {
            teamId = request.getParameter("teamId");
        }*/

        //List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), teamId);

        //request.setAttribute("teamList", orderedTeamList);
        request.setAttribute("allHomweworks", homeworkService.getAllHomeworkDtosByTeamId(teamID));
        request.setAttribute("allHomeworkResults", homeworkResultService.getAllHomeWorkResultDtosOfATeam(teamID.toString()));
        request.getRequestDispatcher("/WEB-INF/DisplayHomeworks.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //ez kell hozzá, hogy működjön a navbar
         HttpSession session = request.getSession();
        session.setAttribute("servletName", "DisplayHomeworkServlet");
        Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);
        }else{
        teamID=(Long)session.getAttribute("teamID");
                }
        //ez kell hozzá, hogy működjön a navbar vége
        
        
        /*if ("Szerkeszt".equalsIgnoreCase(request.getParameter("edit"))) {
            System.out.println("az edit értéke, amit a user küldött: " + request.getParameter("edit"));
            System.out.println("a custId: " + request.getParameter("custId"));
            //request.setAttribute("homeworkId", request.getParameter("custId"));
            //RequestDispatcher rd = getServletContext().getRequestDispatcher("/UpdateHomeworkServlet");
            session.setAttribute("homeworkId", request.getParameter("custId"));
            RequestDispatcher rd = request.getRequestDispatcher("/UpdateHomeworkServlet");
            rd.forward(request, response);*/

        /*} else if (request.getParameter("create") != null) {
            System.out.println("create :-) ");
            response.sendRedirect(request.getContextPath() + "/HomeworkServlet");*/
        if (request.getParameter("delete") != null) {
            System.out.println("a homework id: " + request.getParameter("delete"));
            
            //getting teamId of the entity to be deleted, in order to be able to navigate to the display page of that team
            String teamIdString;
            HomeworkDto dto = homeworkService.getHomeworkDtoById(homeworkService.parseStringToLong(request.getParameter("delete")));
            teamIdString = String.valueOf(dto.getTeam().getId());
            
            homeworkService.deleteHomeworkDBRecord(request.getParameter("delete"));
            //request.getRequestDispatcher("/WEB-INF/DisplayHomeworks.jsp").forward(request, response);
            response.sendRedirect("DisplayHomeworkServlet");
        }else if(request.getParameter("edit") != null){
         session.setAttribute("homeworkId", request.getParameter("edit"));
         response.sendRedirect("UpdateHomeworkServlet");
        }else if(request.getParameter("evaluate") != null){
         session.setAttribute("homeworkId", request.getParameter("evaluate"));
         response.sendRedirect("AddHomeworkResultServlet");   
        }else {

            //String teamId = request.getParameter("teamDropdown");
            //System.out.println("DisplayhomeworkServlet post -  teamId: " + teamId);
            //List<TeamDTO> orderedTeamList = homeworkService.orderTeamDTOs(teamService.loadTeamDropDown(), teamId);

            //request.setAttribute("teamList", orderedTeamList);
            request.setAttribute("allHomweworks", homeworkService.getAllHomeworkDtosByTeamId(homeworkService.parseStringToLong(teamID.toString())));
            request.setAttribute("allHomeworkResults", homeworkResultService.getAllHomeWorkResultDtosOfATeam(teamID.toString()));
            

//Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/DisplayHomeworks.jsp").forward(request, response);  
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége
            

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
