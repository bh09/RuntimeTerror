/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.service.HomeworkService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "HomeworkServlet", urlPatterns = {"/HomeworkServlet"})
public class HomeworkServlet extends HttpServlet {

    @EJB
    TeamService teamService;

    @EJB
    HomeworkService homeworkService;
    
    @EJB
    StudentRepo studentRepo;
    @EJB
Links links;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
   
        //select menü feltöltése
        //request.setAttribute("teamList", teamService.loadTeamDropDown());
        request.setCharacterEncoding("Latin1");  
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "HomeworkServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        //Kell a navbarhoz vége
        if (teamID==-1L) {
            request.setAttribute("fieldsVisible", false);
        }else{
        request.setAttribute("fieldsVisible", true);
        }
        request.getRequestDispatcher("/WEB-INF/addHomework.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("Latin1");
        //ez kell hozzá, hogy működjön a navbar
         HttpSession session = request.getSession();
        session.setAttribute("servletName", "HomeworkServlet");
        Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);
        }else{
        teamID=(Long)session.getAttribute("teamID");
                }
        //ez kell hozzá, hogy működjön a navbar vége
        if (teamID==-1L) {
            request.setAttribute("fieldsVisible", false);
        }else{
            request.setAttribute("fieldsVisible", true);
        }
        String date=request.getParameter("date");
        System.out.println(date);
        String hwTitle=request.getParameter("hwtitle");
        String hwDesc=request.getParameter("hwdesc");
        String maxPoints=request.getParameter("maxPoints");
        
        
        if (null!=date&&null!=hwTitle&&null!=hwDesc&&null!=maxPoints) {
        homeworkService.createHomeworkRecord(
                homeworkService.stringToLocaldate(date),
                teamService.getTeamByIdService(teamID.toString()),
                hwTitle,
                hwDesc,
                homeworkService.parseStringToInt(maxPoints)
        );    
        }
       
//Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/addHomework.jsp").forward(request, response);   
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége
        
        //response.sendRedirect(request.getContextPath() + "/DisplayHomeworkServlet" + "?teamId=" + request.getParameter("teamDropdown"));

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
            //3 team hozzáadása
//        Team team0 = new Team();
//        team0.setTeamName("BH09");
//        team0.getGoogleCalendarURL();
//        teamRepository.addTeam(team0);
//        
//        Team team2 = new Team();
//        team2.setTeamName("BH10");
//        team2.getGoogleCalendarURL();
//        teamRepository.addTeam(team2);
//        
//        Team team3 = new Team();
//        team3.setTeamName("BH11");
//        team3.getGoogleCalendarURL();
//        teamRepository.addTeam(team3);
    
    

}
