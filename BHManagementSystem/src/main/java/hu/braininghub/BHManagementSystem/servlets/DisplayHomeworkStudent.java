/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.service.HomeworkResultService;
import hu.braininghub.BHManagementSystem.service.HomeworkService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "DisplayHomeworkStudent", urlPatterns = {"/DisplayHomeworkStudent"})
public class DisplayHomeworkStudent extends HttpServlet {
    
    @EJB
    HomeworkResultService homeworkResultService;
    
    @EJB
    Links links;
    
  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String studentID = String.valueOf(session.getAttribute("studentID"));
        Long teamID = (Long) session.getAttribute("teamID");

        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }

        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }
        
        //homeworks of student
        request.setAttribute("homeworkResults", homeworkResultService.getAllHomeWorkResultDtosOfAStudent(studentID));
        request.setAttribute("totalMaxPoints", homeworkResultService.getAllHomeworMaxPointsOfAStudent(studentID));
        request.setAttribute("totalResultPoints", homeworkResultService.getAllHomeworkResultPointsOfAStudent(studentID));
        request.setAttribute("totalPercentage", homeworkResultService.getHomeWorkResultPercentageOfAStudent(studentID));
        request.getRequestDispatcher("/WEB-INF/studentPage/DisplayHomeworkStudent.jsp").forward(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
