/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.exam.ExamResultDTO;
import hu.braininghub.BHManagementSystem.service.ExamResultService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "DisplayExamsStudent", urlPatterns = {"/DisplayExamsStudent"})
public class DisplayExamsStudent extends HttpServlet {

    @EJB
    ExamResultService examResultService;

    @EJB
    Links links;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String studentID = String.valueOf(session.getAttribute("studentID"));
        Long teamID = (Long) session.getAttribute("teamID");

        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }

        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }
        
        request.setAttribute("examResultListNormal",  examResultService.getNormalExamResultsOfAStudent(studentID));
        System.out.println("normal" + examResultService.getNormalExamResultsOfAStudent(studentID).size());
        request.setAttribute("examResultListFinal",  examResultService.getFinalExamResultsOfAStudent(studentID));
        System.out.println("final: " + examResultService.getFinalExamResultsOfAStudent(studentID).size());
        request.getRequestDispatcher("/WEB-INF/studentPage/DisplayExamsStudent.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
