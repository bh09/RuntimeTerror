/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.AnswersDTO;
import hu.braininghub.BHManagementSystem.service.AnswersService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "TeacherAnswerServlet", urlPatterns = {"/TeacherAnswerServlet"})
public class TeacherAnswerServlet extends HttpServlet {
@EJB
AnswersService answerService;
@EJB
Links links;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "TeacherAnswerServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        request.setAttribute("teamID", teamID);
        
        List<AnswersDTO> allAnswersForATeam=answerService.getAllAnswersForATeam(teamID);
        
        Map<AnswersDTO,List<List<String>>> mappedAnswers=mappedAnswers(uncheckedAnswers(allAnswersForATeam));
        
        request.setAttribute("uncheckedList",mappedAnswers);
        
        request.setAttribute("limit", 10);
        Map<AnswersDTO,List<List<String>>> mappedAnswersChecked=mappedAnswers(answerService.checkedAnswers(allAnswersForATeam,10));
        request.setAttribute("checkedList", mappedAnswersChecked);

        request.getRequestDispatcher("/WEB-INF/AnswersTeacher.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    HttpSession session = request.getSession();
        session.setAttribute("servletName", "TeacherAnswerServlet");
        Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);    
        }else{
        teamID=(Long)session.getAttribute("teamID");
              }
    
    String answerIDst=request.getParameter("create");
        System.out.println(answerIDst);
    if(answerIDst!=null&&!"".equals(answerIDst)){
        
        Long answerID=Long.parseLong(answerIDst);
        
        String comment1=request.getParameter("firstComment");
        String comment2=request.getParameter("secondComment");
        String comment3=request.getParameter("thirdComment");
        
        String score1st=request.getParameter("resultPoints1_"+answerID);
        System.out.println(score1st);
        String score2st=request.getParameter("resultPoints2_"+answerID);
        String score3st=request.getParameter("resultPoints3_"+answerID);
        
        double score1=parser(score1st);
        System.out.println(score1);
        double score2=parser(score2st);
        double score3=parser(score3st);
        
        answerService.updateAnswers(score1, score2, score3, comment1, comment2, comment3,answerID);
        request.setAttribute("create", "");
    }
    if(teamID==-1L){
        
    }else{
        List<AnswersDTO> allAnswersForATeam=answerService.getAllAnswersForATeam(teamID);
        
        Map<AnswersDTO,List<List<String>>> mappedAnswers=mappedAnswers(uncheckedAnswers(allAnswersForATeam));
        
        request.setAttribute("uncheckedList",mappedAnswers);
        String limitTableString=request.getParameter("limitTable");
        Integer limit;
        if (limitTableString!=null) {
            limit=Integer.parseInt(limitTableString);
            request.setAttribute("limit", limit);
        }else{
            limit=10;
        }
        Map<AnswersDTO,List<List<String>>> mappedAnswersChecked=mappedAnswers(answerService.checkedAnswers(allAnswersForATeam,limit));
        request.setAttribute("checkedList", mappedAnswersChecked);
    }
    //Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/AnswersTeacher.jsp").forward(request, response); 
    }else{
     response.sendRedirect(redirectURL);   
    }    
       

    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

public List<AnswersDTO> uncheckedAnswers(List<AnswersDTO>allAnswers){
    return allAnswers.stream()
            .filter(answer->answerService.equal(answer.getScore1())||answerService.equal(answer.getScore2())||answerService.equal(answer.getScore3()))
            .sorted(Comparator.comparing((AnswersDTO ans)->ans.getQuestions().getFireQuestion()))
                    .collect(Collectors.toList());
}

public List<String> dropDown(double currentValue){
List<String> options=new ArrayList<>();
if(answerService.equal(currentValue)){    
return notChosen(options);
}else{
return chosen(options,currentValue);    
}    
}
public List<String> notChosen(List<String> options){
    
    options.add("n/a");
    options.add("0.0");
    options.add("0.2");
    options.add("0.4");
    options.add("0.6");
    options.add("0.8");
    options.add("1.0");
return options;    
}
public List<String> chosen(List<String> options,double inputValue){
   double value=0.0;
   double firstValueInDropDown=-1.0;
  
   DecimalFormat df = new DecimalFormat("#.#", new DecimalFormatSymbols(new Locale("en")));
    for (int i = 1; i <=6 ; i++) {
        if(Math.abs(inputValue-value)<0.01){
        firstValueInDropDown=value;
        }
        value=value+0.2;
    }
    String formattedFirst=df.format(firstValueInDropDown);
    options.add(formattedFirst);
    value=0.0;    
    for(int i = 1; i <=6 ; i++){
        if (Math.abs(firstValueInDropDown-value)>0.01) {
    String formatted=df.format(value);       
    options.add(formatted);
        }
    value=value+0.2;
    } 
    return options;
    }
public List<List<String>> createDropdowns(double firstResult, double secondResult,double thirdResult){
List<List<String>> dropdowns=new ArrayList<>();
dropdowns.add(dropDown(firstResult));
dropdowns.add(dropDown(secondResult));
dropdowns.add(dropDown(thirdResult));
return dropdowns;
}
public Map<AnswersDTO,List<List<String>>> mappedAnswers(List<AnswersDTO> answers){
Map<AnswersDTO,List<List<String>>> answersMap=new LinkedHashMap<>();
    for (AnswersDTO answer : answers) {
    answersMap.put(answer, createDropdowns(answer.getScore1(), answer.getScore2(), answer.getScore3()));
    }
return answersMap;    
}
public double parser(String input){
    if("n/a".equals(input)){
        return-1.0;
    }
    else{
        return Double.parseDouble(input);
    }
}
}

