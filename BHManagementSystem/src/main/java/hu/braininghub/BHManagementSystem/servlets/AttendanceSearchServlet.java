package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.service.AttendanceService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bence58
 */
@WebServlet(name = "AttendanceSearchServlet", urlPatterns = {"/AttendanceSearch"})
public class AttendanceSearchServlet extends HttpServlet {

    @EJB
    AttendanceService as;
    @EJB
    Links links;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AttendanceSearch");
        request.getRequestDispatcher("WEB-INF/attendancesearch.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession();
        session.setAttribute("servletName", "AttendanceSearch");
        if(null!=request.getParameter("attendanceDay")){
        request.setAttribute("listOfStudents", as.getStudentsByDate(request.getParameter("attendanceDay")));
        }
        //Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("WEB-INF/attendancesearch.jsp").forward(request, response);
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége
       
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
