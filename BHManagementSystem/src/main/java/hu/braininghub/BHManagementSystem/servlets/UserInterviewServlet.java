/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.service.InterviewService;
import hu.braininghub.BHManagementSystem.service.Links;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "UserInterviewServlet", urlPatterns = {"/UserInterview"})
public class UserInterviewServlet extends HttpServlet {

    private static final long USERIDE = 7L;
    @EJB
    InterviewService is;
    @EJB
    Links links;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     HttpSession session = request.getSession();
        session.setAttribute("servletName", "UserInterview");
        Long studentID=(Long)session.getAttribute("studentID");
        request.setAttribute("studentinterview", studentDto(studentID));

        request.setAttribute("interviews", is.getInterviewListByStudentId(studentID));

        request.getRequestDispatcher("WEB-INF/studentPage/studentinterview.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "UserInterview");
    //Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLStudent(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("WEB-INF/studentPage/studentinterview.jsp").forward(request, response);   
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége
       
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public StudentDTO studentDto(Long id) {
        return is.getStudent(id);

    }

}
