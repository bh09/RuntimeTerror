/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;



import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.QuestionBean;
import hu.braininghub.BHManagementSystem.service.TeamBean;
import hu.braininghub.BHManagementSystem.service.TeamInterface;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "QuizQuestionServlet", urlPatterns = {"/QuizQuestionServlet"})
public class QuizQuestionServlet extends HttpServlet {
Long teamID;    
@EJB
QuestionBean questionBean;
@EJB(name="ejb/teams", description="stateless")
TeamInterface teamBean;
@EJB
Links links;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "QuizQuestionServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        //Kell a navbarhoz vége
        if (teamID==-1L) {
            request.setAttribute("fieldsVisible", false);
        }else{
        request.setAttribute("fieldsVisible", true);
        }
    request.getRequestDispatcher("/WEB-INF/QuizQuestions.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //ez kell hozzá, hogy működjön a navbar
         HttpSession session = request.getSession();
        session.setAttribute("servletName", "QuizQuestionServlet");
        Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);    
        }else{
        teamID=(Long)session.getAttribute("teamID");
                }
        //ez kell hozzá, hogy működjön a navbar vége
        
        if (teamID==-1L) {
            request.setAttribute("fieldsVisible", false);
        }else{
        request.setAttribute("fieldsVisible", true);
        }
        
    String question1=request.getParameter("question1");
    String question2=request.getParameter("question2");
    String question3=request.getParameter("question3");
      
if(question1==null){
  question1="";
  question2="";
  question3="";
}else{
        if(question1.isEmpty()){
        request.setAttribute("writeQuestion1", true);    
        }else if(question2.isEmpty()){
        request.setAttribute("writeQuestion2", true);      
        }else if(question3.isEmpty()){
        request.setAttribute("writeQuestion3", true);    
        }else{
        Long previousQuestionID=questionBean.getQuestionsForTeamLastIndex(teamID);
            System.out.println(previousQuestionID+"pqid");
        questionBean.createQuestion(question1, question2, question3,teamBean.getTeamByID(teamID));
       
            if (questionBean.isAddedToDatabase(teamID, previousQuestionID)) {
            request.setAttribute("dbSuccess",true);
            }
        }
}
//Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/QuizQuestions.jsp").forward(request, response);   
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
