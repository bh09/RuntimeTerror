/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.service.AnswersService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.QuestionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.OptionalLong;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "StudentAnswerServlet", urlPatterns = {"/StudentAnswerServlet"})
public class StudentAnswerServlet extends HttpServlet {
@EJB
QuestionBean questions;
@EJB
AnswersService answersService;
@EJB
Links links;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession();
        session.setAttribute("servletName", "StudentAnswerServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        Long lastQuestionID=questions.getQuestionsForTeamLastIndex(teamID);
        
        
        if (lastQuestionID!=0L){
        QuestionsDTO question=questions.getQuestionForQuestionID(lastQuestionID);
     
        if(null!=question.getFireQuestion()
                &&question.getFireQuestion().toLocalDateTime().isAfter(LocalDateTime.now())
                &&!answersService.isAnswerExistsForQuestionAndStudent(lastQuestionID, (Long)session.getAttribute("studentID"))){    
        request.setAttribute("fieldsVisible", true);
        
        request.setAttribute("firstQuestion", question.getFirstQuestion());
        request.setAttribute("secondQuestion", question.getSecondQuestion());
        request.setAttribute("thirdQuestion", question.getThirdQuestion());
        }else{
        request.setAttribute("fieldsVisible", false);    
        }
        }else{
        request.setAttribute("fieldsVisible", false);    
        }
request.getRequestDispatcher("/WEB-INF/studentPage/AnswersStudent.jsp").forward(request, response);   
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession();
        session.setAttribute("servletName", "StudentAnswerServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        
        Long lastQuestionID=questions.getQuestionsForTeamLastIndex(teamID);
        if(!answersService.isAnswerExistsForQuestionAndStudent(lastQuestionID, (Long)session.getAttribute("studentID"))){
        request.setAttribute("fieldsVisible", true);    
        }else{
        request.setAttribute("fieldsVisible", false);    
        }
        if (null!=request.getParameter("create")&&""!=request.getParameter("create")) {
            System.out.println("A create-ben van");
            QuestionsDTO question=questions.getQuestionForQuestionID(lastQuestionID);
        if(question.getFireQuestion().toLocalDateTime().isAfter(LocalDateTime.now())){
        String answer1=request.getParameter("firstQuestion");
        String answer2=request.getParameter("secondQuestion");
        String answer3=request.getParameter("thirdQuestion");
        answersService.createAnswerForStudent(answer1, answer2, answer3, lastQuestionID, (Long)session.getAttribute("studentID"));
        request.setAttribute("create","");
        }
        }
        
    String inputButton=request.getParameter("inputLink");

    String redirectURL=links.getURLStudent(inputButton, session.getAttribute("servletName").toString());
    if(null==redirectURL||redirectURL.equals("Quit")){
    response.sendRedirect("StudentServlet");     
    
    //request.getRequestDispatcher("/WEB-INF/studentPage/AnswersStudent.jsp").forward(request, response);    
    }else{
    response.sendRedirect(redirectURL);   
    } 
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
