package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.service.ExamService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.TeamService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "AddExamServlet", urlPatterns = {"/AddExamServlet"})
public class AddExamServlet extends HttpServlet {

    @EJB
    ExamService examService;

    @EJB
    TeamService teamService;
    
    @EJB
    Links links;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AddExamServlet");
        
        Long teamID = (Long) session.getAttribute("teamID");

        if (teamID == -1L) {
            request.setAttribute("fieldsVisible", false);
        } else {
            request.setAttribute("fieldsVisible", true);
        }
        
        if (null == teamID) {
            teamID = teamService.getAllTeams().stream().findFirst().get().getId();
        }

        request.getRequestDispatcher("/WEB-INF/AddExam.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "AddExamServlet");
        Long teamID = null;

        if (request.getParameter("teamDropdown") != null) {
            teamID = Long.parseLong(request.getParameter("teamDropdown"));
            session.setAttribute("teamID", teamID);
        } else {
            teamID = (Long) session.getAttribute("teamID");
        }
        
        
        if (teamID==-1L) {
            request.setAttribute("fieldsVisible", false);
        }else{
            request.setAttribute("fieldsVisible", true);
        }
               
        String date = request.getParameter("date");
        String examType = request.getParameter("examType");
        String submitButton = request.getParameter("create");

        if("Létrehoz".equalsIgnoreCase(submitButton)) {
            examService.saveNewExamEntity(date, String.valueOf(teamID), examType);
            request.setAttribute("teamID", teamID);
            session.setAttribute("teamID", teamID);
            //response.sendRedirect(request.getContextPath() + "/DisplayExamsServlet" + "?teamID=" + teamID);
        }
        
        String inputLink = request.getParameter("inputLink");

        String redirectURL = links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
        if (null == redirectURL) {
            request.getRequestDispatcher("/WEB-INF/AddExam.jsp").forward(request, response);
        } else {
            response.sendRedirect(redirectURL);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
