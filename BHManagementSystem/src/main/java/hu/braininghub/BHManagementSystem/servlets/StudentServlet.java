/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.service.AttendanceService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.QService;
import hu.braininghub.BHManagementSystem.service.StudentBean;
import hu.braininghub.BHManagementSystem.service.StudentInterface;
import hu.braininghub.BHManagementSystem.service.TeamInterface;
import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "StudentServlet", urlPatterns = {"/StudentServlet"})
public class StudentServlet extends HttpServlet {
@EJB(name="ejb/students", description="stateless")
StudentInterface students;
@EJB
Links links;
@EJB
AttendanceService attendanceService;
@EJB(name="ejb/teams", description="stateless")
TeamInterface teams;
@EJB
QService qservice;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*String studentIDstring=request.getParameter("studentID");
        /*Long studentID=Long.parseLong(studentIDstring);
        request.setAttribute("username", students.getStudentByStudentID(studentID).getName());*/
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "StudentServlet");
        Long studentID=(Long)session.getAttribute("studentID");
        Long teamID=(Long)session.getAttribute("teamID");
        request.setAttribute("calendar",teams.getTeamByID(teamID).getGoogleCalendarURL());
        request.setAttribute("missedClass",attendanceService.calculateMissingPercentage(attendanceService.getMissedClassById(studentID)));
        request.setAttribute("displayQ", (int)(qservice.calculateQForStudent(studentID)*100));
        System.out.println((int)(qservice.calculateQForStudent(studentID)*100));
        request.getRequestDispatcher("/WEB-INF/studentPage/Student.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "StudentServlet");
        Long teamID=(Long)session.getAttribute("teamID");
        Long studentID=(Long)session.getAttribute("studentID");
        request.setAttribute("calendar",teams.getTeamByID(teamID).getGoogleCalendarURL());
        request.setAttribute("displayQ", (int)(qservice.calculateQForStudent(studentID)*100));
        String inputButton=request.getParameter("inputLink");
        String redirectURL=links.getURLStudent(inputButton, session.getAttribute("servletName").toString());
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/studentPage/Student.jsp").forward(request, response);   
    }else{
     response.sendRedirect(redirectURL);   
    } 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
