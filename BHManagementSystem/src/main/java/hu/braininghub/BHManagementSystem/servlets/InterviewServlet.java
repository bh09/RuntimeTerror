/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import hu.braininghub.BHManagementSystem.entities.Interview;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.service.InterviewService;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.TeamService;
import hu.braininghub.BHManagementSystem.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Bence58
 */
@WebServlet(name = "Interview", urlPatterns = {"/Interview"})
public class InterviewServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(InterviewServlet.class);

    @EJB
    InterviewService ir;

    @EJB
    TeamRepository teamRepository;
    @EJB
    Links links;
    private List<Team> teams = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //initDropDownGroups();
        //addDodownGroupsToList();
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "Interview");
        Long teamID=(Long)session.getAttribute("teamID");
        //request.setAttribute("teamList", teams);
        if (teamID==-1L) {
        request.setAttribute("teamNotChosen", true);
        }else{
        request.setAttribute("teamNotChosen", false);
        request.setAttribute("studentList", getStudentsByGrupId(teamID));
        }
        
        request.getRequestDispatcher("WEB-INF/interview.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //long helper = setGrupToTheFirstInTheDropdown(request.getParameter("teamDropdown"));
         HttpSession session = request.getSession();
        session.setAttribute("servletName", "Interview");
       Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);    
        }else{
        teamID=(Long)session.getAttribute("teamID");
                }        
//Kell a navbarhoz vége
        //request.setAttribute("teamList", teams);
        
        if (teamID == -1L) {
            log.info("POST: Disabled lett minden mező.");
            request.setAttribute("teamNotChosen", true);
        } else {
            log.info("POST: Enabled lett minden mező.");
            request.setAttribute("teamNotChosen", false);
        }

        request.setAttribute("studentList", getStudentsByGrupId(teamID));
        log.info("student ID: " + request.getParameter("studentDropdown"));

        String hrComment = request.getParameter("hrcomment");
        String javaComment = request.getParameter("javacomment");

        if (StringUtil.noneEmpty(request.getParameter("javapoints"), request.getParameter("hrpoints"))) {

            int javaPoints = Integer.parseInt(request.getParameter("javapoints"));
            int hrPoints = Integer.parseInt(request.getParameter("hrpoints"));
            addingInterviewResultToTheService(javaComment, hrComment, hrPoints, javaPoints, request.getParameter("studentDropdown"));
            ir.updateStudentInterviewCulomn(Integer.parseInt(request.getParameter("studentDropdown")), Integer.parseInt(request.getParameter("interviewSelect")));
        }
        //Kell a navbarhoz
    String inputLink=request.getParameter("inputLink");

    
    String redirectURL=links.getURLTeacher(inputLink, session.getAttribute("servletName").toString());
    if(null==redirectURL){
    request.getRequestDispatcher("WEB-INF/interview.jsp").forward(request, response);
    }else{
     response.sendRedirect(redirectURL);   
    }
    //kell a navbarhoz vége
//response.sendRedirect(request.getContextPath()+"/Interview");
       
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

   // ez a metodus ceareli a listat ami groupokat fogja taralmazni illetve feltolti egy dummy goupal aminek az idje -1 és ez alapján lehet desable minden cucc.
    public void initDropDownGroups() {
        log.info("initDropDownGroups: begins");
        teams.clear();
        Team team = new Team();
        team.setTeamName("Válassz csoportot");
        team.setId(-1L);
        teams.add(team);
        log.info("initDropDownGroups: end");

    }
    // ez a fugvény adja a dropdown listahoz a létező grupokat

    public void addDodownGroupsToList() {
        log.info("addDodownGroupsToList: begins");
        List<Team> teams2 = teamRepository.getTeam();
        for (Team team1 : teams2) {
            teams.add(team1);
        }
        log.info("addDodownGroupsToList: list {}", teams);

    }
    // ez a fugveny küldi tovább a servicéhez az interju értékeléseket

    public void addingInterviewResultToTheService(String javaComment, String hrComment, int hrPoints, int javaPoints, String studentId) {

        if (StringUtil.noneEmpty(hrComment, javaComment, studentId)) {

            Interview iv = new Interview();
            iv.setHrComments(hrComment);
            iv.setHrPoints(hrPoints);
            iv.setJavaComments(javaComment);
            iv.setJavaPoints(javaPoints);

            log.info("addingInterviewResultToTheService/ interview objektum: {}", iv);
            ir.addStudentIdToTheInterview(Integer.parseInt(studentId), iv);

            //ir.addInterview(iv);
        }

    }

    public long setGrupToTheFirstInTheDropdown(String grupId) {

        Long teamID = Long.parseLong(grupId);
        log.info("setGrupToTheFirstInTheDropdown: teamid: {}", teamID);
        Team team = new Team();
        team = teams.stream()
                .filter(t -> t.getId().equals(teamID))
                .findFirst()
                .get();
        teams.remove(team);
        teams.add(0, team);
        return teamID;
    }

    public List<StudentDTO> getStudentsByGrupId(Long teamID) {


        return ir.getStudents(teamID);

    }

}
