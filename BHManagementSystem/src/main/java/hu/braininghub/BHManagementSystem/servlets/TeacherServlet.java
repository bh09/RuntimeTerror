/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;



import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.repositories.TeamRepository;
import hu.braininghub.BHManagementSystem.service.Links;
import hu.braininghub.BHManagementSystem.service.QuestionBean;
import hu.braininghub.BHManagementSystem.service.TeamBean;
import hu.braininghub.BHManagementSystem.service.TeamInterface;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "TeacherServlet", urlPatterns = {"/TeacherServlet"})
public class TeacherServlet extends HttpServlet {
@EJB(name="ejb/teams", description="stateless")
TeamInterface teams;
@EJB
QuestionBean questionBean;
@EJB
Links links;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        

        //List<TeamDTO>teamDTOs=addChooseTeam(teams.getAllTeams());
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "TeacherServlet");
//        request.setAttribute("teamList", teamDTOs);
        request.setAttribute("home",null); 
        request.getRequestDispatcher("/WEB-INF/Teacher.jsp?teacherID="+request.getParameter("teacherID")).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String teamIDstring=request.getParameter("teamDropdown");
        HttpSession session = request.getSession();
        session.setAttribute("servletName", "TeacherServlet");
        Long teamID=null;
                if (request.getParameter("teamDropdown")!=null) {
        teamID=Long.parseLong(request.getParameter("teamDropdown"));
        session.setAttribute("teamID", teamID);
        if(teamID!=-1L){    
        request.setAttribute("calendar",teams.getTeamByID(teamID).getGoogleCalendarURL());
        }
        }else{
        teamID=(Long)session.getAttribute("teamID");
                }
        
        request.setAttribute("teacherid",request.getParameter("teacherid"));
        String teamIDstring=request.getParameter( "teamDropdown" );
        
    String inputButton=request.getParameter("inputLink");
        System.out.println(inputButton); 
    String redirectURL=links.getURLTeacher(inputButton, session.getAttribute("servletName").toString());
        System.out.println(redirectURL);
    if(null==redirectURL){
     request.getRequestDispatcher("/WEB-INF/Teacher.jsp").forward(request, response);   
    }else{
     response.sendRedirect(redirectURL);   
    }     
       
       }
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
