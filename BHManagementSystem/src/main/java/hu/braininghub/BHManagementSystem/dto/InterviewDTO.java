package hu.braininghub.BHManagementSystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class InterviewDTO {

    private long id;
    private String hrComment;
    private double hrPoints;
    private String javaComment;
    private double javaPoints;

}
