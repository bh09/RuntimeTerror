/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.dto;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rajnaig
 */
//Beadja az összes gettert settert, equals hashcode tostring
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {

    private Date birtDate;
    private List<AnswersDTO> answerList;
    private TeamDTO team;
    private String name;
    private Long id;
    private int phoneNumber;
    private boolean firstInterview;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.birtDate);
        hash = 19 * hash + Objects.hashCode(this.answerList);
        hash = 19 * hash + Objects.hashCode(this.team);
        hash = 19 * hash + Objects.hashCode(this.name);
        hash = 19 * hash + Objects.hashCode(this.id);
        hash = 19 * hash + this.phoneNumber;
        hash = 19 * hash + (this.firstInterview ? 1 : 0);
        hash = 19 * hash + (this.secondInterview ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StudentDTO other = (StudentDTO) obj;
        if (this.phoneNumber != other.phoneNumber) {
            return false;
        }
        if (this.firstInterview != other.firstInterview) {
            return false;
        }
        if (this.secondInterview != other.secondInterview) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.birtDate, other.birtDate)) {
            return false;
        }
        if (!Objects.equals(this.answerList, other.answerList)) {
            return false;
        }
        if (!Objects.equals(this.team, other.team)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    private boolean secondInterview;

    public Date getBirtDate() {
        return birtDate;
    }

    public void setBirtDate(Date birtDate) {
        this.birtDate = birtDate;
    }

    public List<AnswersDTO> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<AnswersDTO> answerList) {
        this.answerList = answerList;
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(TeamDTO team) {
        this.team = team;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isFirstInterview() {
        return firstInterview;
    }

    public void setFirstInterview(boolean firstInterview) {
        this.firstInterview = firstInterview;
    }

    public boolean isSecondInterview() {
        return secondInterview;
    }

    public void setSecondInterview(boolean secondInterview) {
        this.secondInterview = secondInterview;
    }
    

}
