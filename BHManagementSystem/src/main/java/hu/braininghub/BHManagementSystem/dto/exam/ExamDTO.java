
package hu.braininghub.BHManagementSystem.dto.exam;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.exam.ExamType;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Greg Takacs
 */
public class ExamDTO {
    
    private Long id;
    private LocalDate examDate;
    private ExamType examType;
    private TeamDTO teamDTO;
    private Double maxPopintsTheory;
    private Double maxPointsJava;
    private Double maxPointsTeamwork;
    private List<ExamResultDTO> examResultDTOs;

    public ExamDTO() {
    }

    public ExamDTO(Long id, LocalDate examDate, ExamType examType, TeamDTO teamDTO, Double maxPopintsTheory, Double maxPointsJava, Double maxPointsTeamwork, List<ExamResultDTO> examResultDTOs) {
        this.id = id;
        this.examDate = examDate;
        this.examType = examType;
        this.teamDTO = teamDTO;
        this.maxPopintsTheory = maxPopintsTheory;
        this.maxPointsJava = maxPointsJava;
        this.maxPointsTeamwork = maxPointsTeamwork;
        this.examResultDTOs = examResultDTOs;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getExamDate() {
        return examDate;
    }

    public void setExamDate(LocalDate examDate) {
        this.examDate = examDate;
    }

    public ExamType getExamType() {
        return examType;
    }

    public void setExamType(ExamType examType) {
        this.examType = examType;
    }

    public Double getMaxPopintsTheory() {
        return maxPopintsTheory;
    }

    public void setMaxPopintsTheory(Double maxPopintsTheory) {
        this.maxPopintsTheory = maxPopintsTheory;
    }

    public Double getMaxPointsJava() {
        return maxPointsJava;
    }

    public void setMaxPointsJava(Double maxPointsJava) {
        this.maxPointsJava = maxPointsJava;
    }

    public Double getMaxPointsTeamwork() {
        return maxPointsTeamwork;
    }

    public void setMaxPointsTeamwork(Double maxPointsTeamwork) {
        this.maxPointsTeamwork = maxPointsTeamwork;
    }

    public TeamDTO getTeamDTO() {
        return teamDTO;
    }

    public void setTeamDTO(TeamDTO teamDTO) {
        this.teamDTO = teamDTO;
    }

    public List<ExamResultDTO> getExamResultDTOs() {
        return examResultDTOs;
    }
    
    public boolean isOneExamResultPresent () {
        if (this.getExamResultDTOs().isEmpty()) {
            return true;
        } else {
            return false;
        }
    
    }
    

    public void setExamResultDTOs(List<ExamResultDTO> examResultDTOs) {
        this.examResultDTOs = examResultDTOs;
    }

}
