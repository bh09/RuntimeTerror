/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.dto;

import java.sql.Timestamp;
import java.time.LocalDate;

/**
 *
 * @author rajnaig
 */
public class ClassDayDTO {
  private Long id;
  private Timestamp beginClass;
  private Timestamp endClass;
  private LocalDate formattedDate;  

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getBeginClass() {
        return beginClass;
    }

    public void setBeginClass(Timestamp beginClass) {
        this.beginClass = beginClass;
    }

    public Timestamp getEndClass() {
        return endClass;
    }

    public void setEndClass(Timestamp endClass) {
        this.endClass = endClass;
    }

    public LocalDate getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(LocalDate formattedDate) {
        this.formattedDate = formattedDate;
    }
}
