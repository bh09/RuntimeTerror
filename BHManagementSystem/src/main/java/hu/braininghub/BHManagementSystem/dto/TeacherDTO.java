/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.dto;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rajnaig
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherDTO {
    private String name;
    private String password;
    private String email;
    private int phoneNumber; 
    private Date birtDate;
}
