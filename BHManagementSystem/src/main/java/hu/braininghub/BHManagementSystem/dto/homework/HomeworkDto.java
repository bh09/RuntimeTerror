package hu.braininghub.BHManagementSystem.dto.homework;

import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Greg Takacs
 */
public class HomeworkDto {

    private Long id;
    private String title;
    private String description;
    private int maxPoints;
    private LocalDate dateReleased;
    private LocalDate deadline;
    private TeamDTO team;

    public HomeworkDto() {
    }
    
    

    public HomeworkDto(Long id, String title, String description, int maxPoints, LocalDate dateReleased, LocalDate deadline, TeamDTO team) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.maxPoints = maxPoints;
        this.dateReleased = dateReleased;
        this.deadline = deadline;
        this.team = team;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    public LocalDate getDateReleased() {
        return dateReleased;
    }

    public void setDateReleased(LocalDate dateReleased) {
        this.dateReleased = dateReleased;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeamDTO(TeamDTO team) {
        this.team = team;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        hash = 67 * hash + Objects.hashCode(this.title);
        hash = 67 * hash + Objects.hashCode(this.description);
        hash = 67 * hash + this.maxPoints;
        hash = 67 * hash + Objects.hashCode(this.dateReleased);
        hash = 67 * hash + Objects.hashCode(this.deadline);
        hash = 67 * hash + Objects.hashCode(this.team);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HomeworkDto other = (HomeworkDto) obj;
        if (this.maxPoints != other.maxPoints) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateReleased, other.dateReleased)) {
            return false;
        }
        if (!Objects.equals(this.deadline, other.deadline)) {
            return false;
        }
        if (!Objects.equals(this.team, other.team)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HomeworkDto{" + "id=" + id + ", title=" + title + ", description=" + description + ", maxPoints=" + maxPoints + ", dateReleased=" + dateReleased + ", deadline=" + deadline + ", team=" + team + '}';
    }


    
    

}
