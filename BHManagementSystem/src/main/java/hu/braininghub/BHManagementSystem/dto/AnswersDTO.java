/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.dto;

import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rajnaig
 */
@Data//Beadja az összes gettert settert, equals hashcode tostring
@NoArgsConstructor
@AllArgsConstructor
public class AnswersDTO {
 private Long id;
    private String answer1;
    private String comment1;
    private double score1;
    private String answer2;
    private String comment2;
    private double score2;
    private String answer3;
    private String comment3;
    private double score3;
    private QuestionsDTO questions;
    private StudentDTO student;   
}
