
package hu.braininghub.BHManagementSystem.dto.exam;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.exam.Exam;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import java.util.Objects;
import javax.persistence.ManyToOne;

/**
 *
 * @author Greg Takacs
 */
public class ExamResultDTO {
    
    private Long id;
    
    private StudentDTO studentDTO;
    private ExamDTO examDTO;
    private Double resultPointsTheory;
    private Double resultPointsJava;
    private Double resultPointsTeamwork;

    public ExamResultDTO() {
    }

    public ExamResultDTO(Long id, StudentDTO studentDTO, ExamDTO examDTO, Double resultPointsTheory, Double resultPointsJava, Double resultPointsTeamwork) {
        this.id = id;
        this.studentDTO = studentDTO;
        this.examDTO = examDTO;
        this.resultPointsTheory = resultPointsTheory;
        this.resultPointsJava = resultPointsJava;
        this.resultPointsTeamwork = resultPointsTeamwork;
    }

    
    
    

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ExamDTO getExamDTO() {
        return examDTO;
    }

    public void setExamDTO(ExamDTO examDTO) {
        this.examDTO = examDTO;
    }
    

    public Double getResultPointsTheory() {
        return resultPointsTheory;
    }

    public void setResultPointsTheory(Double resultPointsTheory) {
        this.resultPointsTheory = resultPointsTheory;
    }

    public Double getResultPointsJava() {
        return resultPointsJava;
    }

    public void setResultPointsJava(Double resultPointsJava) {
        this.resultPointsJava = resultPointsJava;
    }

    public Double getResultPointsTeamwork() {
        return resultPointsTeamwork;
    }

    public void setResultPointsTeamwork(Double resultPointsTeamwork) {
        this.resultPointsTeamwork = resultPointsTeamwork;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExamResultDTO other = (ExamResultDTO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ExamResultDTO{" + "id=" + id + ", studentDTO=" + studentDTO + ", examDTO=" + examDTO + ", resultPointsTheory=" + resultPointsTheory + ", resultPointsJava=" + resultPointsJava + ", resultPointsTeamwork=" + resultPointsTeamwork + '}';
    }

    
    
    

}

