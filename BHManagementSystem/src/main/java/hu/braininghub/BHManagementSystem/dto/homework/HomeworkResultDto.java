
package hu.braininghub.BHManagementSystem.dto.homework;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import java.util.Objects;

/**
 *
 * @author Greg Takacs
 */
public class HomeworkResultDto {
    
    private Long id;
    private StudentDTO student;
    private HomeworkDto homework;
    private double resultPoints;

    public HomeworkResultDto(StudentDTO student, HomeworkDto homework, double resultPoints) {
        this.student = student;
        this.homework = homework;
        this.resultPoints = resultPoints;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudentDTO getStudentDTO() {
        return student;
    }

    public void setStudentDTO(StudentDTO student) {
        this.student = student;
    }

    public HomeworkDto getHomework() {
        return homework;
    }

    public void setHomeworkDto(HomeworkDto homework) {
        this.homework = homework;
    }

    public double getResultPoints() {
        return resultPoints;
    }

    public void setResultPoints(double resultPoints) {
        this.resultPoints = resultPoints;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.student);
        hash = 73 * hash + Objects.hashCode(this.homework);
        hash = 73 * hash + (int) (Double.doubleToLongBits(this.resultPoints) ^ (Double.doubleToLongBits(this.resultPoints) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HomeworkResultDto other = (HomeworkResultDto) obj;
        if (Double.doubleToLongBits(this.resultPoints) != Double.doubleToLongBits(other.resultPoints)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.student, other.student)) {
            return false;
        }
        if (!Objects.equals(this.homework, other.homework)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HomeworkResultDto{" + "id=" + id + ", student=" + student + ", homework=" + homework + ", resultPoints=" + resultPoints + '}';
    }

    

}
