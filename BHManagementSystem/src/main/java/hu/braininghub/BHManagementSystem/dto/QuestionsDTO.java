/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.dto;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.quiz.Answers;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rajnaig
 */
@Data//Beadja az összes gettert settert, equals hashcode tostring
@NoArgsConstructor
@AllArgsConstructor
public class QuestionsDTO {
private Long id;
    private String firstQuestion;
    private String secondQuestion;
    private String thirdQuestion;
    private Timestamp fireQuestion;
    private TeamDTO team;    

    public void setTeamDTO(TeamDTO team) {
    this.team=team;
    }
    public TeamDTO getTeamDTO() {
    return team;
    }
}
