/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 *
 * @author rajnaig
 */
@ExtendWith(MockitoExtension.class)
public class TeacherRepositoryTest {
    @Mock
    private TeacherRepository teacherRepository;    

@Test
    public void getTeamByQuestionIDShouldThrowExceptionWhenInputNull(){
    when(teacherRepository.getTeacherByTeacherID(null)).thenCallRealMethod();
    assertAll(
    ()->{assertThrows(Exception.class,() -> {
                    teacherRepository.getTeacherByTeacherID(null);
                    });}        
    
    );    
    }
    
}
