/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.repositories.quiz;

import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.naming.NameNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author rajnaig
 */
@ExtendWith(MockitoExtension.class)
public class QuestionRepositoryTest {

    private QuestionRepository questionRepo;
    private List<Questions> questionList;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    QuestionRepository questionRepository;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    EntityManager em;
    @BeforeEach
    public void setup(){
         questionRepo=new QuestionRepository(em);
    }
    
  
    public void setupQuestions() {
        questionList=new ArrayList<>();
        Questions q1 = new Questions();
        Questions q2 = new Questions();
        Questions q3 = new Questions();
        Team t1=new Team();
        Team t2=new Team();
        Team t3=new Team();
        t1.setId(1L);
        t2.setId(1L);
        t3.setId(1L);
        
        q1.setId(1L);
        q1.setTeam(t1);
        q1.setFirstQuestion("first");
        q1.setSecondQuestion("second");
        q1.setThirdQuestion("third");

        questionList.add(q1);
        q1.setId(2L);
        q2.setFirstQuestion("fourth");
        q2.setSecondQuestion("fifth");
        q2.setThirdQuestion("sitxth");
        q1.setTeam(t2);
        
        questionList.add(q2);
         
        q3.setId(3L);
        q3.setFirstQuestion("seventh");
        q3.setSecondQuestion("eight");
        q3.setThirdQuestion("nine");

        q3.setTeam(t3);
        questionList.add(q3);

    }
    public void setupRealQuestionRepository(){
        
                questionRepository=new QuestionRepository();
    }
    @Test
    public void getQuestionsForTeamIfReturnData() {
        setupQuestions();
        assertAll("QuestionRepositoryTest",
                () -> {
                    when(questionRepository.getQuestionsForTeam(1L)).thenReturn(questionList);
                    assertAll("QuestionRepositoryTest",
                    ()->{
                    assertEquals(3,questionRepository.getQuestionsForTeam(1L).size());  
                    },
                    ()->{
                        assertTrue(questionRepository.getQuestionsForTeam(1L).get(0).getClass().equals(Questions.class));
                    },
                    ()->{
                    
                    
                    }       
                    );
                }
        );
            
    }
    @Test
    public void getQuestionsForTeamIfThrowsErrorForNullInput(){
    when(questionRepository.getQuestionsForTeam(null)).thenCallRealMethod();
    assertAll("QuestionRepositoryForTeamNullInput",
    ()->{assertThrows(Exception.class,() -> {
                    questionRepository.getQuestionsForTeam(null);
                    });}        
    
    );    
    }
    
    @Test
    public void searchShouldReturnSomethingWhenInputIsNumber(){  
    //spy(questionRepo.searchQuestionsForTeam(1L, "1"));  
    assertAll("SearchNotThrowException",
    ()->{assertEquals(null,questionRepo.searchQuestionsForTeam(1L,"1"));
                    }   
    );    
    }
    
    @Test
    public void searchShouldReturnSomethingWhenInputIsText(){
    //spy(questionRepo.searchQuestionsForTeam(1L, "valami"));  
    assertAll("SearchNotThrowException",
    ()->{assertEquals(null,questionRepo.searchQuestionsForTeam(1L,"valami"));
                    }   
    );
        
    }
    @Test
    public void searchShouldThrowsErrorForNullInput(){
    when(questionRepository.searchQuestionsForTeam(null,"valami")).thenCallRealMethod();
    assertAll("QuestionRepositoryForSearchNullInput",
    ()->{assertThrows(Exception.class,() -> {
                    questionRepository.searchQuestionsForTeam(null,"valami");
                    });}        
    
    );    
    }
    /*
    @Test
    public void lastIndexShouldReturnNullIfLastIndexIsNotPresentInDatabase(){
        spy(questionRepo.getQuestionsForTeamLastIndex(1L));  
    assertAll("LastIndexNotThrowExceptionIfNothingReturnedFromDB",
    ()->{assertEquals(null,questionRepo.getQuestionsForTeamLastIndex(1L));
                    }   
    );    
    }
    */
    @Test
    public void lastIndexShouldThrowsErrorForNullInput(){
    when(questionRepository.getQuestionsForTeamLastIndex(null)).thenCallRealMethod();
    assertAll("QuestionRepositoryForLastIndexNullInput",
    ()->{assertThrows(Exception.class,() -> {
                    questionRepository.getQuestionsForTeamLastIndex(null);
                    });}        
    
    );    
    }
    @Test
    public void isQuestionAlreadyFiredShouldThrowsErrorForNullInput(){
    when(questionRepository.isQuestionAlreadyFired(null)).thenCallRealMethod();
    assertAll("QuestionRepositoryForareadyFiredNullInput",
    ()->{assertThrows(Exception.class,() -> {
                    questionRepository.isQuestionAlreadyFired(null);
                    });}        
    
    );    
    }
    
    @Test
    public void lastIndexShouldReturnNullIfNotPresentInDB(){
        assertAll(
        ()->{
        when(questionRepo.getQuestionsForTeamLastIndex(1L)).thenThrow(NullPointerException.class);
        assertEquals(null,questionRepo.getQuestionsForTeamLastIndex(1L));
        }
        
        );
    }
    @Test
    public void getTeamByQuestionIDShouldThrowExceptionWhenInputNull(){
    when(questionRepository.getTeamByQuestionID(null)).thenCallRealMethod();
    assertAll("QuestionRepositoryForareadyFiredNullInput",
    ()->{assertThrows(Exception.class,() -> {
                    questionRepository.getTeamByQuestionID(null);
                    });}        
    
    );    
    }
    /*
            public Team getTeamByQuestionID(Long questionID){
    Query q=em.createQuery("From Questions, where id=:questionID");
    q.setParameter("questionID", questionID);
    Questions question=(Questions)q.getSingleResult();
    return question.getTeam();
    }
    
    public Long getQuestionsForTeamLastIndex(Long teamID){
        Query q = em.createNativeQuery("Select max(id) from questions Where team_id=:teamID");
        q.setParameter("teamID", teamID);

    try{   
     return ((BigInteger)q.getSingleResult()).longValue();
    }catch(NullPointerException ex){
        
    }
    return null;
    }
    public void overwriteQuestion(Long questionID,String firstQuestion,String secondQuestion,String thirdQuestion){
    Query q = em.createQuery("UPDATE Questions set firstQuestion=:firstQuestion, secondQuestion=:secondQuestion, thirdQuestion=:thirdQuestion Where id=:questionID");
            q.setParameter("firstQuestion", firstQuestion);
            q.setParameter("secondQuestion", secondQuestion);
            q.setParameter("thirdQuestion", thirdQuestion);
            q.setParameter("questionID", questionID);
            q.executeUpdate();
    
    }
    public void deleteQuestionByQuestionID(Long questionID){
        Query q = em.createQuery("DELETE FROM Questions Where id=:questionID");
            q.setParameter("questionID", questionID);
            q.executeUpdate();
    }
    public boolean isQuestionAlreadyFired(Long questionID){ 
     Query q = em.createNativeQuery("Select fireQuestion from questions Where id=:questionID");
        q.setParameter("questionID", questionID);
        try{
         q.getSingleResult();
         return true;
        }catch(NullPointerException ex){
         return false;   
        }
    }
    public void fireQuestion(Long questionID,Timestamp fireQuestion){
      Query q = em.createQuery("UPDATE Questions set fireQuestion=:fireQuestion Where id=:questionID");
            q.setParameter("fireQuestion", fireQuestion);
            q.setParameter("questionID", questionID);
            q.executeUpdate();  
    }
    public void createQuestion(String firstQuestion,String secondQuestion,String thirdQuestion, Team team){
        Questions question=new Questions();
        question.setFirstQuestion(firstQuestion);
        question.setSecondQuestion(secondQuestion);
        question.setThirdQuestion(thirdQuestion);
        question.setTeam(team);
        addQuestion(question);
    }
    @Transactional
    public void addQuestion(Questions question) {
        System.out.println(question);
        em.persist(question);
    }    
    */

    /*@Test
    public void getQuestionsForTeamShouldThrowException(){
        setupRealQuestionRepository();
        System.out.println(questionRepository);
        System.out.println(questionRepository.getQuestionsForTeam(1L));
        assertThrows(NameNotFoundException.class, () -> {
                    questionRepository.getQuestionsForTeam(null);
                    });
    }*/

}
/*public List<Questions> getQuestionsForTeam(Long teamID){
        Query q = em.createQuery("From Questions Where team_id=:teamID", Questions.class);
        q.setParameter("teamID", teamID);
        List<Questions> questions=q.getResultList();
        return questions;
    }*/
/*

public List<Todo> getFilteredTodos(String name, String description, String summary) {
        return em.createNamedQuery("Todo.findByAttribute", Todo.class)
                .setParameter("name", setLikeParam(name))
                .setParameter("description", setLikeParam(description))
                .setParameter("summary", setLikeParam(summary))
                .getResultList();
    }

import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import model.Todo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TestTodoRepository {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    TodoRepository todoRepository;

    List<Todo> filteredTodoList;

    Todo testTodo;

    @BeforeEach
    public void setupFilteredTodoList() {
        filteredTodoList = new ArrayList<>();
        Todo todo1 = new Todo();
        todo1.setDescription("Descr");
        todo1.setSummary("Summ");
        Todo todo2 = new Todo();
        todo2.setDescription("Descr2");
        todo2.setSummary("Summ2");
        filteredTodoList.add(todo1);
        filteredTodoList.add(todo2);
    }

    @BeforeEach
    public void setupTestTodo() {
        testTodo = new Todo();
        testTodo.setDescription("KivaloLeiras");
        testTodo.setSummary("Osszegzes");
    }

    @Test
    public void testTodoRepo() {
        assertAll("TestTodoRepository",
                () -> {
                    when(todoRepository.getFilteredTodos(null, "Descr", "Summ")).thenReturn(filteredTodoList);

                    assertAll("TestReturnedEntities",
                            () -> {
                                assertEquals(2, todoRepository.getFilteredTodos(null, "Descr", "Summ").size());
                            },
                            () -> {
                                assertEquals(1, todoRepository.getFilteredTodos(null, "Descr", "Summ").stream()
                                        .filter(todo -> "Descr".equals(todo.getDescription()) && "Summ".equals(todo.getSummary()))
                                        .count());
                            });
                },
                () -> {
                    when(todoRepository.getEm().find(Todo.class, 5L)).thenReturn(testTodo);

                    assertTrue("KivaloLeiras"
                            .equals(todoRepository.getEm().find(Todo.class, 5L).getDescription()));
                },
                () -> {
                    when(todoRepository.getEm().find(Todo.class, -1L)).thenThrow(PersistenceException.class);

                    assertThrows(PersistenceException.class,
                            () -> {
                                todoRepository.getEm().find(Todo.class, -1L);
                            }
                    );
                }
        );
    }

}
*/
