/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.EventDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.events.Event;
import hu.braininghub.BHManagementSystem.entities.events.ImportanceType;
import hu.braininghub.BHManagementSystem.repositories.eventrepository.EventRepository;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;


import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author rajnaig
 */

@ExtendWith(MockitoExtension.class)
public class EventBeanTest {
    
private EventBean eventBean;
private List<Event> eventList;

@Mock(answer=Answers.RETURNS_DEEP_STUBS)
EventRepository eventRepository;

@BeforeEach
public void setup(){
    eventBean=new EventBean(eventRepository);
}
public void setupEventList(){
    eventList=new ArrayList<>();
    for (int i = 0; i < 10; i++) {
    Event e=new Event();    
    e.setId((long)i);
    e.setExpiry(Timestamp.valueOf(LocalDateTime.now().plusDays(1)));
    e.setMessage(""+i);
    Team team=new Team();
    team.setId((long) i);
    team.setTeamName(""+i);
    team.setGoogleCalendarURL("googleCalendarURL");
    e.setImportance(ImportanceType.URGENT.toString());
    e.setTeam(team);
    e.setTeam(new Team());
    eventList.add(e);
    }
    for (int i = 0; i < 10; i++) {
    Event e=new Event();    
    e.setId((long)i);
    e.setExpiry(Timestamp.valueOf(LocalDateTime.now().minusDays(1)));
    e.setMessage(""+i);
    Team team=new Team();
    team.setId((long) i);
    team.setTeamName(""+i);
    team.setGoogleCalendarURL("googleCalendarURL");
    e.setImportance(ImportanceType.URGENT.toString());
    e.setTeam(team);
    e.setTeam(new Team());
    eventList.add(e);
    }
}

    public void getEventsByTeamIDReturnsEventDTO() {
        setupEventList();
        when(eventRepository.getEventsForTeamID(1L)).thenReturn(eventList);
        assertAll(
        
        ()->{
        Assertions.assertEquals(10,eventBean.getEventsForTeamID(1L).size());
        },
        ()->{
        assertTrue(eventBean.getEventsForTeamID(1L).get(0).getClass().equals(EventDTO.class));
        }        
        );
        eventList.clear();
        when(eventRepository.getEventsForTeamID(1L)).thenReturn(eventList);
        Assertions.assertEquals(0,eventBean.getEventsForTeamID(1L).size());
    }
    @Test
    public void privateEventsAfterExpiryShouldReturnEventsAfterExpiry(){
    setupEventList();
    List<Event> events=eventBean.privateEventsAfterExpiry(eventList);
    assertAll(
    ()->{
    Assertions.assertEquals(10,events.size());    
    },
    ()->{
        for (Event event : events) {
        Assertions.assertTrue(event.getExpiry().after(Timestamp.valueOf(LocalDateTime.now())));    
        }
    
      
    }
    );            
    }
    
    @Test
    public void TestIfEventStringReturnsCorrect(){
    setupEventList();
    when(eventRepository.getEventsForTeamID(1L)).thenReturn(eventList);
    assertEquals(30,(eventBean.getEventsForTeamIDAndConvertToString(1L).split(" ")).length);
    
    }
    
}
/*
public String getEventsForTeamIDAndConvertToString(Long teamID){
    List<Event> eventList=eventRepository.getEventsForTeamID(teamID);
    eventList=privateEventsAfterExpiry(eventList);
    StringBuffer eventString=new StringBuffer();
        for (Event event : eventList) {
        eventString.append(event.getMessage());
        eventString.append(" ");
        eventString.append(event.getExpiry().toLocalDateTime().getYear());
        eventString.append("-");
        eventString.append(event.getExpiry().toLocalDateTime().getMonth());
        eventString.append("-");
        eventString.append(event.getExpiry().toLocalDateTime().getDayOfMonth());
        eventString.append("-");
        eventString.append(event.getExpiry().toLocalDateTime().getHour());
        eventString.append("-");
        eventString.append(event.getExpiry().toLocalDateTime().getMinute());
        eventString.append("-");
        eventString.append(" ");
        }
    return eventString.toString();
    }
}
*/
