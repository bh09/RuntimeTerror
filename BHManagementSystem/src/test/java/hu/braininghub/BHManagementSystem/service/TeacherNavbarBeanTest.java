/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;


import hu.braininghub.BHManagementSystem.dto.TeamDTO;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertAll;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author rajnaig
 */
@ExtendWith(MockitoExtension.class)
public class TeacherNavbarBeanTest {
private TeacherNavbarBean teacherNavbarBean;
List<TeamDTO> dtoList;    
@Mock    
TeamBean teams;
@BeforeEach
public void setup(){
    teacherNavbarBean=new TeacherNavbarBean(teams);
}
public void setupTeamDTOs(){
    dtoList=new ArrayList<>();
    TeamDTO t1=new TeamDTO();
    TeamDTO t2=new TeamDTO();
    TeamDTO t3=new TeamDTO();
    
    t1.setGoogleCalendarURL("valami");
    t1.setId(1L);
    t1.setTeamName("első");
    dtoList.add(t1);
    
    t2.setGoogleCalendarURL("valami");
    t2.setId(2L);
    t2.setTeamName("második");
    dtoList.add(t2);
    
    t3.setGoogleCalendarURL("valami");
    t3.setId(3L);
    t3.setTeamName("harmadik");
    dtoList.add(t3);
}
@Test
public void shouldAddNewTeam(){
setupTeamDTOs();
    when(teams.getAllTeams()).thenReturn(dtoList);
assertAll(
()->{Assertions.assertEquals(4,teacherNavbarBean.addChooseTeam().size());},
        
()->{Assertions.assertEquals("Válassz csoportot", teacherNavbarBean.addChooseTeam().get(0).getTeamName());}        
);
}
@Test
public void shouldChooseTeam(){
setupTeamDTOs();
Assertions.assertEquals("első", teacherNavbarBean.getSelectedTeam(1L, dtoList).get(0).getTeamName());
Assertions.assertEquals("második", teacherNavbarBean.getSelectedTeam(2L, dtoList).get(0).getTeamName());
Assertions.assertEquals("harmadik", teacherNavbarBean.getSelectedTeam(3L, dtoList).get(0).getTeamName());
}
}
/*
public List<TeamDTO> getSelectedTeam(Long teamID,List<TeamDTO> teamDTOs){ 
        TeamDTO team=teamDTOs.stream()
                .filter(t -> t.getId().equals(teamID))
            .findFirst()
            .get();
        teamDTOs.remove(team);
        teamDTOs=teamDTOs.stream()
                .sorted(Comparator.comparing(t->t.getId()))
                .collect(Collectors.toList());
        teamDTOs.add(0,team);
        return teamDTOs;
}
public List<TeamDTO> addChooseTeam(){
    List<TeamDTO>team=teams.getAllTeams();
    TeamDTO teamDTO=new TeamDTO();
            teamDTO.setTeamName("Válassz csoportot");
            teamDTO.setId(-1L);
            team.add(0, teamDTO);
    return team;
}
*/