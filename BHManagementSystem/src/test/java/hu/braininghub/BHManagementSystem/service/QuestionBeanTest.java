/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.quiz.Questions;
import hu.braininghub.BHManagementSystem.repositories.quiz.QuestionRepository;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import junit.framework.TestCase;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author rajnaig
 */
@ExtendWith(MockitoExtension.class)
public class QuestionBeanTest {
    private QuestionBean questionBean;
    private List<Questions> questionList;   

private QuestionRepository questionRepository=mock(QuestionRepository.class);

        @BeforeEach
public void setup(){
    questionBean=new QuestionBean(questionRepository);
}
public void setupQuestions() {
        questionList=new ArrayList<>();
        Questions q1 = new Questions();
        Questions q2 = new Questions();
        Questions q3 = new Questions();
        q1.setFirstQuestion("first");
        q1.setSecondQuestion("second");
        q1.setThirdQuestion("third");
        q1.setTeam(new Team());

        questionList.add(q1);
        q2.setFirstQuestion("fourth");
        q2.setSecondQuestion("fifth");
        q2.setThirdQuestion("sitxth");
        q2.setTeam(new Team());
        questionList.add(q2);

        q2.setFirstQuestion("seventh");
        q2.setSecondQuestion("eight");
        q2.setThirdQuestion("nine");
        q3.setTeam(new Team());
        questionList.add(q3);

    }

        @Test
    public void getQuestionsForTeamIfReturnData() {
        setupQuestions();
        assertAll("QuestionbeanTest",
                () -> {
                    when(questionRepository.getQuestionsForTeam(1L)).thenReturn(questionList);
                    assertAll("QuestionbeanTest",
                    ()->{
                    assertEquals(3,questionBean.getAllQuestionsForTeams(1L).size());  
                    },
                    ()->{
                        assertTrue(questionBean.getAllQuestionsForTeams(1L).get(0).getClass().equals(QuestionsDTO.class));
                    },
                    ()->{
                        assertFalse(questionBean.getAllQuestionsForTeams(1L).get(1).getClass().equals(Questions.class));
                    
                    }
                    );
                    questionList.clear();
                    when(questionRepository.getQuestionsForTeam(0L)).thenReturn(questionList);
                    assertAll("QuestionbeanTest",
                    ()->{
                    assertEquals(0,questionBean.getAllQuestionsForTeams(0L).size());
                    }
                    );
                }
                
        );
            
        
        
    }
    @Test
    public void lastIndexShouldReturnWhenExistsInDatabase(){
        when(questionRepository.getQuestionsForTeamLastIndex(1L)).thenReturn(1L);
        assertAll("QuestionbeanTestlastIndex",
        ()->{
            assertEquals(1L, questionBean.getQuestionsForTeamLastIndex(1L));
        }        
        
        );
        when(questionRepository.getQuestionsForTeamLastIndex(0L)).thenReturn(null);
        assertAll(
        ()->{assertEquals(0L,questionBean.getQuestionsForTeamLastIndex(0L));}
        );
    }
    @Test
    public void isAddedToDatabaseShouldReturnTrueWhenDatabaseOperationSucceed(){
    when(questionRepository.getQuestionsForTeamLastIndex(1L)).thenReturn(1L);
        assertAll("QuestionBeanAddedToDatabase",
        ()->{assertTrue(questionBean.isAddedToDatabase(0L, 1L));},
        ()->{assertFalse(questionBean.isAddedToDatabase(1L, 1L));}        
    );    
    }
    @Test
public void searchResultsShouldReturnWhenPassedData(){
    setupQuestions();
    when(questionRepository.searchQuestionsForTeam(1L, "valami")).thenReturn(questionList);
    assertAll("QuestionBeanSearchTest",
    ()->{assertEquals(3, questionBean.searchQuestionsForTeam(1L, "valami").size());},
    ()->{assertTrue(questionBean.searchQuestionsForTeam(1L, "valami").get(1).getClass().equals(QuestionsDTO.class));}    
    );
    questionList.clear();
    when(questionRepository.searchQuestionsForTeam(0L,"semmi")).thenReturn(questionList);
    assertAll("QuestionBeanSearchTest",
    ()->{assertEquals(0,questionBean.searchQuestionsForTeam(0L,"semmi").size());}
    );

}
    @Test
    public void isQuestionFiredShouldReturnBoolean(){
    when(questionRepository.isQuestionAlreadyFired(1L)).thenReturn(Boolean.TRUE);
    assertAll("QuestionBeanisFired",
    ()->{assertTrue(questionRepository.isQuestionAlreadyFired(1L));}
    );
    when(questionRepository.isQuestionAlreadyFired(1L)).thenReturn(Boolean.FALSE);
    assertAll("QuestionBeanisFired",
    ()->{assertFalse(questionRepository.isQuestionAlreadyFired(0L));}
    );
}
}  


/*
public boolean isQuestionFired(Long questionID){
     return questionRepository.isQuestionAlreadyFired(questionID);
 }
}*/