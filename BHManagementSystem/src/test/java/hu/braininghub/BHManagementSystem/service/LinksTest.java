/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;



/**
 *
 * @author rajnaig
 */
@ExtendWith(MockitoExtension.class)
public class LinksTest {
private Links links;    
    @BeforeEach
    public void setup(){
    links=new Links();    
    }

    @Test
    public void testTeacherLinksifInputButtonPressedOnTheSamePage() {
        Assertions.assertAll(
        ()->{
            Assertions.assertEquals(null,links.getURLTeacher(null, "something"));
        },
        ()->{
            Assertions.assertEquals(null,links.getURLTeacher("home", "TeacherServlet"));
        },
        ()->{
            Assertions.assertEquals(null,links.getURLTeacher("quiz", "QuizQuestionServlet"));
        },
        ()->{
           Assertions.assertEquals(null,links.getURLTeacher("quizedit", "TaskListServlet"));
        },
        ()->{
           Assertions.assertEquals(null,links.getURLTeacher("interview", "Interview"));
        },
        ()->{
           Assertions.assertEquals(null,links.getURLTeacher("homework", "HomeworkServlet"));
        },
        ()->{
           Assertions.assertEquals(null,links.getURLTeacher("homeworkedit", "DisplayHomeworkServlet"));
        },
         ()->{
           Assertions.assertEquals(null,links.getURLTeacher("attendance", "AttendanceServlet"));
        },
         ()->{
           Assertions.assertEquals(null,links.getURLTeacher("attendancesearch","AttendanceSearch" ));
        },
          ()->{
           Assertions.assertEquals(null,links.getURLTeacher("registration","RegisterStudent" ));
        }
        );
    }
    @Test
    public void testTeacherLinksifInputButtonPressedOnDifferentPage(){
    Assertions.assertAll(
    ()->{
           Assertions.assertEquals("TeacherServlet",links.getURLTeacher("home", "something"));
        },
    ()->{
           Assertions.assertEquals("QuizQuestionServlet",links.getURLTeacher("quiz", "something"));
        },
    ()->{
           Assertions.assertEquals("TaskListServlet",links.getURLTeacher("quizedit", "something"));
        },
    ()->{
           Assertions.assertEquals("Interview",links.getURLTeacher("interview", "something"));
        },
    ()->{
           Assertions.assertEquals("Quit",links.getURLTeacher("quit", "something"));
        },
    ()->{
           Assertions.assertEquals("HomeworkServlet",links.getURLTeacher("homework","something" ));
        },
    ()->{
           Assertions.assertEquals("DisplayHomeworkServlet",links.getURLTeacher("homeworkedit","something" ));
        },
    ()->{
           Assertions.assertEquals("AttendanceServlet",links.getURLTeacher("attendance","something" ));
        },
    ()->{
           Assertions.assertEquals("AttendanceSearch",links.getURLTeacher("attendancesearch","something" ));
        },        
      ()->{
           Assertions.assertEquals("RegisterStudent",links.getURLTeacher("registration","something" ));
        }
    );    
    }
    @Test
    public void testStudentLinksifInputButtonPressedOnTheSamePage(){
     Assertions.assertAll(
        ()->{
            Assertions.assertEquals(null,links.getURLStudent(null, "something"));
        },
        ()->{
            Assertions.assertEquals(null,links.getURLStudent("home", "StudentServlet"));
        },
         ()->{
            Assertions.assertEquals(null,links.getURLStudent("interview", "UserInterview"));
        },
         ()->{
            Assertions.assertEquals(null,links.getURLStudent("quiz", "StudentAnswerServlet"));
        }

             );
    }
    @Test
    public void testStudentLinksifInputButtonPressedOnDifferentPage(){
     Assertions.assertAll(
        ()->{
            Assertions.assertEquals("StudentServlet",links.getURLStudent("home", "something"));
        },
        ()->{
            Assertions.assertEquals("Quit",links.getURLStudent("quit", "something"));
        },
        ()->{
            Assertions.assertEquals("UserInterview",links.getURLStudent("interview", "something"));
        },
             ()->{
            Assertions.assertEquals("StudentAnswerServlet",links.getURLStudent("quiz", "something"));
        }
             
             );
    }
}
/*
public String getURLStudent(String inputButton,String servletName){

        
if("home".equals(inputButton)&&!"StudentServlet".equals(servletName)){
    return  "StudentServlet";

    }else if("homework".equals(inputButton)){
        return "";
    }else if("quiz".equals(inputButton)){
    return "";
    }else if("homeworkpoints".equals(inputButton)){
    return "";
    }else if("quizpoints".equals(inputButton)){
    return "";
    }else if("zhpoints".equals(inputButton)){
    return "";        
    }else if("interview".equals(inputButton)){
    return "";    
    }else if("profil".equals(inputButton)){
    return "";    
    }else if("quit".equals(inputButton)){
    return "Quit";    
    }else{
    return null;    
    }     
}
*/
