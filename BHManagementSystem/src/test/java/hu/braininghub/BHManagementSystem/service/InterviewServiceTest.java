/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.StudentDTO;
import hu.braininghub.BHManagementSystem.entities.Team;
import hu.braininghub.BHManagementSystem.entities.user.Student;
import hu.braininghub.BHManagementSystem.repositories.InterviewRepo;
import hu.braininghub.BHManagementSystem.repositories.StudentRepo;
import hu.braininghub.BHManagementSystem.util.Mappers;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import junit.framework.TestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Bence58
 */
@ExtendWith(MockitoExtension.class)
public class InterviewServiceTest extends TestCase {

    private List<Student> studentList;

    private InterviewService is;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    StudentRepo sr;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    InterviewRepo ir;

    @BeforeEach
    public void setUp() {

        is = new InterviewService(sr, ir);

    }

    public void setUpStudent() {

        studentList = new ArrayList<>();
        Team team = new Team();
        

        Student s1 = new Student();
        Student s2 = new Student();
        Student s3 = new Student();
        Student s4 = new Student();
       
        s1.setFirstInterview(1);
        s1.setSecondInterview(10);
        s1.setTeam(team);
        
        s2.setFirstInterview(2);
        s2.setSecondInterview(11);
        s2.setTeam(team);
        
        s3.setFirstInterview(3);
        s3.setSecondInterview(12);
        s3.setTeam(team);
        
        s4.setFirstInterview(4);
        s4.setSecondInterview(13);
        s4.setTeam(team);
        
        
//        Student s1 = new Student(null, 1, 10, null, null, new Team(), null);
//        Student s2 = new Student(null, 2, 11, null, null, new Team(), null);
//        Student s3 = new Student(null, 3, 12, null, null, new Team(), null);
//        Student s4 = new Student(null, 4, 13, null, null, new Team(), null);
        s1.setFirstInterview(1);
        s1.setSecondInterview(10);
        s1.setTeam(team);
        s2.setFirstInterview(2);
        s2.setSecondInterview(11);
        s2.setTeam(team);
        s3.setFirstInterview(3);
        s3.setSecondInterview(12);
        s3.setTeam(team);
        s4.setFirstInterview(4);
        s4.setSecondInterview(13);
        s4.setTeam(team);
        studentList.add(s1);
        studentList.add(s2);
        studentList.add(s3);
        studentList.add(s4);
    }

    @Test
    public void getStudentsTest() {
        setUpStudent();
        Assertions.assertAll("Visszaadjae", () -> {
            Mockito.when(sr.getStudentByTeamId(8L)).thenReturn(studentList);
            assertEquals("messzidzs", StudentDTO.class, is.getStudents(8L).get(0).getClass());

        }
        );

    }

    public List<StudentDTO> getStudents(Long teamId) {
        return sr.getStudentByTeamId(teamId).stream().map(p -> Mappers.mapStudentToStudentDTO((Student) p)).collect(Collectors.toList());

    }

}
