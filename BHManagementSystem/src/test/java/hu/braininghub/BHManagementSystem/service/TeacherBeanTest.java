/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.service;

import hu.braininghub.BHManagementSystem.dto.TeacherDTO;
import hu.braininghub.BHManagementSystem.entities.user.Teacher;
import hu.braininghub.BHManagementSystem.repositories.TeacherRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;




/**
 *
 * @author rajnaig
 */
@ExtendWith(MockitoExtension.class)
public class TeacherBeanTest {
    private TeacherBean teacherBean;
    private Teacher dummyTeacher; 
    @Mock
    TeacherRepository teacherRepository;
    
    @BeforeEach
    public void setup(){
        teacherBean=new TeacherBean(teacherRepository);
    }
    public void setupTeacher(){
        dummyTeacher=new Teacher();
        dummyTeacher.setId(1L);
        dummyTeacher.setName("dummy");
        dummyTeacher.setPhoneNumber(123456);
    }
    @Test
  public void shouldMapToTeacherDTO(){
      setupTeacher();
      when(teacherRepository.getTeacherByTeacherID(1L)).thenReturn(dummyTeacher);
        Assertions.assertEquals(teacherBean.getTeacherByTeacherID(1L).getClass(),TeacherDTO.class);
  }    
}
