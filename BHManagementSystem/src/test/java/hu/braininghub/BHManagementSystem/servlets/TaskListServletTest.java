/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.BHManagementSystem.servlets;

import hu.braininghub.BHManagementSystem.dto.QuestionsDTO;
import hu.braininghub.BHManagementSystem.service.QuestionBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;
import junit.framework.TestCase;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author rajnaig
 */
//@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.Silent.class)
public class TaskListServletTest {

    private TaskListServlet taskListServlet;
    private List<QuestionsDTO> questionList;
    @InjectMocks
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    HttpServletRequest request;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private QuestionBean questions;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Cookie cookie;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Enumeration<String> enumeration;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Principal principal;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    HttpSession session;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Collection<Part> httpCollection;

    @BeforeEach
    private void setup() {
        request = new HttpServletRequest() {
            @Override
            public String getAuthType() {
                return "";
            }

            @Override
            public Cookie[] getCookies() {
                return new Cookie[3];
            }

            @Override
            public long getDateHeader(String string) {
                return 0L;
            }

            @Override
            public String getHeader(String string) {
                return "";
            }

            @Override
            public Enumeration<String> getHeaders(String string) {
                return enumeration;
            }

            @Override
            public Enumeration<String> getHeaderNames() {
                return enumeration;
            }

            @Override
            public int getIntHeader(String string) {
                return 0;
            }

            @Override
            public String getMethod() {
                return "";
            }

            @Override
            public String getPathInfo() {
                return "";
            }

            @Override
            public String getPathTranslated() {
                return "";
            }

            @Override
            public String getContextPath() {
                return "";
            }

            @Override
            public String getQueryString() {
                return "";
            }

            @Override
            public String getRemoteUser() {
                return "";
            }

            @Override
            public boolean isUserInRole(String string) {
                return false;
            }

            @Override
            public Principal getUserPrincipal() {
                return principal;
            }

            @Override
            public String getRequestedSessionId() {
                return "";
            }

            @Override
            public String getRequestURI() {
                return "";
            }

            @Override
            public StringBuffer getRequestURL() {
                return new StringBuffer();
            }

            @Override
            public String getServletPath() {
                return "";
            }

            @Override
            public HttpSession getSession(boolean bln) {
                return session;
            }

            @Override
            public HttpSession getSession() {
                return session;
            }

            @Override
            public String changeSessionId() {
                return "";
            }

            @Override
            public boolean isRequestedSessionIdValid() {
                return true;
            }

            @Override
            public boolean isRequestedSessionIdFromCookie() {
                return true;
            }

            @Override
            public boolean isRequestedSessionIdFromURL() {
                return true;
            }

            @Override
            public boolean isRequestedSessionIdFromUrl() {
                return true;
            }

            @Override
            public boolean authenticate(HttpServletResponse hsr) throws IOException, ServletException {
                return true;
            }

            @Override
            public void login(String string, String string1) throws ServletException {

            }

            @Override
            public void logout() throws ServletException {

            }

            @Override
            public Collection<Part> getParts() throws IOException, ServletException {
                return httpCollection;
            }

            @Override
            public Part getPart(String string) throws IOException, ServletException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public <T extends HttpUpgradeHandler> T upgrade(Class<T> type) throws IOException, ServletException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Object getAttribute(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getCharacterEncoding() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setCharacterEncoding(String string) throws UnsupportedEncodingException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public int getContentLength() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public long getContentLengthLong() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getContentType() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public ServletInputStream getInputStream() throws IOException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getParameter(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Enumeration<String> getParameterNames() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String[] getParameterValues(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Map<String, String[]> getParameterMap() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getProtocol() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getScheme() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getServerName() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public int getServerPort() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public BufferedReader getReader() throws IOException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getRemoteAddr() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getRemoteHost() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setAttribute(String string, Object o) {

            }

            @Override
            public void removeAttribute(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Locale getLocale() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Enumeration<Locale> getLocales() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isSecure() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public RequestDispatcher getRequestDispatcher(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getRealPath(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public int getRemotePort() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getLocalName() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public String getLocalAddr() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public int getLocalPort() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public ServletContext getServletContext() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public AsyncContext startAsync() throws IllegalStateException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public AsyncContext startAsync(ServletRequest sr, ServletResponse sr1) throws IllegalStateException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isAsyncStarted() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isAsyncSupported() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public AsyncContext getAsyncContext() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public DispatcherType getDispatcherType() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        taskListServlet = new TaskListServlet(questions, request);
    }

    private void setupQuestionList() {
        questionList = new ArrayList<>();
        QuestionsDTO q1 = new QuestionsDTO();
        QuestionsDTO q2 = new QuestionsDTO();
        QuestionsDTO q3 = new QuestionsDTO();

        q1.setId(1L);
        q1.setFirstQuestion("AA");
        q1.setSecondQuestion("DD");
        q1.setThirdQuestion("GG");

        questionList.add(q1);

        q2.setId(2L);
        q2.setFirstQuestion("BB");
        q2.setSecondQuestion("EE");
        q2.setThirdQuestion("HH");
        questionList.add(q2);

        q3.setId(3L);
        q3.setFirstQuestion("CC");
        q3.setSecondQuestion("FF");
        q3.setThirdQuestion("II");
        questionList.add(q3);

    }

    @Test
    public void shouldQuestionscomeInReverseOrder() {
        setupQuestionList();

        when(taskListServlet.sortedByIDReverse(questionList, 2)).thenCallRealMethod();
        assertAll("TaskListIdReverseFullData",
                () -> {
                    assertEquals(3, taskListServlet.sortedByIDReverse(questionList, 2).get(0).getId());
                },
                () -> {
                    assertEquals(2, taskListServlet.sortedByIDReverse(questionList, 2).get(1).getId());
                },
                () -> {
                    assertEquals(2, taskListServlet.sortedByIDReverse(questionList, 2).size());
                }
        );

    }

    @Test
    public void shouldQuestionscomeInNaturalOrderByID() {
        setupQuestionList();
        spy(taskListServlet.sortedByID(questionList, 10));
        assertAll("TaskListIdForwardFullData",
                () -> {
                    assertEquals(1, taskListServlet.sortedByID(questionList, 10).get(0).getId());
                },
                () -> {
                    assertEquals(2, taskListServlet.sortedByID(questionList, 10).get(1).getId());
                },
                () -> {
                    assertEquals(3, taskListServlet.sortedByID(questionList, 10).size());
                }
        );
    }

    @Test
    public void shouldQuestionscomeInReverseByFirstQuestionCharacter() {
        setupQuestionList();
        spy(taskListServlet.sortedByFirstQuestionReverse(questionList, 4));
        assertAll("TaskListFirstQReverseFullData",
                () -> {
                    assertEquals("CC", taskListServlet.sortedByFirstQuestionReverse(questionList, 4).get(0).getFirstQuestion());
                },
                () -> {
                    assertEquals("BB", taskListServlet.sortedByFirstQuestionReverse(questionList, 4).get(1).getFirstQuestion());
                },
                () -> {
                    assertEquals(3, taskListServlet.sortedByFirstQuestionReverse(questionList, 4).size());
                }
        );
    }

    @Test
    public void shouldQuestionscomeInNaturalByFirstQuestionCharacter() {
        setupQuestionList();
        spy(taskListServlet.sortedByFirstQuestion(questionList, 2));
        assertAll("TaskListFirstQForwardFullData",
                () -> {
                    assertEquals("AA", taskListServlet.sortedByFirstQuestion(questionList, 2).get(0).getFirstQuestion());
                },
                () -> {
                    assertEquals("BB", taskListServlet.sortedByFirstQuestion(questionList, 2).get(1).getFirstQuestion());
                },
                () -> {
                    assertEquals(2, taskListServlet.sortedByFirstQuestion(questionList, 2).size());
                }
        );
    }

    @Test
    public void shouldQuestionscomeInReverseBySecondQuestionCharacter() {
        setupQuestionList();
        spy(taskListServlet.sortedBySecondQuestionReverse(questionList, 20));
        assertAll("TaskListFirstQForwardFullData",
                () -> {
                    assertEquals("FF", taskListServlet.sortedBySecondQuestionReverse(questionList, 20).get(0).getSecondQuestion());
                },
                () -> {
                    assertEquals("EE", taskListServlet.sortedBySecondQuestionReverse(questionList, 20).get(1).getSecondQuestion());
                },
                () -> {
                    assertEquals(3, taskListServlet.sortedBySecondQuestionReverse(questionList, 20).size());
                }
        );
    }

    @Test
    public void shouldQuestionscomeInNaturalBySecondQuestionCharacter() {
        setupQuestionList();
        spy(taskListServlet.sortedBySecondQuestion(questionList, 20));
        assertAll("TaskListFirstQForwardFullData",
                () -> {
                    assertEquals("DD", taskListServlet.sortedBySecondQuestion(questionList, 20).get(0).getSecondQuestion());
                },
                () -> {
                    assertEquals("EE", taskListServlet.sortedBySecondQuestion(questionList, 20).get(1).getSecondQuestion());
                },
                () -> {
                    assertEquals(3, taskListServlet.sortedBySecondQuestion(questionList, 20).size());
                }
        );
    }

    @Test
    public void shouldQuestionscomeInReverseByThirdQuestionCharacter() {
        setupQuestionList();
        spy(taskListServlet.sortedByThirdQuestionReverse(questionList, 2));
        assertAll("TaskListFirstQForwardFullData",
                () -> {
                    assertEquals("II", taskListServlet.sortedByThirdQuestionReverse(questionList, 2).get(0).getThirdQuestion());
                },
                () -> {
                    assertEquals("HH", taskListServlet.sortedByThirdQuestionReverse(questionList, 2).get(1).getThirdQuestion());
                },
                () -> {
                    assertEquals(2, taskListServlet.sortedByThirdQuestionReverse(questionList, 2).size());
                }
        );
    }

    @Test
    public void orderInputShouldReturnIntegerWhenNotNull1WhenNull() {

        assertAll("IfInputNull",
                () -> {
                    assertEquals(Integer.valueOf(1), taskListServlet.orderInput(null));
                },
                () -> {
                    assertEquals(Integer.valueOf(5), taskListServlet.orderInput("5"));
                }
        );
    }

    @Test
    public void orderTest() {
        setupQuestionList();
        assertAll("OrderTableTest",
                () -> {
                    //assertEquals(-1, taskListServlet.order(-1, request, questionList, 1));
                },
                () -> {
                    //taskListServlet.order(-1, request, questionList, 1);
                    // assertEquals(1,questionList.size());
                });
    }
}


/*
public void order(Integer order, HttpServletRequest request,List<QuestionsDTO> question,Integer limit){
switch (order) {
        case -1: question=sortedByIDReverse(question,limit);
            request.setAttribute("id", 1);
            
            break;
        case -2: question=sortedByFirstQuestionReverse(question,limit);
            request.setAttribute("firstQuestion", 2);
            
            break;
        case -3: question=sortedBySecondQuestionReverse(question,limit);
        request.setAttribute("secondQuestion", 3);
        
            break;
        case -4: question=sortedByThirdQuestionReverse(question,limit);
        request.setAttribute("thirdQuestion", 4);
        
            break;
         case 1: question=sortedByID(question,limit);
            request.setAttribute("id", -1);
            
            break;
        case 2: question=sortedByFirstQuestion(question,limit);
            request.setAttribute("firstQuestion", -2);
            
            break;
        case 3: question=sortedBySecondQuestion(question,limit);
        request.setAttribute("secondQuestion", -3);
        
            break;
        case 4: question=sortedByThirdQuestion(question,limit);
        request.setAttribute("thirdQuestion", -4);
        
            break;    
    }    
}
 */
